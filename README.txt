This README.txt is adapted from the Drupal.org README Template:

 * https://www.drupal.org/node/2181737


INTRODUCTION
------------

Acronym as module name: OOE = Object Oriented Examples = One of Each

The Object Oriented Examples (OOE) project is a unique educational
tutorial module for Drupal7 intended primarily for enthusiasts of
graphical software engineering with Unified Modeling Language (UML)
and fans of object-oriented Design Patterns.

This is an actively maintained and evolving development project,
with a dedicated live demonstration site and a gallery of graphical
Unified Modeling Language (UML) diagrams of the OOE system:

 * http://drupal7demo.webel.com.au/module/ooe

 * http://drupal7demo.webel.com.au/view/uml/gallery

OOE may also be taken to mean "One Of Each", as it endeavours to 
represent one of each of the capabilities of Drupal7 module development
in purely object-oriented form, starting with object-oriented versions
of some well known existing Drupal7 tutorials and examples.

It achieves this by using a special "bridge" between the Drupal7
contributed module API (via a specially organised .module file) and
a completely object-oriented OOE world that uses a special PHP recipe
that is (more) amenable to reverse engineering to graphical UML,
currently optimised for processing with the PEAR:PHP_UML script into XMI
and graphical modelling in the MagicDraw UML tool (although once in XMI
any decent UML tool could be used).

When using the OOE classes one never has to deal directly with Drupal
"by convention" structured arrays; instead, the Drupal arrays are encapsulated
completely, and the OOE classes know how to build and hand off valid Drupal
structured arrays to the Drupal contributed module API. 

By encapsulating Drupal structured arrays OOE also supports IDE prompting
on operations/methods (and their tightly bound documentation) that is not
possible with Drupal7's "by convention" structured arrays, as demonstrated in
NetBeans IDE in this short video: http://drupal7demo.webel.com.au/node/2227


REQUIREMENTS
------------

This module requires the following modules:

 * Xautoload (http://drupal.org/project/xautoload)

 * Page controller (http://drupal.org/project/page_controller)

The core Drupal7 includes/form.inc file MUST also be "tweaked"
for use with object-oriented form controllers as described here:

 http://drupal7demo.webel.com.au/node/2361

The following Drupal issue reports describe why these completely
harmless tweaks are required, and also describe those code tweaks:

- https://www.drupal.org/node/2166371

- https://www.drupal.org/node/2165895 

It is planned to provide patches for these against Drupal7 core soon.



INSTALLATION
------------

 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * In order to use the form controller demos you MUST however also
   perform the tweaks to Drupal7 includes/form.inc describe above.



CONFIGURATION
-------------

 * There is currently only one configuration parameter, the
   number of 'current posts' shown in the current posts blocks
   in the object-oriented adaptation of the Current Posts example.

   /admin/config/development/ooe


ISSUE REPORTING
---------------

Drupal.org (sandbox) project page:
  https://drupal.org/sandbox/webel/2120905

To submit bug reports and feature suggestions, or to track changes:
  https://drupal.org/project/issues/2120905


MAINTAINERS
-----------

webel - http://drupal.org/user/392755

No other maintainers are currently sought.


CREDITS
-------

The Object Oriented Examples (OOE) module is developed by Webel IT Australia,
specialists in PHP-driven Drupal CMS web engineering, UML, SysML, Java and XML.
In addition to using Drupal CMS for developing clients' web sites, Webel has
used Drupal CMS for many years to develop educational web sites promoting 
graphical software engineering with Unified Modeling Language (UML) and 
graphical systems engineering with Systems Modeling Language (SysML).

http://www.webel.com.au


SPONSORS SOUGHT
---------------

If you are interesting in seeing OOE expanded to become a fully-fledged,
UML-friendly, object-oriented API for Drupal7 module development, or in
seeing a Drupal8 version developed, please consider supporting the project.
Donations to the Object Oriented Examples (OOE) project may be made online
via PayPal: http://drupal7demo.webel.com.au/lm_paypal/donations.
