<?php

/**
 * @file
 * AbstractControlledProject.
 */

namespace Drupal\ooe\Project;

/**
 * Provides a starting point for page controlled projects.
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/1570 AbstractControlledProject @endlink.
 *
 * @author darrenkelly
 */
abstract class AbstractControlledProject extends AbstractProject implements IControlledProject {

  /**
   * Overridden to return the controlled page menu item.
   *
   * Because this is a page controlled project,
   * the primary menu must be a controlled page menu item.
   *
   * @return \Drupal\ooe\Menu\IMenuItem
   *   The controlled page menu item.
   */
  final public function getMenuItem() {
    return $this->getPageMenuItem();
  }

  /**
   * Overridden to return a new page menu item.
   *
   * @return \Drupal\ooe\Menu\IMenuItem
   *   A new page menu item.
   */
  final protected function myMenuItem() {
    return $this->myPageMenuItem();
  }

  /**
   * The page-controlled menu item.
   *
   * @var \Drupal\ooe\Page\IPageMenuItem
   */
  private $pageMenuItem;

  /**
   * Lazily creates and configures a page menu item.
   *
   * The actual page menu item chosen will depend
   * on @link AbstractControlledProject::myPageMenuItem @endlink.
   *
   * @return \Drupal\ooe\Page\IPageMenuItem
   *   A lazily created and configured page menu item.
   */
  public function getPageMenuItem() {
    if (empty($this->pageMenuItem)) {
      $this->pageMenuItem = $this->myPageMenuItem();
    }
    return $this->pageMenuItem;
  }

  /**
   * The page controller for the main page menu item.
   *
   * @var \Drupal\ooe\Page\IPageController
   */
  private $pageController;

  /**
   * Lazily creates and configures a page controller.
   *
   * The actual page menu item chosen will depend
   * on @link AbstractControlledProject::myPageController @endlink.
   *
   * @return \Drupal\ooe\Page\IPageController
   *   A lazily created and configured page controller.
   */
  protected function getPageController() {
    if (empty($this->pageController)) {
      $this->pageController = $this->myPageController();
    }
    return $this->pageController;
  }

  /**
   * Implement to return a specific page controller.
   *
   * @return \Drupal\ooe\Page\IPageController
   *   A new configured page controller.
   */
  abstract protected function myPageController();

  /**
   * Implement to return a specific page menu item.
   *
   * @return \Drupal\ooe\Page\IPageMenuItem
   *   A new configured page menu item.
   */
  abstract protected function myPageMenuItem();

  /**
   * Creates a default new page menu item using the primary page controller.
   *
   * This is a helper method that assists in implementations
   * of @link AbstractControlledProject::myPageMenuItem @endlink
   * by enforcing
   * some rules (such as using the primary page controller for the menu item)
   * and some sensible defaults and conventions such as optionally prefixing
   * the title and path with the module display name or machine name.
   *
   * There is however no obligation to use it to
   * implement @link AbstractControlledProject::myPageMenuItem() @endink.
   * However, if you choose not to use it, you should at least
   * use the primary page controller of this in the page menu item !
   *
   * @param string $title
   *   The menu title, will be optionally automatically
   *   prefixed with the module display name and ': '.
   * @param string $path
   *   The menu path, will be optionally automatically
   *   prefixed with the module machine name and '/'.
   *
   * @return \Drupal\ooe\Page\IPageMenuItem
   *   A default new page menu item using the primary page controller.
   */
  protected function newPageMenuItem($title, $path) {
    $fullTitle = $this->isDoPrefixMenuTitles() ? $this->getModuleDisplayName() . ": $title" : $title;
    $fullPath = $this->isDoPrefixMenuPaths() ? $this->getModule() . "/$path" : $path;

    return $this->factory()->newPageMenuItem(
            $this->getPageController(), $fullTitle, $fullPath
    );
  }

}

// @codingStandardsIgnoreStart
/**
 * Reimplement in subclasses to configure or override the defaults.
 *
 * @param IMenuItem $menuItem
 * @return IMenuItem
 */
/*
protected function myConfigMenuItem(IMenuItem $menuItem) {
return $this->pageMenuItem;
}
*/
// @codingStandardsIgnoreEnd