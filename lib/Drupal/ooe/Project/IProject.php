<?php

/**
 * @file
 * IProject.
 */

namespace Drupal\ooe\Project;

use Drupal\ooe\Module\IModuleHelper;

/**
 * A project within a module is a useful starting point for creating
 * "one of each" of the type of things a project typically requires,
 * such as a menu item, and a
 * block view that is bound to a block view.
 *
 * Remember, there is no obligation to use an @link IProject @endlink
 * at all, it is merely a useful starting point that covers many cases.
 *
 * @UML: @link http://drupal7demo.webel.com.au/node/1560 IProject @endlink
 *
 * @author darrenkelly
 */
interface IProject extends IModuleHelper {

  /**
   * A primary menu item for the project.
   *
   * @return \Drupal\ooe\Menu\IMenuItem
   *   A primary menu item for the project.
   */
  public function getMenuItem();

  /**
   * A set of menu items handled by this.
   *
   * By convention, the primary menu item should act as the
   * top-level menu item within the project's menu items.
   *
   * @return \Drupal\ooe\Menu\IMenuItem[]
   *   An array of menu items handled by this.
   */
  public function getMenuItems();

  /**
   * A primary block for the project.
   *
   * There is no obligation to use it (enable it, make it visible).
   *
   * @return \Drupal\ooe\Block\IBlock
   *   A primary block for the project.
   */
  public function getBlock();

  /**
   * A primary block view for the primary block of the project.
   *
   * @return \Drupal\ooe\Block\IBlockView
   *   A primary block view for the primary block of the project.
   */
  public function getBlockView();

}
