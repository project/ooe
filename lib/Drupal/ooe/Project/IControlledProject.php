<?php

/**
 * @file
 * IControlledProject.
 */

namespace Drupal\ooe\Project;

/**
 * A "project" within a module, for which the primary menu
 * item is a controlled page menu item.
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/1564 IControlledProject @endlink.
 *
 * @author darrenkelly
 */
interface IControlledProject extends IProject {

  /**
   * The primary page menu item.
   *
   * Usually this is a menu item either for a single page for
   * a very simple project or the top-level grouping menu item
   * for a project with many other sub menu items for subpages.
   *
   * For a typical example please visit
   * @link http://drupal7demo.webel.com.au/ooe the main welcome page @endlink
   * for all demos in OOE, which is offered by the @link Demo @endlink project.
   *
   * @return \Drupal\ooe\Page\IPageMenuItem
   *   A primary controlled page menu item for the project.
   */
  public function getPageMenuItem();

}
