<?php

/**
 * @file
 * DemoFormController.
 */

namespace Drupal\ooe\Demo\Form;

use Drupal\ooe\Form\AbstractFormController;
use Drupal\ooe\Form\Form;
use Drupal\ooe\Form\FormTextField;
use Drupal\ooe\Form\FormSubmit;
use Drupal\ooe\Form\FormFieldset;

/**
 * Demonstrates a simple form controller.
 *
 * Adapted from the Drupal7 form examples of the Examples project.
 *
 * @author darrenkelly
 */
class DemoFormController extends AbstractFormController {

  // DRY! Define constants for field names.
  // We could also use static private $FIELD_ etc. here.
  // (but note PHP5.3 does not support final class variables).

  const FIELD_YOB = 'year_of_birth';

  const FIELD_NAME_FIRST = 'first';

  const FIELD_NAME_LAST = 'last';

  const FIELDSET_NAME = 'name';

  const FUNC_VALIDATE_YOB = 'validateYearOfBirth';

  const FUNC_SUBMIT = 'submit';

  /**
   * Constructor.
   *
   * @param string $module
   *   The module machine name.
   */
  public function __construct($module) {
    parent::__construct($module);
  }

  /**
   * Implementation of the required form builder method.
   *
   * Please note how this OOE version respects the DRY
   * principle through use of field name constants !
   *
   * @param array $form
   *   Drupal form array.
   * @param array $form_submit
   *   Drupal form submission array.
   *
   * @return array
   *   A Drupal form array.
   */
  public function build(array $form, array &$form_submit) {

    $formBuilder = new Form(
        $form,
        t('An OOE controlled form with a description item header, fieldset for a couple of name textfields, a year textfield, a validation handler, a submit button, and a submit handler'),
        // Title.
        t('Fields and field groups can carry descriptions, as can the top-level form')
    );

    $fieldsetName = new FormFieldset(t('Name'));
    $fieldsetName
        ->setCollapsible(TRUE)
        ->setCollapsed(FALSE);

    $textFieldNameFirst = new FormTextField(
        t('Firstname'),
        // Will be translated automatically.
        TRUE
        // Required!
    );

    $textFieldNameFirst
        ->setDescription(t('Please enter your first name.'))
        ->setDefaultValue(t('[First]'))
        ->setSize(20)
        ->setMaxlength(30);
    // Supports setter chaining !

    $textFieldNameLast = new FormTextField(t('Lastname'), FALSE);
    $textFieldNameLast
        ->setDescription(t('Please enter your last name.'))
        ->setDefaultValue(t('[Last]'))
        ->setSize(20)
        ->setMaxlength(30);

    // IMPORTANT: notice array keys are encapsulated so DRY respected !

    $fieldsetName->add(self::FIELD_NAME_FIRST, $textFieldNameFirst);

    $fieldsetName->add(self::FIELD_NAME_LAST, $textFieldNameLast);

    $formBuilder->add(self::FIELDSET_NAME, $fieldsetName);
    // IMPORTANT: after add kids to fieldset !

    // @todo Consider always add child as reference
    // so catches changes after adding (on final get()).

    // https://api.drupal.org/api/examples/form_example!form_example_tutorial.inc/function/form_example_tutorial_6/6
    // 'New form field added to permit entry of year of birth.
    // The data entered into this field will be validated with ..'
    // a _custom_ validation method specified in this OOE demo !

    $textFieldYob = new FormTextField(t('Year of birth'), TRUE);
    // Required so always has to validate.

    $textFieldYob->setDescription(t('Format is "YYYY", valid range is [1900..2000]'));

    $formBuilder->add(self::FIELD_YOB, $textFieldYob);

    $this->addValidateHandler($form, self::FUNC_VALIDATE_YOB);

    // https://api.drupal.org/api/examples/form_example!form_example_tutorial.inc/function/form_example_tutorial_2/7
    //
    // 'A simple submit button that refreshes the form and clears
    // its contents. This is the default behavior for forms.'

    $submit = new FormSubmit(t('Submit'));
    // No title !

    $formBuilder->setSubmit($submit);
    // Default key is 'submit'.

    $this->addSubmitHandler($form, self::FUNC_SUBMIT);

    return $formBuilder->get();
    // Merges original $form with additions from this method.
  }

  /**
   * Adapted from form_example_tutorial.inc.
   *
   * Now we add a handler/function to validate the data entered into the
   * "year of birth" field to make sure it's between the values of 1900
   * and 2000. If not, it displays an error. The value report is
   * $form_state['values'] (see http://drupal.org/node/144132#form-state).
   *
   * Notice the name of the function. It is simply the name of the form
   * followed by '_validate'. This is always the name of the default validation
   * function. An alternate list of validation functions could have been
   * provided in $form['#validate'].
   *
   * @param array $form
   *   A Drupal form array.
   * @param array $form_state
   *   A Drupal form state array.
   *
   * @see form_example_tutorial_6_validate
   */
  static public function validateYearOfBirth(array $form, array &$form_state) {
    $year_of_birth = self::getFormValue($form_state, self::FIELD_YOB);
    if ($year_of_birth && ($year_of_birth < 1900 || $year_of_birth > 2000)) {
      form_set_error(self::FIELD_YOB,
          t('Enter a year between 1900 and 2000.')
          );
    }
  }

  /**
   * Determines whether the DRY or WET submit handler is used.
   *
   * @var bool
   */
  static private $USE_DRY_SUBMIT = TRUE;

  /**
   * Submit handler: delegates to either the DRY or WET submit handler.
   *
   * @param array $form
   *   A Drupal form array.
   * @param array $form_state
   *   A Drupal form state array.
   */
  static public function submit(array $form, array &$form_state) {
    if (self::$USE_DRY_SUBMIT) {
      self::submitDryStyle($form, $form_state);
    }
    else {
      self::submitDrupalStyle($form, $form_state);
    }
  }

  /**
   * Submit function adapted from form_example_tutorial_7_submit(); DRY form.
   *
   * 'Adds a submit handler/function to our form to send a successful
   * completion message to the screen.'
   *
   * Please note how this OOE version respects
   * the @link http://drupal7demo.webel.com.au/node/10 DRY @endlink
   * principle through use of field name constants !
   *
   * @param array $form
   *   A Drupal form array.
   * @param array $form_state
   *   A Drupal form state array.
   *
   * @todo This is however not yet compatible with Drupal translation
   * (which looks for a literal string with placeholders outside
   * run time).
   */
  static public function submitDryStyle(array $form, array &$form_state) {

    // @todo Need utility method for standard formatting
    // of fields by name and value fetched from $form.
    drupal_set_message(
        // @codingStandardsIgnoreStart
        t('DRY OO-style handler version: Submitted: name="@' . self::FIELD_NAME_FIRST . ' @' . self::FIELD_NAME_LAST . '", year of birth=@' . self::FIELD_YOB,
            array(
              '@' . self::FIELD_NAME_FIRST => self::getFormValue($form_state, self::FIELD_NAME_FIRST),
              '@' . self::FIELD_NAME_LAST => self::getFormValue($form_state, self::FIELD_NAME_LAST),
              '@' . self::FIELD_YOB => self::getFormValue($form_state, self::FIELD_YOB),
            )
        )
        // Coder: ERROR | Concatenating translatable strings is not allowed,
        // use placeholders instead and only one string literal.
        // @codingStandardsIgnoreEnd
    );
  }

  /**
   * Submit function adapted from form_example_tutorial_7_submit(): WET form.
   *
   * 'Adds a submit handler/function to our form to send a successful
   * completion message to the screen.'
   *
   * Please note this alternative version uses a quite error
   * prone @link http://drupal7demo.webel.com.au/node/12 WET @endlink
   * coding style with repetition of the names of form fields
   * in both array keys and in Drupal format string placeholders.
   *
   * It is however compatible with the Drupal t() method since the
   * format string with placeholders passed to t() can be read
   * by the Drupal localization system outside run time.
   *
   * @param array $form
   *   A Drupal form array.
   * @param array $form_state
   *   A Drupal form state array.
   */
  static public function submitDrupalStyle(array $form, array &$form_state) {
    drupal_set_message(
       t('WET Drupal-style handler version: Submitted: name="@first @last", year of birth=@year_of_birth',
        array(
          '@first' => $form_state['values']['first'],
          '@last' => $form_state['values']['last'],
          '@year_of_birth' => $form_state['values']['year_of_birth'],
        ))
    );
  }

}
