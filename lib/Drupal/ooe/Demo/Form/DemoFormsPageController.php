<?php

/**
 * @file
 * DemoFormsPageController.
 */

namespace Drupal\ooe\Demo\Form;

use Drupal\ooe\Page\AbstractPageController;
use Drupal\ooe\Common;
use Drupal\ooe\Render\RenderSet;
use Drupal\ooe\Render\RenderMenuSubtree;
use Drupal\ooe\Demo\DemoCommon;
use Drupal\ooe\Menu\MenuCommon;

/**
 * A page controller for a demo of various form styles.
 *
 * @author darrenkelly
 */
class DemoFormsPageController extends AbstractPageController {

  /**
   * Constructor.
   */
  public function __construct() {
    parent::__construct(Common::MODULE);
  }

  const CALLBACK = 'page';

  /**
   * Overridden to return the eventual callback method.
   *
   * @return string
   *   The callback function name.
   */
  public function getCallbackName() {
    return self::CALLBACK;
  }

  /**
   * Builds a page render array for the set of form demos.
   *
   * This version uses a @link IRenderFactor @endlink.
   *
   * @return array
   *   A Drupal page render array
   */
  public function page() {

    $page = $this->rf()->newSet();

    $page->addItem('info', DemoCommon::renderTipHoverLinks());

    $link_path = Common::STEM_DEMO_FORMS;
    $page->addItem(
        'links',
        $this->rf()->newMenuSubtree($link_path, MenuCommon::MENU_NAVIGATION)
    );

    return $page->get();
  }

  /**
   * Builds a page render array for the set of form demos.
   *
   * This version does not use a @link IRenderFactor @endlink.
   *
   * @return array
   *   A Drupal page render array
   */
  public function pageOldNoFactory() {

    $page = new RenderSet();

    $page->addItem('info', DemoCommon::renderTipHoverLinks());

    $link_path = Common::STEM_DEMO_FORMS;
    $page->addItem('links',
                   new RenderMenuSubtree($link_path, MenuCommon::MENU_NAVIGATION));

    return $page->get();
  }

}
