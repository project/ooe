<?php

/**
 * @file
 * DemoOfForms.
 */

namespace Drupal\ooe\Demo\Form;

use Drupal\ooe\Project\AbstractControlledProject;
use Drupal\ooe\Form\FormControllerMenuItem;
use Drupal\ooe\Block\BlockVisibilityKind;
use Drupal\ooe\Block\IBlock;
use Drupal\ooe\Demo\Form\DemoFormsPageController;
use Drupal\ooe\Common;

/**
 * A project that demonstrates both an old-style Drupal7 using
 * a .module file form builder callback and .module file
 * submit and validation handlers, and an OOE-style controlled
 * form that encapsulates the form builder and handlers.
 *
 * See it live
 * at @link http://drupal7demo.webel.com.au/ooe/demo_forms @endlink.
 *
 * @author darrenkelly
 */
class DemoOfForms extends AbstractControlledProject implements IDemoOfForms {

  /**
   * Implemented to configure a block for the forms demo.
   *
   * Currently configures the block to not be listed.
   *
   * @return \Drupal\ooe\Block\IBlock
   *   A new configured forms demo block.
   */
  protected function myBlock() {
    $deltaSuffix = 'block_forms_demo';
    $info = t('Forms demo block');
    return $this->newBlock($deltaSuffix, $info)
            ->setVisibility(BlockVisibilityKind::NOTLISTED);
  }

  /**
   * Implemented to create and configure a @link DemoBlockView @endlink.
   *
   * @param \Drupal\ooe\Block\IBlock $block
   *   The primary block for which this must create a new block view.
   *
   * @return \Drupal\ooe\Block\IBlockView
   *   A new configured forms demo block view.
   */
  protected function myBlockView(IBlock $block) {
    $subject = t('!module-display-name: Forms demo block', array('!module-display-name' => $this->getModuleDisplayName()));
    return new DemoBlockView($block, $subject);
  }

  /**
   * Implemented to return a new @link DemoFormsPageController @endlink.
   *
   * See it live
   * at @link http://drupal7demo.webel.com.au/ooe/demo_forms @endlink.
   *
   * @return \Drupal\ooe\Page\IPageController
   *   A new forms demo primary menu page controller.
   */
  protected function myPageController() {
    return new DemoFormsPageController();
  }

  /**
   * Implemented to configure forms demo main group page menu item.
   *
   * @return \Drupal\ooe\Page\IPageMenuItem
   *   A new configured forms demo main group page menu item.
   */
  protected function myPageMenuItem() {
    $path = Common::STEM_DEMO_FORMS;
    $title = 'forms demos';
    return $this->newPageMenuItem($title, $path)
            ->forceAccessCallbackTrue();
  }

  /**
   * A menu item for a simple form example with no controller.
   *
   * This is "old" Drupal7 style.
   *
   * @var \Drupal\ooe\Form\IFormMenuItem
   */
  private $menuItemBasicForm;

  /**
   * Lazily creates and configures a menu item for a simple forms demo.
   *
   * Lazily creates and configures a menu item for a simple forms
   * demo with no controller ("old" Drupal7 style).
   *
   * IMPORTANT: Because it has no controller the 'page arguments' will be
   * set to call a function in the original ooe.module file (not this).
   *
   * This is only here to demonstrate the "old" Drupal7 way of doing it,
   * when working with OOE one should otherwise always use a controller.
   *
   * @return \Drupal\ooe\Form\IFormMenuItem
   *   A lazily created and configured menu item
   *   for a simple form example with no controller.
   */
  protected function menuItemBasicForm() {
    if (empty($this->menuItemBasicForm)) {
      $this->menuItemBasicForm = $this->factory()->newFormMenuItem(
          $this->getModuleDisplayName() . ': non-OO uncontrolled form', STEM_DEMO_FORMS . '/simple'
      );
      $this->menuItemBasicForm->forceAccessCallbackTrue();
      // @todo Reconsider for www public, even for demo.

      $this->menuItemBasicForm->setPageArguments(array($this->getModule() . '_form_demo'));
      $this->menuItemBasicForm->setDescription(
          'An old-style form without a controller, needs a form builder callback and validate and submit handlers in the .module file'
      );
      $this->menuItemBasicForm->setWeight(0);
    }
    return $this->menuItemBasicForm;
  }

  /**
   * Implemented to add menu items.
   *
   * Adds menu items for both the "simple"
   * uncontrolled form demo and the controlled OOE-style form demo.
   */
  protected function myFillMenuItemSet() {
    $this->addMenuItem($this->menuItemBasicForm())
        ->addMenuItem($this->menuItemControlledForm());
  }

  /**
   * A form controller for the controlled form demo.
   *
   * @var Drupal\ooe\Form\IFormController
   */
  private $formController;

  /**
   * Lazily creates and configures a demo form controller.
   *
   * Lazily creates and configures
   * a @link DemoFormController @endlink
   * for the form demo.
   *
   * @return \Drupal\ooe\Form\IFormController
   *   A lazily created and configured form controller for the form demo.
   */
  protected function formController() {
    if (empty($this->formController)) {
      $this->formController = new DemoFormController($this->getModule());
      // $this->formController->..
    }
    return $this->formController;
  }

  /**
   * A form controller menu item for the controlled form demo.
   *
   * @var \Drupal\ooe\Form\IFormControllerMenuItem
   */
  private $menuItemControlledForm;

  /**
   * Lazily creates and configures a form controller menu item.
   *
   * Lazily creates and configures a @link FormControllerMenuItem @endlink
   * form controller menu item for the controlled form demo.
   *
   * @return \Drupal\ooe\Form\IFormControllerMenuItem
   *   A lazily created and configured form controller
   *   menu item for the controlled form demo.
   */
  protected function menuItemControlledForm() {
    if (empty($this->menuItemControlledForm)) {
      $this->menuItemControlledForm = new FormControllerMenuItem(
          $this->formController(), $this->getModule(), $this->getModuleDisplayName() . ': object-oriented controlled form', Common::STEM_DEMO_FORMS . '/controlled'
          // @todo Make DRY.
      );
      $this->menuItemControlledForm->setAccessArguments(array());
      $this->menuItemControlledForm->forceAccessCallbackTrue();
      $this->menuItemControlledForm->setDescription(
          'An OOE-style form with an IFormController; encapsulates the form builder callback and validate and submit handlers in a PHP Class file'
      );
      $this->menuItemControlledForm->setWeight(1);
    }
    return $this->menuItemControlledForm;
  }

  /**
   * Delegate form builder for the uncontrolled Drupal7-style form demo.
   *
   * Currently uses @link DemoOfForms::myFormDrupalStyle @endlink.
   *
   * @todo Update to use OOE form builder classes.
   *
   * @param array $form
   *   A Drupal form array.
   * @param array $form_submit
   *   A Drupal form submission array.
   *
   * @return array
   *   A Drupal form array.
   */
  public function build(array $form, array &$form_submit) {
    return $this->myFormDrupalStyle($form, $form_submit);
    // @todo Prefer OOE Form Builder objects.
  }

  /**
   * Creates a Drupal7-style form for the "simple" uncontrolled form demo.
   *
   * Adapted from the main Drupal7 form examples: form_example_tutorial_7().
   *
   * Please note how this Drupal-style version uses very
   * @link http://drupal7demo.webel.com.au/node/12 WET @endlink coding,
   * not @link http://drupal7demo.webel.com.au/node/12 DRY @endlink coding;
   * the field names and form array keys strings are repeated many times
   * in tedious, error-prone fashion,
   * and there is no object-oriented intelligence
   * used to enforce the documented (only) Drupal array conventions.
   *
   * Dr Darren of Webel IT Australia says:
   *
   * "I don't care how many core Drupal coders code this way,
   * I absolutely insist that you should not repeat array key strings ever
   * when coding against Webel's OOE module as an object-oriented bridge API
   *  for Drupal.
   *
   * I suggest that you compare this typical Drupal-style code with the
   * superior
   * encapsulation illustrated by the OOE @link DemoFormController @endlink,
   * and that when
   * working with OOE you never code like most Drupal7 examples ever again.'
   *
   * @param array $form
   *   A Drupal form array.
   * @param array $form_submit
   *   A Drupal form submission array.
   *
   * @return array
   *   A Drupal form array.
   */
  protected function myFormDrupalStyle(array $form, array &$form_submit) {

    $form['description'] = array(
      // https://api.drupal.org/api/examples/form_example!form_example_tutorial.inc/function/form_example_tutorial_1/7
      '#type' => 'item',
      '#title' => t('A form with a fieldset for a couple of textfields, a validation handler, a submit button, and a submit handler'),
      '#description' => t('This one adds #default_value and #description'),
        // https://api.drupal.org/api/examples/form_example!form_example_tutorial.inc/function/form_example_tutorial_5/7
    );

    $form['name'] = array(
      '#type' => 'fieldset',
      '#title' => t('Name'),
      // Make the fieldset collapsible.
      // https://api.drupal.org/api/examples/form_example!form_example_tutorial.inc/function/form_example_tutorial_4/7
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $form['name']['first'] = array(
      '#title' => t('Firstname'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => "First name",
      // @codingStandardsIgnoreStart
      //'#description' => "Please enter your first name.",
      // @codingStandardsIgnoreEnd
      '#description' => t('Please enter your first name.'),
      '#size' => 20,
      '#maxlength' => 20,
    );

    $form['name']['last'] = array(
      '#title' => t('Lastname'),
      '#type' => 'textfield',
    );

    // https://api.drupal.org/api/examples/form_example!form_example_tutorial.inc/function/form_example_tutorial_6/6
    // New form field added to permit entry of year of birth.
    // The data entered into this field will be validated with
    // the default validation function.
    $form['year_of_birth'] = array(
      '#type' => 'textfield',
      '#title' => "Year of birth",
      // @codingStandardsIgnoreStart
      //'#description' => 'Format is "YYYY", valid range is [1900..2000]',
      // @codingStandardsIgnoreEnd
      '#description' => t('Format is "YYYY", valid range is [1900..2000]'),
    );

    // @codingStandardsIgnoreStart
    // https://api.drupal.org/api/examples/form_example!form_example_tutorial.inc/function/form_example_tutorial_2/7
    // @codingStandardsIgnoreEnd
    // 'A simple submit button that refreshes the form and clears
    // its contents. This is the default behavior for forms.'
    $form['submit'] = array(
      '#value' => 'Submit',
      '#type' => 'submit',
    );

    return $form;
  }

  /**
   * Delegate validator for the "simple" Drupal7-style  uncontrolled form demo.
   *
   * Adapted from form_example_tutorial_6_validate($form, &$form_state).
   *
   * 'Now we add a handler/function to validate the data entered into the
   * "year of birth" field to make sure it's between the values of 1900
   * and 2000. If not, it displays an error. The value report is
   * $form_state['values'] (see http://drupal.org/node/144132#form-state).'
   *
   * This is currently coded in "old"
   * @link http://drupal7demo.webel.com.au/node/12 WET @endlink
   * Drupal7-style with array key strings, not OOE
   * @link http://drupal7demo.webel.com.au/node/12 DRY @endlink style.
   *
   * @param array $form
   *   A Drupal form array.
   * @param array $form_state
   *   A Drupal form state array.
   *
   * @see form_example_tutorial_6()
   */
  public function validate(array $form, array &$form_state) {
    $year_of_birth = $form_state['values']['year_of_birth'];
    if ($year_of_birth && ($year_of_birth < 1900 || $year_of_birth > 2000)) {
      form_set_error('year_of_birth', t('Enter a year between 1900 and 2000.'));
    }
  }

  /**
   * Delegate for the submit function adapted from form_example_tutorial_7().
   *
   * 'Adds a submit handler/function to our form to send a successful
   * completion message to the screen'.
   *
   * This is currently coded in "old"
   * @link http://drupal7demo.webel.com.au/node/12 WET @endlink
   * Drupal7-style with "by convention" array key strings,
   * and with variable names buried in the message string
   * and formatting string, not OOE
   * @link http://drupal7demo.webel.com.au/node/12 DRY @endlink style.
   *
   * @param array $form
   *   A Drupal form array.
   * @param array $form_state
   *   A Drupal form state array.
   */
  public function submit(array $form, array &$form_state) {
    drupal_set_message(
        t('The form has been submitted. name="@first @last", year of birth=@year_of_birth', array(
      '@first' => $form_state['values']['first'],
      '@last' => $form_state['values']['last'],
      '@year_of_birth' => $form_state['values']['year_of_birth'],
            )
        )
    );
    // @todo Encapsulate !
    // Webel: using field names hidden in formatting strings
    // like this is not robust, it is fragile and WET, not DRY.
    // There must be a better way (even if Drupal currently does it).
  }

}
