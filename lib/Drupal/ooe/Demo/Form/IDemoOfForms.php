<?php

/**
 * @file
 * IDemoOfForms.
 */

namespace Drupal\ooe\Demo\Form;

use Drupal\ooe\Project\IControlledProject;

use Drupal\ooe\Form\IFormManager;

/**
 * An OOE project for demonstration of various form types.
 *
 * @author darrenkelly
 */
interface IDemoOfForms extends IControlledProject, IFormManager {
}
