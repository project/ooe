<?php

/**
 * @file
 * OoePageController.
 */

namespace Drupal\ooe\Demo;

use Drupal\ooe\Common;
use Drupal\ooe\Page\AbstractPageController;
use Drupal\ooe\Menu\MenuCommon;
use Drupal\ooe\Xhtml\XHTML;
use Drupal\ooe\Demo\Theme\WebelD7bartikCSS;

/**
 * An IPageController implementation for the top-level OOE Welcome page.
 *
 * The comments in the implementation
 * of @link OoePageController::page @endlink()
 * serve to guide developers progressively
 * through the use of OOE @link IRender @endlink objects.
 *
 * @author darrenkelly
 */
class OoePageController extends AbstractPageController {

  /**
   * Constructor.
   */
  public function __construct() {
    parent::__construct(Common::MODULE);
    // @todo: how inject/propagate module name.
  }

  /**
   * Overridden to return the name of the eventual callback method.
   *
   * @return string
   *   The callback name.
   */
  public function getCallbackName() {
    return 'page';
  }

  /**
   * Builds the main demos welcome page using render objects.
   *
   * This implementation uses OOE @link IRender @endlink
   * objects to generate the main demos welcome page.
   *
   * Render objects encapsulate a subheader with a
   * greeting, some paragraphs with general information
   * about the demos and links to related resources,
   * as well as providing a subtree of demo menu items.
   *
   * All render objects are obtained from an @link IRenderFactory @endlink.
   *
   * The comments in this implementation
   * guide developers progressively through
   * the use of OOE @link IRender @endlink objects.
   *
   * Visit also:
   * @link https://drupal.org/node/930760 Render Arrays in Drupal 7 @endlink.
   *
   * @return array
   *   A Drupal render array, built using OOE render objects.
   */
  public function page() {

    // We start with a new render group/set that can contain children.
    $page = $this->rf()->newSet();

    // Use an H3 subheader renderer within the group,
    // obtained from the IRenderFactory.
    $page->addItem(
        'subheader', $this->rf()->newH(
            t('Welcome to the "OOE = Object Oriented Examples = One of Each" tutorial module demos'), 3)
    );

    // Then we demonstrate some simple P render objects,
    // only intended for very simple markup.
    $page->addItem(
        'text', $this->rf()->newP(
            t('Currently menu items for all demos are collected under the OOE group of the main navigation menu.')
        )
    );

    $page->addItem(
        'textmenus', $this->rf()->newP(
            t('An expanded subtree of menu links to all demos is also provided at the bottom of this page.')
        )
    );

    // The next few examples have some more
    // complex markup buried in them, some links.
    $lDocs = l(t('OOE Module'), Common::URL_DEMO_SITE . '/module/ooe');
    $page->addItem(
        'docs', $this->rf()->newP(
            t('The main description and documentation for the OOE project is under !link-docs.', array('!link-docs' => $lDocs))
        )
    );

    $lApi = l(t('OOE API'), Common::URL_DEMO_SITE . '/api/ooe');
    $page->addItem(
        'api', $this->rf()->newP(
            t('The automatically extracted API documentation with code is under !link-api.', array('!link-api' => $lApi))
        )
    );

    $lUml = l(t('OOE UML'), Common::URL_DEMO_SITE . '/node/47');
    $page->addItem(
        'uml', $this->rf()->newP(
            t('The graphical Unified Modeling Language (UML) analysis and design(ed) models are under !link-uml.', array('!link-uml' => $lUml))
        )
    );

    // Now demonstrate wrapping some markup explicitly with tags;
    // this approach is a bit tedious,
    // but later we show how the render factory gives us
    // some nice shortcuts to make it much easier.
    //
    // We will have some markup wrapped in bold B and pushed inside
    // a DIV grouping render set object with a CSS warning class and
    // some centering to highlight it.
    //
    $tagB = $this->tf()->newTag(XHTML::B);
    // A reusable tag for wrapping render objects !

    $umlwarn = $this->rf()->newR(
        // See https://www.drupal.org/node/322774
        // 'keep inline markup for translation but avoid block tags'.
        t('Unless you study the UML diagrams and the policy notes linked to them you will<br/>probably not fully understand this tutorial module and why it is coded as it is !'), $tagB
        // This will "wrap" the markup using the
        // Drupal render array prefix/suffix.
    );

    // Drupal t() concerns: from https://www.drupal.org/node/194962
    // How to write Drupal translatable interfaces
    // '4. t() should whenever possible - do not contain HTML
    // (except content sensitive translations with links)'
    // '5. Do not add line breaks inside t().'
    // '6. Do not split t() strings over multiple lines separated with DOTs.'

    // We explicitly ask for a DIV wrapper tag.
    $tagDiv = $this->tf()->newTag(XHTML::DIV);
    $tagDiv->setCssClasses(array(WebelD7bartikCSS::WARN, WebelD7bartikCSS::CENTER));
    $cssStyleWarn = 'border:2px solid #333; background-color: #FFDDAA !important; margin: 1ex 1em; padding: 1ex 1em;';
    $tagDiv->setCssStyle($cssStyleWarn);

    // Ask the factory for a DIV wrapped set.
    $umlWarnDiv = $this->rf()->newSet($tagDiv);
    // We could instead have asked simply for a newSetDiv, but
    // then it would not have classes applied to the wrapping DIV.
    // Put the bolded warning markup in the styled DIV group.
    $umlWarnDiv->addItem('umlwarn', $umlwarn);

    // Put the styled DIV group in the page group.
    $page->addItem('uml_warn_div', $umlWarnDiv);

    $lUmlDiagrams = l(t('UML diagrams for the Demo class'), Common::URL_DEMO_SITE . '/node/1652');
    $page->addItem('umlthis', $this->rf()->newP(
            t('TIP: start with the !link-uml-diagrams for this page !', array('!link-uml-diagrams' => $lUmlDiagrams))
        )
    );

    //
    // Instead of wrapping tediously by hand, we can
    // get render elements already wrapped for us.
    // We can get well-known tags from the tag factory
    // directly (then adjust attributes if we wish).
    // And instead of dealing with block-level "markup"
    // render objects we can get grouping objects.
    //
    // For a first demo, we will hold each piece in local variables
    // and perform each operation on separate lines (no chaining).
    //

    $tagI = $this->tf()->newI();
    // Reusable !
    $tagI->addCssClass(WebelD7bartikCSS::DEMO);
    $cssStyleDemo = 'background-color: #ccFFcc !important; margin-bottom: 0.5ex; border: 1px solid silver;';
    $tagI->setCssStyle($cssStyleDemo);

    $iGenerated = $this->rf()->newR(
        t('This page is generated using OOE IRender objects to manage Drupal7 render arrays.'), $tagI
        // Will wrap the markup in an I tag using a
        // Drupal render array prefix/suffix.
    );

    $pInfo = $this->rf()->newSetP();
    // Best to use a grouping P object here.
    // Then give it a child.
    $pInfo->addItem('iGenerated', $iGenerated);

    // Then add it to the page group.
    $page->addItem('pInfo', $pInfo);

    // Using chaining, we can perform the entire composition without any
    // temporary variables, although sometimes that makes it harder to read.
    //
    // We reuse the $tagI wrapper from above; any
    // attributes applied to it will be reused too !
    //
    $page->addItem(
        'pClass', $this->rf()->newSetP()->addItem(
            'iClass', $this->rf()->newR(
                t(
                    'This page is generated by the class: !class', array('!class' => get_class($this))), $tagI
            )
        )
    );
    // That may at first seem like quite a bit of work just to create
    // a P with some styled I markup and some text, however it will be
    // able to generate a valid nested Drupal render array. And because the
    // render objects are fetched via a factory, we can apply policies.

    // Now we create a header for the subtree of menu links for all demos.
    $page->addItem(
        'menuLinksHeader', $this->rf()->newH(
            t('!module-display-name: Expanded menu item links for all demos', array('!module-display-name' => Common::MODULE_NAME)
            ), 4)
    );

    // And the entire menu subtree for all
    // demos is fetched and displayed as links.
    $linkPath = Common::MODULE;
    $page->addItem(
        'menuLinks', $this->rf()->newMenuSubtree(
            $linkPath, MenuCommon::MENU_NAVIGATION
        )
    );

    // Finally, we ask the top-level IRenderGroup
    // to build the entire Drupal render array.
    return $page->get();
  }

  /**
   * OBSOLETE implementation returns simplest markup array with greeting.
   *
   * This OBSOLETE implementation deliberately just
   * returns the simplest #markup array with a greeting.
   *
   * No translation.
   *
   * @return array
   *   A drupal page render array.
   */
  public function pageNonOo() {
    $page = array(
      '#markup' =>
      '<h3>' . 'Welcome to "One of Each (OOE) = Object Oriented Examples" tutorial module demos' . '</h3>' .
      '<p>' . 'Currently all demos are collected under the OOE group of the main navigation menu.' . '</p>',
    );
    return $page;
  }

}

// @codingStandardsIgnoreStart
/* NO: PageController expects noargs constructor
 * but can't overload constructors !
 * See also:
 * http://stackoverflow.com/questions/1699796/best-way-to-do-multiple-constructors-in-php
 public function __construct($module) {
  parent::__construct($module);
 }
 */
// @codingStandardsIgnoreEnd
