<?php

/**
 * @file
 * DemoBlockView.
 */

namespace Drupal\ooe\Demo\Block;

use Drupal\ooe\Block\AbstractBlockView;
use Drupal\ooe\Render\Render;

/**
 * Demonstrates an encapsulated block view.
 *
 * @author darrenkelly
 */
class DemoBlockView extends AbstractBlockView {

  // @codingStandardsIgnoreStart
  //protected function myContentHTML() { return t('Test my content.');}
  // @codingStandardsIgnoreEnd

  /**
   * Implemented to return some stub render array content.
   *
   * @return array
   *   A Drupal render array.
   */
  protected function myContentRenderArray() {
    $r = new Render(t('My translatable content.'));
    return $r->get();
  }

}
