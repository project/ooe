<?php

/**
 * @file
 * OoeBridge.
 */

namespace Drupal\ooe\Demo\Module;

// OK with xautoload, and MyClassOrInterface.php (not MyClassOrInterface.inc).
use Drupal\ooe\Module\AbstractModuleMap;
use Drupal\ooe\Common;
use Drupal\ooe\Demo\Tabs\DemoOfMenuTabs;
use Drupal\ooe\Theme\BartikRegions;
use Drupal\ooe\Demo\Adapted\CurrentPosts;
use Drupal\ooe\Demo\Project\Demo;
use Drupal\ooe\Demo\Arguments\DemoOfPageArguments;
use Drupal\ooe\Demo\Arguments\IDemoOfPageArguments;
use Drupal\ooe\Demo\Form\DemoOfForms;

// For convenience (brevity) define some holders of Common consts.
define('STEM_DEMO_FORMS', Common::STEM_DEMO_FORMS);

/**
 * The "module-file to object-orientation" bridge for the main OOE tutorial.
 *
 * For a full explanation of the special role of this bridge file please visit:
 * @link http://drupal7demo.webel.com.au/module/ooe Webel Drupal7 demo: OOE @endlink.
 *
 * It enables one, in combination with a
 * @link http://drupal7demo.webel.com.au/node/1034 special PHP coding recipe @endlink,
 * developed by Darren Kelly of Webel IT Australia,
 * to reverse-engineer and model significant portions
 * of the object-oriented parts of the system (everything on
 * the "other side" of the ooe.module file) in graphical
 * @link http://drupal7demo.webel.com.au/node/47 Unified Modelling Language (UML) @endlink !
 *
 * This class includes object-oriented bridge method equivalents of
 * all hook, handler, and callback functions in the .module file.
 * The main ooe.module file delegates every (!) non-OO
 * hook, handler, or callback function to
 * a method in this bridge with identical function name, except
 * the MYMODULE_ prefix is dropped from each delegate method here.
 * (The method parameter signatures need not always exactly match the
 * .module function signatures, since in some cases the entire set of
 * arguments is simple automatically forwarded to downstream delegates.)
 *
 * IMPORTANT: This special OOE bridge convention means that the delegate
 * methods (only) of such bridge classes (only) must depart from the
 * @link https://drupal.org/node/608152 Drupal OO coding standard recommendation @endlink
 * that 'Methods and class properties should use lowerCamel naming'
 * in order to match the
 * @link https://drupal.org/coding-standards">non-OO Drupal coding standard @endlink
 * that 'Functions and variables should be named using lowercase,
 * and words should be separated with an underscore'.
 *
 * The operations/methods of all other Classes and Interfaces in the
 * OOE system otherwise meet the Drupal function coding standards,
 * except that there is an outstanding inconsistency regarding
 * $camelCase vs $lower_case parameter variables for methods:
 * @link https://www.drupal.org/node/2303963 OO: Coder complains about camel caps (camelCase) argument to setter method in Class @endlink
 *
 * The only callbacks, validate and submit handlers
 * given here are for demonstration of the "old" Drupal7 way of doing it;
 * in OOE otherwise all callbacks and handlers are encapsulated in
 * special object-oriented controllers like
 * @link \Drupal\ooe\Page\IPageController @endlink
 * and
 * @link \Drupal\ooe\Form\IFormController @endlink.
 *
 * In other words, for the
 * sake of educational counter-demonstration, this bridge is more complex
 * than a bridge for a completely object-oriented OOE-style module need be.
 *
 * While one could directly implement many of the methods here, that would
 * undermine its role as a bridge; thus most of the delegate methods here
 * in turn delegate to implementations provided by demonstration
 * @link IProject @endlink implementations, and to page controller
 * and form controller demonstrations.
 *
 * @author Darren Kelly (Webel IT Australia)
 */
class OoeBridge extends AbstractModuleMap {

  /**
   * Constructor.
   *
   * @todo Consider introducing an IModule instead
   * of just passing the module machine name around.
   *
   * @param string $module
   *   The module machine name.
   */
  public function __construct($module) {
    parent::__construct($module);
  }

  /**
   * Delegate for the ooe_help() implementation of hook_help().
   *
   * Displays help and module information
   * (in this case merely the Current Posts example help).
   *
   * Adapted from the Current Posts example:
   * @link https://drupal.org/node/1095546 Writing comments and implementing your first hook @endlink.
   *
   * Visit also
   * @link https://drupal.org/node/632280 Help text standard (for core and contrib) @endlink
   *
   * @todo Use OOE @link IRender @endlink objects
   * (currently it just uses HTML markup).
   *
   * @param string $path
   *   The path of the site we're using to display help.
   * @param array $arg
   *   Array that holds the current path as returned from arg() function.
   *
   * @return string|array
   *   A page string or render array for help info.
   *
   * @see http://drupal7demo.webel.com.au/module/ooe
   *
   * @see http://drupal7demo.webel.com.au/module/ooe/uml
   *
   * @see http://drupal7demo.webel.com.au/module/api/ooe
   */
  public function help($path, array $arg) {
    switch ($path) {
      // 'A switch statement is used here because it is typical for the
      // help function to offer more than one page containing help text.'
      case "admin/help#ooe":

        // @codingStandardsIgnoreStart
        // ORIGINAL.
        // 'The admin/help#modulename case is used by Drupal core
        // to link from the main help page (/admin/help or ?q=admin/help).'
        // return '<p>' . t("Displays links to nodes created on this date") .
        // '</p>';
        // 'You will eventually want to add more text
        // to provide a better help message to the user'
        // @codingStandardsIgnoreEnd

        // Webel.
        return '<p>' . t("The Current Posts part of the OOE demo displays links to nodes created on this date") . '</p>';

      // @codingStandardsIgnoreStart
      break;
      // Coder: WARNING | Code after RETURN statement cannot be executed.
      // Webel: left in since in the original Current Posts example.
      // @codingStandardsIgnoreStart
    }
  }

  /**
   * Delegate for the ooe_block_info() implementation of hook_block_info().
   *
   * IMPORTANT: this version uses an OOE IBlockSet with multiple
   * IBlock implementations to encapsulate the block info arrays !
   *
   * Adapted from
   * @link https://drupal.org/node/1104464 Declaring the block @endlink
   *
   * 'You can use a given hook exactly once in any module,
   * so this hook must declare all blocks the module needs.'
   *
   * @return array
   *   A block info array compatible with hook_block_info().
   */
  public function block_info() {

    $blockSet = $this->factory()->newBlockSet();

    $blockSet->addBlock($this->currentPosts()->getBlock());

    $blockSet->addBlock($this->demo()->getBlock());

    // @codingStandardsIgnoreStart
    // $blockSet->addBlock($this->demoOfPageArguments()->getBlock());
    // $blockSet->addBlock($this->demoOfForms()->getBlock());
    // @codingStandardsIgnoreEnd

    return $blockSet->get();
  }

  /**
   * A current posts project.
   *
   * @var \Drupal\ooe\Demo\Adapted\ICurrentPosts
   */
  private $currentPosts;

  /**
   * Lazily creates and configures a @link CurrentPosts @endlink project.
   *
   * It does not pass on its own factory as the @link CurrentPosts @endlink
   * project configures itself with
   * a @link \Drupal\ooe\Demo\Adapted\CurrentPostsFactory @endlink.
   *
   * @return \Drupal\ooe\Demo\Adapted\ICurrentPosts
   *   A lazily created and configured current posts project.
   */
  protected function currentPosts() {
    if (empty($this->currentPosts)) {
      $this->currentPosts = new CurrentPosts(
          $this->getModule(), $this->accessArguments());
    }
    return $this->currentPosts;
  }

  /**
   * A @link Demo @endlink page-controlled project.
   *
   * @var \Drupal\ooe\Project\IControlledProject
   */
  private $demo;

  /**
   * Lazily creates and configures a demo page-controlled project.
   *
   * Uses a new @link Demo @endlink page-controlled project.
   *
   * Shares the same @link IFactory @endlink.
   *
   * The controlled page of the @link Demo @endlink serves as
   * a welcome page and grouping page for all of the OOE demos:
   * @link http://drupal7demo.webel.com.au/ooe @endlink
   *
   * @return \Drupal\ooe\Project\IControlledProject
   *   A lazily created and configured @link Demo @endlink project.
   */
  protected function demo() {
    if (empty($this->demo)) {
      $this->demo = new Demo(
          $this->getModule(),
          $this->factory(),
          $this->accessArguments(),
          $this->regionBartikSidebar1st());
    }
    return $this->demo;
  }

  /**
   * A project for demonstration of page argument extraction.
   *
   * @var \Drupal\ooe\Demo\Arguments\IDemoOfPageArguments
   */
  private $demoOfPageArguments;

  /**
   * Lazily creates and configures a @link DemoOfPageArguments @endlink.
   *
   * Shares the same @link IFactory @endlink.
   *
   * To run it please visit
   * @link http://drupal7demo.webel.com.au/ooe/demo_page_arguments @endlink
   *
   * @return \Drupal\ooe\Demo\Arguments\IDemoOfPageArguments
   *   A lazily created and configured demo of Drupal page arguments processing.
   */
  protected function demoOfPageArguments() {
    if (empty($this->demoOfPageArguments)) {
      $this->demoOfPageArguments = new DemoOfPageArguments(
          $this->getModule(), $this->factory(), $this->accessArguments(), $this->regionBartikSidebar1st());
    }
    return $this->demoOfPageArguments;
  }

  /**
   * A demo of various forms.
   *
   * Includes some Drupal7-style forms
   * with handlers in the ooe.module file, and OOE controlled forms
   * which encapsulate the form building and handlers in Class methods.
   *
   * @var \Drupal\ooe\Demo\Form\IDemoOfForms
   */
  private $demoOfForms;

  /**
   * Lazily creates a demo of various forms.
   *
   * Includes Drupal7-style forms
   * with handlers in the ooe.module file, and OOE-style controlled forms,
   * which encapsulate the form building and handlers in Class methods.
   *
   * Shares the same @link IFactory @endlink.
   *
   * @see DemoOfForms
   *
   * @return \Drupal\ooe\Demo\Form\IDemoOfForms
   *   A lazily created and configured demo of various forms.
   */
  protected function demoOfForms() {
    if (empty($this->demoOfForms)) {

      // Create:
      // @codingStandardsIgnoreStart
      //$this->demoOfForms = $this->factory()->newDemoOfForms();
      // @codingStandardsIgnoreEnd
      $this->demoOfForms = new DemoOfForms(
          $this->getModule(), $this->factory(), $this->accessArguments(), $this->regionBartikSidebar1st());

      // Configure:
      // $this->demoOfForms->set?
    }
    return $this->demoOfForms;
  }

  /**
   * A Bartik theme sidebar region.
   *
   * @var \Drupal\ooe\Layout\IRegion
   */
  private $regionBartikSidebar1st;

  /**
   * Lazily creates a Bartik theme sidebar region.
   *
   * @return \Drupal\ooe\Layout\IRegion
   *   A lazily created Bartik them sidebar region.
   */
  protected function regionBartikSidebar1st() {
    if (empty($this->regionBartikSidebar1st)) {
      $this->regionBartikSidebar1st = $this->factory()->newRegion(BartikRegions::SIDEBAR_FIRST);
    }
    return $this->regionBartikSidebar1st;
  }

  /**
   * Delegate for the implementation of hook_block_view($delta).
   *
   * Delegates to @link IBlockView @endlink implementations
   * for construction of the block view arrays !
   * It leverages @link IProject @endlink implementations
   * to provide the block views.
   *
   * @param string $delta
   *   The block delta identifer.
   *
   * @return array
   *   A block view array compatible with hook_block_view().
   */
  public function block_view($delta = '') {

    switch ($delta) {
      case $this->currentPosts()->getBlock()->getDelta():
        return $this->currentPosts()->getBlockView()->get();

      case $this->demo()->getBlock()->getDelta():
        return $this->demo()->getBlockView()->get();

      case $this->demoOfPageArguments()->getBlock()->getDelta():
        return $this->demoOfPageArguments()->getBlockView()->get();

      case $this->demoOfForms()->getBlock()->getDelta():
        return $this->demoOfForms()->getBlockView()->get();
    }
  }

  /**
   * A demo of Drupal menu tabs.
   *
   * Please note that as a point of demonstration, a concrete implementation
   * Class type has been chosen here, rather than an Interface as otherwise
   * recommended.
   *
   * All other examples here use Interface variables:
   * @link http://drupal7demo.webel.com.au/node/1804 Design-By-Contract @endlink
   *
   * @var \Drupal\ooe\Demo\Tabs\DemoOfMenuTabs
   */
  private $demoOfMenuTabs;

  /**
   * Lazily creates and configures a @link DemoOfMenuTabs @endlink.
   *
   * In this example a Class
   * without an Interface is used merely as a point of demonstration.
   *
   * All other examples here use Interface variables:
   * @link http://drupal7demo.webel.com.au/node/1804 Design-By-Contract @endlink
   *
   * To run it visit
   * @link http://drupal7demo.webel.com.au/ooe/demo/tabs @endlink
   *
   * @return \Drupal\ooe\Demo\Tabs\DemoOfMenuTabs
   *   A lazily created and configured @link DemoOfMenuTabs @endlink.
   */
  protected function demoOfMenuTabs() {
    if (empty($this->demoOfMenuTabs)) {
      $this->demoOfMenuTabs = new DemoOfMenuTabs(
          $this->getModule(), $this->factory()
      );
      $this->demoOfMenuTabs->getMenuTabs()->setWeight(-1);
    }
    return $this->demoOfMenuTabs;
  }

  /**
   * An admin menu item.
   *
   * @var \Drupal\ooe\Menu\IMenuItem
   */
  private $menuItemAdmin;

  /**
   * Lazily creates and configures an admin menu item.
   *
   * @return \Drupal\ooe\Menu\IMenuItem
   *   A lazily created admin menu item.
   */
  protected function menuItemAdmin() {

    // @todo Consider using page controlled.

    if (empty($this->menuItemAdmin)) {

      // @codingStandardsIgnoreStart
      $this->menuItemAdmin = $this->factory()->newAdminMenuItem(
          $this->getModuleDisplayName() .
          ": settings", 'admin/config/development/' .
          $this->getModule()
          // NB: MUST match configure path in module.info !
      );
      // @codingStandardsIgnoreEnd

      $this->menuItemAdmin->setDescription(
          'Configuration for ' . $this->getModuleDisplayName() . ' module'
      );
      // @codingStandardsIgnoreStart
      // 'In Drupal's menu system, 'title' and 'description'
      // attributes are automatically translated.
      // Throughout Drupal, you are encouraged to use
      // the t() function on all string literals.
      // This is one place where you must remember not to do that.'
      // @codingStandardsIgnoreEnd

      // @codingStandardsIgnoreStart
      // $menuItemAdmin->setTypeNormal(true); // Redundant, default.
      // @codingStandardsIgnoreEnd
    }
    return $this->menuItemAdmin;
  }

  /**
   * A Drupal access arguments array for those demos with access restrictions.
   *
   * @var array
   */
  private $accessArguments;

  /**
   * Lazily creates a Drupal access arguments array.
   *
   * For those demos with access restrictions.
   *
   * @return array
   *   A Drupal access arguments array for those demos with access restrictions.
   */
  protected function accessArguments() {
    if (empty($this->accessArguments)) {
      $this->accessArguments = array('access ' . $this->getModule() . ' content');
    }
    return $this->accessArguments;
  }

  /**
   * Demo menu item as private var to show more amenable to reverse in UML.
   *
   * @var \Drupal\ooe\Page\IPageMenuItem
   */
  private $demoMenuItem;

  /**
   * Delegate for the ooe_menu() implementation of hook_menu().
   *
   * Instead of using structured arrays directly, the @link OoeBridge @endlink
   * uses implementations of @link IMenuItem @endlink collected in an
   * @link IMenuItemSet @endlink, which builds the hook_menu() arrays.
   * This makes the menu building very compact, clear, and flexible.
   * Contributions to the set of menu items may in turn
   * be collected from (for example) multiple
   * @link IProject @endlink implementations.
   *
   * Adapted from the Current Posts Example:
   * @link https://drupal.org/node/1111212 Preparing for a module configuration form @endlink
   *
   * 'hook_menu() implementations return an associative array
   * whose keys define paths and whose
   * values are an associative array of properties
   * for each path. The definition for each path
   * may include a page callback function,
   * which is invoked when the registered path is requested.
   * After we have set up our form in hook_menu,
   * we will create the actual form by writing
   * the page callback function we define here.'
   *
   * @see ooe_menu()
   *
   * @return array
   *   A Drupal array compatible with hook_menu().
   */
  public function menu() {

    $menuItemSet = $this->factory()->newMenuItemSet();

    // Add an admin menu item to the menu item set.
    // NOTE: this will ultimately place it with
    // an explicit key in the return array.
    $menuItemSet->addMenuItem($this->menuItemAdmin());

    // Demonstrate the OO adaptation of the 'current_posts' example before
    // the other demos since it does not use a page controller,
    // and the original is well known.
    $menuItemSet->addMenuItem($this->currentPosts()->getMenuItem());

    // The welcome page(s) for all OOE demos (uses a page controller).
    // As a class var so can demonstrate IDE prompting.
    $this->demoMenuItem = $this->demo()->getMenuItem();
    $this->demoMenuItem->setExpanded(TRUE);
    $menuItemSet->addMenuItem($this->demoMenuItem);

    // Here entire sets of menu items are added via IProject implementations.
    $menuItemSet->addMenuItems($this->demoOfMenuTabs()->getMenuItems());
    $menuItemSet->addMenuItems($this->demoOfPageArguments()->getMenuItems());
    $menuItemSet->addMenuItems($this->demoOfForms()->getMenuItems());

    // This will build the entire menu array compatible with hook_menu().
    return $menuItemSet->get();
  }

  /**
   * Delegate for a page callback for admin settings.
   *
   * Currently only for Current Posts.
   *
   * Adapted from the Current Post example:
   * @link https://drupal.org/node/1111260 Creating the configuration form @endlink
   *
   * The matching delegating function was set in the .module file's
   * @link ooe_menu @endlink() implementation of hook_menu() via
   * @link OoeBridge::menu @endlink().
   *
   * @param array $form
   *   A Drupal form array.
   * @param array $form_state
   *   A Drupal form state array.
   *
   * @return array
   *   The admin form structure with the Current Posts settings.
   */
  public function form_admin(array $form, array &$form_state) {
    return $this->currentPosts()->getFormAdmin($form, $form_state);
  }

  /**
   * Delegate for validation of the admin settings form.
   *
   * Currently only uses an @link ICurrentPosts @endlink project.
   *
   * Adapted from the Current Posts example:
   * @link https://drupal.org/node/1111280 Validating the data @endlink
   *
   * 'Notice the $form_state array variable,
   * an argument here and also for our form function.
   * $form_state is passed by reference along through
   * each stage of form processing to capture information
   * about the form's workflow and its current state.
   * Incoming $_POST data is first sanitized and checked
   * against the structure of the form before being handed off
   * to validate and submit handlers.
   * The array's values key is the default key for storing this data.
   *
   * See drupal_build_form()  for a list of $form_state keys.'
   *
   * @param array $form
   *   A structured array containing the elements and properties of the form.
   * @param array $form_state
   *   An array that stores information about the form's current state
   *   during processing.
   */
  public function form_admin_validate(array $form, array &$form_state) {
    return $this->currentPosts()->validateAdmin($form, $form_state);
  }

  /**
   * Delegate for the ooe_permission() implementation of hook_permission().
   *
   * Adapted from the Current Posts example:
   * @link https://drupal.org/node/1118210 Specifying a custom permission for a new page @endlink.
   *
   * @todo OOE Access array encapsulation.
   *
   * @return array
   *   A Drupal permissions array
   */
  public function permission() {
    return array(
      'access ' . $this->getModule() . ' content' => array(
        'title' => t('Access content for the !module-display-name module',
            array('!module-display-name' => $this->getModuleDisplayName())),
        // @codingStandardsIgnoreStart
        // https://www.drupal.org/node/322732
        // Dynamic strings with placeholders (recommends prefer hiphens).
        // @codingStandardsIgnoreEnd
      ),
    );
    // @codingStandardsIgnoreStart
    // Coder: WARNING | Translatable strings must not begin or end
    // with white spaces use placeholders with t() for variables.
    //
    // Coder: ERROR | Concatenating translatable strings is not allowed,
    // use placeholders instead and only one string literal.
    // @codingStandardsIgnoreEnd
  }

  /**
   * Delegate page callback function for Current Posts.
   *
   * Declared in ooe_menu() via @link OoeBridge::menu @endlink().
   *
   * Adapted from the Current Posts example:
   * @link https://drupal.org/node/1118218 Theming the page @endlink
   *
   * This OOE version encapsulates the database query and the
   * page construction in a @link CurrentPosts @endlink implementation
   * of an @link IProject @endlink and a
   * @link \Drupal\ooe\Demo\Adapted\CurrentPostsHelper @endlink.
   *
   * Visit also
   * @link https://drupal.org/node/930760 Render Arrays in Drupal 7 @endlink
   *
   * @return array
   *   A Drupal render array for displaying the current posts.
   */
  public function page_current_posts() {
    return $this->currentPosts()->getPage();
  }

  /**
   * Encapsulates the name of a delegate page callback.
   *
   * For use in the .module file.
   *
   * The reason this is included in this bridge class (rather than say directly
   * as a string in the .module file that is a client of this) is that if the
   * delegate callback method name changes here then clients will automatically
   * catch the change (after the
   * @link DRY http://drupal7demo.webel.com.au/node/10 @endlink
   * coding principle).
   */
  const FUNC_DEMO_PAGE_ARGUMENTS_AUTO = 'demo_page_arguments_auto';

  /**
   * Delegate for a page callback to a demo of auto argument extraction.
   *
   * It delegates to an @link IDemoOfPageArguments @endlink project,
   * which leverages @link DemoArguments @endlink to
   * format a standard arguments extraction test page.
   *
   * From hook_menu():
   * 'Note that this automatic passing of optional path arguments
   * applies only to page and theme callback functions.'
   *
   * There is no need here to specify explicit parameters
   * matching the original .module file page callback,
   * as all arguments will simply be forwarded.
   *
   * @see func_get_args()
   *
   * @see call_user_func_array();
   *
   * @return array
   *   A render array page with diagnostics on the argument extraction.
   */
  public function demo_page_arguments_auto() {
    return call_user_func_array(
        array(
          $this->demoOfPageArguments(),
          IDemoOfPageArguments::FUNC_PAGE_ARGUMENTS_AUTO,
        ), func_get_args()
    );
  }

  /**
   * Encapsulates the name of a delegate page callback.
   *
   * For use in the .module file.
   *
   * The reason this is included in this bridge class (rather than say directly
   * as a string in the .module file that is a client of this) is that if the
   * delegate callback method name changes here then clients will automatically
   * catch the change (after the
   * @link DRY http://drupal7demo.webel.com.au/node/10 @endlink coding principle).
   */
  const FUNC_DEMO_PAGE_ARGUMENTS_FORCED = 'demo_page_arguments_forced';

  /**
   * Delegate for a page callback to a demo of forced arguments.
   *
   * It delegates to an @link IDemoOfPageArguments @endlink project,
   * which leverages @link DemoArguments @endlink to
   * format a standard arguments extraction test page.
   *
   * Forced arguments are explained at hook_menu().
   *
   * There is no need here to specify explicit parameters
   * matching the .module file page callback
   * as all arguments will simply be forwarded.
   *
   * @see func_get_args()
   *
   * @see call_user_func_array();
   *
   * @return array
   *   A render array with diagnostics on the argument extraction.
   */
  public function demo_page_arguments_forced() {
    return call_user_func_array(
        array(
          $this->demoOfPageArguments(),
          IDemoOfPageArguments::FUNC_PAGE_ARGUMENTS_FORCED,
        ),
        func_get_args()
    );
  }

  /**
   * Delegate for a Drupal7-style (uncontrolled) form.
   *
   * Adapted from the main Drupal7 form examples: form_example_tutorial_7();
   * Delegates in turn
   * to @link IFormManager::getForm @endlink($form, &$form_submit)
   * via an @link IDemoOfForms @endlink project.
   *
   * @param array $form
   *   A Drupal form array.
   * @param array $form_submit
   *   A Drupal form submission array.
   *
   * @return array
   *   A Drupal form array.
   *
   * @see ooo_form_demo
   */
  public function form_demo(array $form, array &$form_submit) {
    return $this->demoOfForms()->build($form, $form_submit);
  }

  /**
   * Delegate for the (non-controlled) form validation example.
   *
   * Adapted from form_example_tutorial_6_validate($form, &$form_state);
   * delegates in turn to
   * @link IFormManager::validate @endlink($form, &$form_state)
   * via an @link IDemoOfForms @endlink.
   *
   * 'Now we add a handler/function to validate the data entered into the ..'
   * form.
   *
   * @see ooe_form_demo_validate
   *
   * @param array $form
   *   A Drupal form array.
   * @param array $form_state
   *   A Drupal form state array.
   */
  public function form_demo_validate(array $form, array &$form_state) {
    $this->demoOfForms()->validate($form, $form_state);
  }

  /**
   * Delegate for the (non-controlled) submit function example.
   *
   * Adapted from form_example_tutorial_7();
   * delegates in turn to
   * @link IFormManager::submit @endlink($form, &$form_state)
   * via an @link IDemoOfForms @endlink.
   *
   * 'Adds a submit handler/function to our form to send a successful
   * completion message to the screen.'
   *
   * @see ooe_form_demo_submit
   *
   * @param array $form
   *   A Drupal form array.
   * @param array $form_state
   *   A Drupal form state array.
   */
  public function form_demo_submit(array $form, array &$form_state) {
    $this->demoOfForms()->submit($form, $form_state);
  }

}
