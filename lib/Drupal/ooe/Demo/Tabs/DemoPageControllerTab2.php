<?php

/**
 * @file
 * DemoPageControllerTab2.
 */

namespace Drupal\ooe\Demo\Tabs;

use Drupal\ooe\Demo\Tabs\AbstractPageControllerTabsShared;

/**
 * Page controller for the 2nd tab.
 *
 * @author darrenkelly
 */
class DemoPageControllerTab2 extends AbstractPageControllerTabsShared {

  /**
   * Constructor.
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * Builds the page render array using OOE Render objects.
   *
   * @return array
   *   A Drupal render array.
   */
  public function page() {
    return $this->pageFor(t('2nd'));
  }


  /**
   * Builds a page array directly (non OO counter-example).
   *
   * English only, no translation.
   *
   * @return array
   *   A Drupal render array.
   */
  public function pageOldNonOo() {
    $page['info'] = array(
      '#markup' => "<h3>A controlled page for the 2nd tab</h3>" .
      "<p>Note how this tab page does not have a menu link !</p>",
    );
    return $page;
  }

}
