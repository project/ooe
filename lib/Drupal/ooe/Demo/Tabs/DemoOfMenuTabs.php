<?php

/**
 * @file
 * DemoOfMenuTabs.
 */

namespace Drupal\ooe\Demo\Tabs;

use Drupal\ooe\Module\DefaultModuleHelper;

use Drupal\ooe\Factory\IFactory;

/**
 * Demonstrates @link MenuTabs @endlink.
 *
 * Convention: This is called DemoOfMenuTabs rather than DemoMenuTabs
 * as it is not an IMenuTabs (it uses an IMenuTabs).
 *
 * It is extremely important that you examine this demonstration class
 * in conjunction with the explanation and UML diagram(s) at
 * @link http://drupal7demo.webel.com.au/node/1019 @endlink
 * as it deliberately includes some examples of HOW NOT TO DO THINGS
 * when working in general with the UML-friendly Webel coding recipe for OOE.
 *
 * Note also that this example is NOT an @link IProject @endlink;
 * most other examples in the OOE suite leverage @link IProject @endlink
 * as a starting point, but you are under no obligation to do so.
 *
 * @author darrenkelly
 */
class DemoOfMenuTabs extends DefaultModuleHelper {

  /**
   * The menu tabs.
   *
   * @var \Drupal\ooe\Menu\IMenuTabs
   */
  private $menuTabs;

  /**
   * The menu tabs.
   *
   * @return \Drupal\ooe\Menu\IMenuTabs
   *   The menu tabs.
   */
  public function getMenuTabs() {
    return $this->menuTabs;
  }

  /**
   * Constructor: fetches an @link IMenuTabs @endlink from a factory.
   *
   * Creates some tabs with page controllers.
   *
   * This example deliberately pragmatically uses inline "new" creation
   * of page controllers passed directly to the tab creation method
   * (instead of private variables with lazy creation/configuration methods
   * for each of the page controllers).
   *
   * The approach here is NOT graphical UML-friendly, and is only done
   * to show you WHAT NOT TO DO when working with OOE as explained here:
   *
   * @link http://drupal7demo.webel.com.au/node/1019 @endlink
   *
   * @link http://drupal7demo.webel.com.au/node/1177 @endlink
   *
   * @param string $module
   *   The machine name of the module.
   * @param IFactory $factory
   *   Optional factory; if none given or null
   *   a @link DefaultFactory @endlink will be used.
   */
  public function __construct($module, IFactory $factory = NULL) {
    parent::__construct($module, $factory);

    $path = $module . '/demo/tabs';
    // @todo ENCAPSULATE!

    $this->menuTabs = $this->factory()->newMenuTabs(
        $path,
        new DemoPageControllerMain(),
        // Pseudo local page controller is not UML-friendly.
        $this->getModuleDisplayName() . ": Tabs demo"
            );

    $this->menuTabs->newTabMenuItem(
            new DemoPageControllerTab2(),
            // Pseudo local page controller is not UML-friendly.
            "Tab2"
            );

    $this->menuTabs->newTabMenuItem(
            new DemoPageControllerTab3(),
            // Pseudo local page controller is not UML-friendly.
            "Tab3"
            );
  }

  /**
   * All of the menu items of this.
   *
   * @return \Drupal\ooe\Menu\IMenuItem[]
   *   All of the menu items of this.
   */
  public function getMenuItems() {
    return $this->menuTabs->getMenuItems();
  }

}
