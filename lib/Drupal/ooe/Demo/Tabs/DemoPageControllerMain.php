<?php

/**
 * @file
 * DemoPageControllerMain.
 */

namespace Drupal\ooe\Demo\Tabs;

use Drupal\ooe\Page\AbstractPageController;
use Drupal\ooe\Common;

/**
 * Demonstration page controller for the main page of a group of tabs.
 *
 * @author darrenkelly
 */
class DemoPageControllerMain extends AbstractPageController {

  /**
   * Constructor.
   */
  public function __construct() {
    parent::__construct(Common::MODULE);
  }

  /**
   * Implemented to return the name of the eventual callback method.
   *
   * @return string
   *   The callbackname.
   */
  public function getCallbackName() {
    return 'page';
  }

  /**
   * Builds the page render array using OOE Render objects.
   *
   * @return array
   *   A Drupal render array.
   */
  public function page() {

    $page = $this->rf()->newSet();

    $page->addItem('subheader', $this->rf()->newH(t('The main (default) controlled page for the tabs'), 3));
    $lTab1 = l(t('1st Tab'), 'ooe/demo/tabs/tab1');
    $lMain = l(t('Tabs demo'), 'ooe/demo/tabs');

    $page->addItem('pSameTarget', $this->rf()->newSetP()->addItem(
            'iSameTarget', $this->rf()->newI(
                t('Note how this is the same target for the !link-tab1 and the main !link-main menu link.',
                    array('!link-tab1' => $lTab1, '!link-main' => $lMain)
                    )
                )
        )
    );

    $class = get_class($this);
    $page->addItem('pClass', $this->rf()->newSetP()->addItem(
            'iClass', $this->rf()->newI(
                t('This page was built by the class: !class', array('!class' => $class))
                )
        )
    );

    $hr = $this->rf()->newHr();
    // Reusable !

    $page->addItem('hr1', $hr);

    $page->addItem('explain', $this->rf()->newP(
            t('In OOE the MenuTabs class (used by DemoOfMenuTabs) does the hard work for you.') . '<br/>' .
            t('For each tab you just provide a dedicated IPageController instance, except for the') . '<br/>' .
            t('first (default) tab and the main page, which share a single page controller') . '<br/>' .
            t('that is passed to the MenuTabs constructor.')
        )
        // @todo Issue: The splitting of t() across text portions broken by
        // <br/> is not ideal. See also https://www.drupal.org/node/322774.
    );

    $page->addItem('hr2', $hr);

    $lDrupalMenu = l(t('Drupal7 API: hook_menu()'),
        'https://api.drupal.org/api/drupal/modules!system!system.api.php/function/hook_menu/7');
    $page->addItem(
        'external', $this->rf()->newSetP()->addItem(
            'lDrupalMenu', $this->rf()->newI(
                t('For an explanation of hand-coded tabs visit !link-drupal-hook-menu',
                    array('!link-drupal-hook-menu' => $lDrupalMenu))
                )
        )
    );

    return $page->get();
  }

  /**
   * Old non-OO form as counter-example.
   *
   * English only, no translation.
   *
   * @return string
   *   A Drupal page array.
   */
  public function pageOldNonOo() {
    $page['info'] = array(
      '#markup' =>
      '<h3>The main (default) controlled page for the tabs</h3>' .
      '<p>Note how this is the same target for the <a href="/ooe/demo/tabs/tab1">1st Tab</a> and the main <a href="/ooe/demo/tabs">Tabs demo</a> menu link.</p>' .
      '<p>' .
      'In OOE the MenuTabs class (used by DemoOfMenuTabs) does the hard work for you.<br/>' .
      'For each tab you just provide a dedicated IPageController instance, except for the <br/>' .
      'first (default) tab and the main page, which share a single page controller<br/>' .
      'that is passed to the MenuTabs constructor.<br/>' .
      '</p>' .
      '<hr/><p><i>For an explanation of hand-coded tabs visit <a href="https://api.drupal.org/api/drupal/modules!system!system.api.php/function/hook_menu/7">Drupal7 API: hook_menu()</a></i></p>',
    );
    return $page;
  }

}
