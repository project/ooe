<?php

/**
 * @file
 * AbstractPageControllerTabsShared.
 */

namespace Drupal\ooe\Demo\Tabs;

use Drupal\ooe\Page\AbstractPageController;
use Drupal\ooe\Common;

/**
 * Collects aspects of tabs demo common to Tab2 and Tab3.
 *
 * @author darrenkelly
 */
abstract class AbstractPageControllerTabsShared extends AbstractPageController {

  /**
   * Constructor.
   */
  protected function __construct() {
    parent::__construct(Common::MODULE);
  }

  /**
   * Implemented to return the name of the eventual callback method.
   *
   * @return string
   *   The callback method name.
   */
  final public function getCallbackName() {
    return 'page';
  }

  /**
   * Builds a page render array using OOE @link IRender @endlink objects.
   *
   * Render objects obtained via a @link IRenderFactory @endlink.
   *
   * @param string $tab
   *   Ordinal, translated, name of the tab (for example, '2nd' or '3rd').
   *
   * @return array
   *   A Drupal render array.
   */
  final protected function pageFor($tab) {
    $page = $this->rf()->newSet();

    $page->addItem('subheader', $this->rf()->newH(
            t('A controlled page for the !tab-order tab', array('!tab-order' => $tab)), 3));

    $class = get_class($this);
    $page->addItem(
        'pClass', $this->rf()->newSetP()->addItem(
            'iClass', $this->rf()->newI(
                t('This page was built by the class: !class', array('!class' => $class))
            )
        )
    );
    $page->addItem('hr', $this->rf()->newHr());

    $page->addItem('no-link', $this->rf()->newP(
            t('Note how this tab page does not have a menu link !')
        )
    );

    return $page->get();
  }

}
