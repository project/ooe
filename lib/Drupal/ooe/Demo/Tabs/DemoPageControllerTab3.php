<?php

/**
 * @file
 * DemoPageControllerTab3.
 */

namespace Drupal\ooe\Demo\Tabs;

use Drupal\ooe\Demo\Tabs\AbstractPageControllerTabsShared;

/**
 * Page controller for the 3rd tab.
 *
 * @author darrenkelly
 */
class DemoPageControllerTab3 extends AbstractPageControllerTabsShared {

  /**
   * Constructor.
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * Builds the page render array using OOE Render objects.
   *
   * @return array
   *   A Drupal render array.
   */
  public function page() {
    return $this->pageFor(t('3rd'));
  }

  /**
   * Builds a page array directly (non OO counter-example).
   *
   * English only, no translation.
   *
   * @return array
   *   A Drupal render array.
   */
  public function pageOldNonOo() {
    $page['info'] = array(
      '#markup' => "<h3>A controlled page for the 3rd tab</h3>",
    );
    return $page;
  }

}
