<?php

/**
 * @file
 * WebelD7bartikCSS.
 */

namespace Drupal\ooe\Demo\Theme;

/**
 * Some constants for CSS classes used by the Webel IT Australia
 * 'd7bartik' theme (adapted from the Drupal7 Bartik theme)
 * as used at @link http://drupal7demo.webel.com.au @endlink.
 *
 * @author darrenkelly
 */
class WebelD7bartikCSS {

  const WARN = 'warn';

  const CENTER = 'CENTER';

  const LI_DEMO = 'li-demo';

  const DEMO = 'demo';

  const STUB = 'stub';

  const ERROR = 'error';

}
