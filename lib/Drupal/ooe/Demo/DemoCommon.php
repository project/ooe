<?php

/**
 * @file
 * DemoCommon.
 */

namespace Drupal\ooe\Demo;

use Drupal\ooe\Render\RenderHelper;

/**
 * Collects markup and markup render objects shared by many demos.
 *
 * Also explores ways of sharing Drupal-translatable strings
 * in order to respect the
 * @link Don't Repeat Yourself (DRY) http://en.wikipedia.org/wiki/Don't_repeat_yourself @endlink
 * principle.
 *
 * @author darrenkelly
 */
class DemoCommon extends RenderHelper {

  // @codingStandardsIgnoreStart
  // const MARKUP_TIP_HOVER_LINKS
  //  = 'For more information on each demo please hover the mouse over each link:';
  // No ! Can't be Drupal-translated using t(self::MARKUP_TIP_HOVER_LINKS).
  // const MARKUP_TIP_HOVER_LINKS
  //  = t('For more information on each demo please hover the mouse over each link:');
  // No ! PHP const can't be result of calculation.
  // http://php.net/manual/en/language.oop5.constants.php
  // final static private $MARKUP_TIP_HOVER_LINKS = ..
  // No ! Unlike Java, PHP does not support final on static class variables.
  // http://php.net/manual/en/language.oop5.final.php
  // static private $MARKUP_TIP_HOVER_LINKS =
  //    t('For more information on each demo please hover the mouse over each link:');
  // No ! PHP5.3 does not support calculation in initializers.
  // @codingStandardsIgnoreEnd

  /**
   * For demonstration of complex static init in PHP5.3.
   *
   * It will be initialized with a Drupal-translated string.
   *
   * Note that because it can't be set to final, it must
   * remain private, otherwise the outside world could
   * change it after initialization.
   *
   * @var string
   */
  static private $T_MARKUP_TIP_HOVER_LINKS;

  /**
   * Demonstrates initialising complex static class variables.
   *
   * Used here to initialize a shareable Drupal-tranlsated string.
   *
   * @link http://stackoverflow.com/questions/693691/how-to-initialize-static-variables @endlink
   */
  static public final function init() {
    self::$T_MARKUP_TIP_HOVER_LINKS = t('For more information on each demo please hover the mouse over each link:');
  }

  /**
   * A translated shareable message about info when hovering mouse over links.
   *
   * One way of sharing a Drupal-translatable string (DRY principle).
   *
   * @return string
   *   A translated shareable message.
   */
  static public final function tMarkupTipHoverLinks() {
    return t('For more information on each demo please hover the mouse over each link:');
    // Could be combined with lazy init on a private static class variable.
  }

  /**
   * For demonstration of complex static init in PHP5.3.
   *
   * It will be lazily initialized with a Drupal-translated string.
   *
   * @var string
   */
  static private $T_MARKUP_TIP_HOVER_LINKS_LAZY;

  /**
   * A translated shareable message about info when hovering mouse over links.
   *
   * One way of sharing a Drupal-translatable string (DRY principle).
   *
   * This version uses lazy initialization.
   *
   * @return string
   *   A translated shareable message.
   */
  static public final function tMarkupTipHoverLinksLazy() {
    if (!isset(self::$T_MARKUP_TIP_HOVER_LINKS_LAZY)) {
      self::$T_MARKUP_TIP_HOVER_LINKS_LAZY = t('For more information on each demo please hover the mouse over each link:');
    }
    return self::$T_MARKUP_TIP_HOVER_LINKS_LAZY;
  }

  /**
   * A render object with markup for a tip about hovering over demo links.
   *
   * @var \Drupal\ooe\Render\IRender
   */
  static private $renderTipHoverLinks;

  /**
   * Lazily creates a render object for a links hover tip.
   *
   * With markup for a tip about hovering over demo links.
   *
   * @return \Drupal\ooe\Render\IRender
   *   A render object with markup for a tip about hovering over demo links.
   */
  static public function renderTipHoverLinks() {
    if (!isset(self::$renderTipHoverLinks)) {

      // @codingStandardsIgnoreStart
      //
      // Alternative1
      // $tip = self::$T_MARKUP_TIP_HOVER_LINKS;
      //
      // Alternative2
      // $tip = self::tMarkupTipHoverLinks();
      //
      // @codingStandardsIgnoreEnd

      // Alternative3
      $tip = self::tMarkupTipHoverLinksLazy();

      self::$renderTipHoverLinks = self::rf()->newSetP()->addItem(
          'tip', self::rf()->newR($tip, self::tf()->newI())
      );
    }
    return self::$renderTipHoverLinks;
  }

}

DemoCommon::init();
// So complex static class variables initialized.
// http://stackoverflow.com/questions/693691/how-to-initialize-static-variables
