<?php

/**
 * @file
 * DemoPageController.
 */

namespace Drupal\ooe\Demo\Page;

use Drupal\ooe\Page\AbstractPageController;
use Drupal\ooe\Demo\Arguments\DemoArgumentsHelper;
use Drupal\ooe\Demo\Arguments\DemoOfPageArguments;
use Drupal\ooe\Common;

/**
 * Demonstrates an @link IPageController @endlink implementation
 * with a page callback that takes some demonstration arguments.
 *
 * @author darrenkelly
 */
class DemoPageController extends AbstractPageController {

  const DEMO_NAME = 'controlled';

  // @codingStandardsIgnoreStart
  /* NO: PageController expect noargs constructor
   * but can't overload constructors !
   * See also:
   * http://stackoverflow.com/questions/1699796/best-way-to-do-multiple-constructors-in-php
   * public function __construct($module) {
   * parent::__construct($module);
   * }
   */
  // @codingStandardsIgnoreEnd

  /**
   * Constructor.
   */
  public function __construct() {
    parent::__construct(Common::MODULE);
  }

  /**
   * Overridden to return the name of the eventual callback method.
   *
   * @return string
   *   The name of the ultimate callback method.
   */
  public function getCallbackName() {
    return 'page';
    // No need to encapsualate as a const since will never be used elsewhere.
  }

  /**
   * Delegate callback; builds an argument extraction demonstration page.
   *
   * Uses the @link DemoArguments @endlink helper class.
   *
   * This demo  callback method has 2 named parameters with defaults.
   * This is to emphasise that no assumptions are made about either the
   * ultimate callback method's name or about the number
   * of parameters it has (number of arguments it can process).
   *
   * (For a minimal demo with no args visit @link OoePageController @endlink.)
   *
   * Visit also:
   * - @link http://en.wikipedia.org/wiki/Foobar Foobar @endlink
   * - func_get_args()
   *
   * @return array
   *   A Drupal page render array
   */
  public function page($foo = '[foo]', $bar = '[bar]') {

    $stem = DemoOfPageArguments::stemDemoPageArgs() . '/' . self::DEMO_NAME;

    $demo = new DemoArgumentsHelper($stem);

    $demo->setMapArgs(
        array(
          'foo' => $foo,
          'bar' => $bar,
        )
    );

    $demo->setTestArgLinks(
        array(
          NULL,
          '1st',
          '1st/2nd',
          '1st/2nd/3rd',
        )
    );

    return $demo->getPage(func_get_args());
  }

}
