<?php

/**
 * @file
 * CurrentPostsHelper.
 */

namespace Drupal\ooe\Demo\Adapted;

use Drupal\ooe\Common;
use Drupal\ooe\Render\Render;
use Drupal\ooe\Render\Tag\RenderH;
use Drupal\ooe\Render\RenderList;
use Drupal\ooe\Render\RenderSet;
use Drupal\ooe\Render\ListItems;
use Drupal\ooe\Render\ListItem;

/**
 * The well known 'current_posts' adapted to more OO form.
 *
 * Adapted to a (more) OO form from the well-known
 * Drupal module development 'current_posts' demo:
 * @link https://drupal.org/node/1104482 Retreiving data @endlink.
 *
 * @author darrenkelly
 */
class CurrentPostsHelper {

  /**
   * Display option for a block.
   *
   * @var
   */
  const DISPLAY_BLOCK = 'block';

  /**
   * Display option for a page.
   */
  const DISPLAY_PAGE = 'page';


  // @todo how inject module name.

  // @codingStandardsIgnoreStart
  //const SETTING_POSTS_MAX = Common::MODULE . '_current_posts_max';
  // FAILS: one can't do computation in a const declaration !
  //
  //static private $SETTING_POSTS_MAX
  // = Common::MODULE_NAME . '_current_posts_max';
  // @todo: FAILS: Why ?
  // @codingStandardsIgnoreEnd

  /**
   * Admin setting variable name.
   */
  const SETTING_POSTS_MAX = 'ooe_current_posts_max';

  /**
   * Number of years to fetch posts.
   *
   * @var int
   */
  static private $NUM_YEARS = 5;

  /**
   * Set beginning and end dates, retrieve posts from database.
   *
   * Visit also:
   * @link https://drupal.org/node/1104482 Retreiving data @endlink,
   * @link http://au1.php.net/manual/en/function.getdate.php getdate() @endlink,
   * @link http://au1.php.net/mktime mktime(hour, minute, second, month, day, year, is_dst) @endlink.
   *
   * @param string $display
   *   The display type.
   *
   * @return \SelectQuery
   *   A result set of the targeted posts.
   */
  static public function getContents($display) {

    // Get today's date.
    $today = getdate();

    // @codingStandardsIgnoreStart
    // ORIGINAL: Calculate the date a week ago.
    //$start_time = mktime(0, 0, 0, $today['mon'], ($today['mday'] - 7), $today['year']);
    // @codingStandardsIgnoreEnd

    // Webel: Calculate the date some years ago
    // so at least something always shows.
    $start_time = mktime(0, 0, 0, $today['mon'], $today['mday'], $today['year'] - self::$NUM_YEARS);

    // Get all posts from the given time period ago to the present.
    $end_time = time();

    $max_num = variable_get(self::SETTING_POSTS_MAX, 3);

    // Use Database API to retrieve current posts.
    $query = db_select('node', 'n')
        // @codingStandardsIgnoreStart
        //->fields('n', array('nid', 'title', 'created'))// ORIGINAL.
        // @codingStandardsIgnoreEnd
        // Webel: load 'type' already.
        ->fields('n', array('nid', 'title', 'created', 'type'))
        // Webel: exclude [api] pseudo-node type.
        ->condition('n.type', 'api', '<>')
        // Published.
        ->condition('status', 1)
        ->condition('created', array($start_time, $end_time), 'BETWEEN')
        // Most recent first.
        ->orderBy('created', 'DESC');

    if ($display == self::DISPLAY_BLOCK) {
      // Restrict the range if called with 'block' argument.
      $query->range(0, $max_num);
    }
    return $query->execute();
  }

  /**
   * Adapted to create a page for the 'current posts' example.
   *
   * Note that this version creates and uses OOE Render classes directly.
   * (We could have also used an
   * @link \Drupal\ooe\Render\IRenderFactory @endlink,
   * but for this first demonstration the render objects are created directly.)
   *
   * Compare with @link getPageNonOO() @endlink
   * (which uses explicit render array keys)
   * and note how much more concise, safe, and clearer this OO version is.
   * At all stages here the OOE Classes/Methods and IDE guide you
   * through the  process of building valid Drupal render arraya.
   *
   * Visit also
   * @link https://drupal.org/node/930760 Render Arrays in Drupal 7 @endlink
   *
   * @param \DatabaseStatementInterface $result
   *   The query results.
   *
   * @return array
   *   A Drupal render array.
   */
  static public function getPage(\DatabaseStatementInterface $result) {

    $page = new RenderSet();

    // Additional H2 subheader.
    $subheader = new RenderH(
        t('!module-display-name: Render objects version', array('!module-display-name' => Common::MODULE_NAME)), 2);
    $page->addItem('subheader', $subheader);

    // Array to contain items for the page to render.
    $listItems = self::makeNodeResultLinksListItems($result);

    $key = Common::MODULE . '_current_posts';
    // Webel:DRY!

    if (!$listItems->hasItems()) {
      // No content in the last time period.

      $noposts = new Render(
          t('No posts available for the time period !number-of-years years', array('!number-of-years' => self::$NUM_YEARS)
      ));
      $page->addItem($key, $noposts);
    }
    else {
      $postsList = new RenderList(
          $listItems,
          // 'All posts from the last week (excluding API pseudo-nodes)'
          t('All posts from the last !number-of-years years (excluding API pseudo-nodes)', array('!number-of-years' => self::$NUM_YEARS)
          )
      );
      $postsList->setThemeHookSuggestionModule(Common::MODULE);
      // !

      $page->addItem($key, $postsList);
    }
    return $page->get();
  }

  /**
   * Iterate over the result set and format as links.
   *
   * Encapsulated in an object-oriented list items manager.
   *
   * @return \Drupal\ooe\Render\IListItems
   *   A list items group.
   */
  static public function makeNodeResultLinksListItems(\DatabaseStatementInterface $result) {
    $items = new ListItems();
    // @todo factory
    // Iterate over the resultset and format as links.
    foreach ($result as $node) {
      // $node = node_load($node->nid);
      // So can access type. Not needed if load in results.

      $data = '[' . $node->type . '] ' . l($node->title, 'node/' . $node->nid);
      // Webel: Adapted to indicate type.

      $li = new ListItem($data);
      $items->addItem($li);
    }
    return $items;
  }

  /**
   * Iterate over the result set and format as links.
   *
   * @return array
   *   A list items array suitable for theme_item_list().
   */
  static public function makeNodeResultLinks(\DatabaseStatementInterface $result) {
    $items = array();
    // Iterate over the resultset and format as links.
    foreach ($result as $node) {

      // @codingStandardsIgnoreStart
      //$node = node_load($node->nid);
      // So can access type. Not needed if load in results.
      // @codingStandardsIgnoreEnd

      $items[] = array(
        // @codingStandardsIgnoreStart
        // 'data' => l($node->title, 'node/' . $node->nid),
        // @codingStandardsIgnoreEnd
        'data' => '[' . $node->type . '] ' . l($node->title, 'node/' . $node->nid),
        // Adapted to indicate type.
      );
    }
    return $items;
  }

  /**
   * Adapted to create a page for the 'current posts' example (non OO version).
   *
   * Note that this version still uses explicit render array keys
   * (it does not use OOE @link IRender @endlink classes).
   * Please compare with @link CurrentPostsHelper::getPage @endlink()
   * (which uses OOE Render classes) and observe how much more
   * error prone this Drupal-style version is, because it relies on
   * the coder correctly remembering Drupal "by convention" array keys.
   *
   * This coding style also does not lend itself to IDE prompting.
   *
   * Visit also:
   * @link https://drupal.org/node/930760 Render Arrays in Drupal 7 @endlink
   *
   * @param \DatabaseStatementInterface $result
   *   The query result.
   *
   * @return string
   *   A Drupal page array.
   */
  static public function getPageNonOo(\DatabaseStatementInterface $result) {

    // Array to contain items for the page to render.
    $items = self::makeNodeResultLinks($result);

    $page_array = array();
    $key_page_array = Common::MODULE . '_arguments';
    // Webel:DRY !

    if (empty($items)) {
      // No content in the last time period.
      $page_array[$key_page_array] = array(
        // '#title' => t($title),
        // 'Title serves as page subtitle' (https://drupal.org/node/1118218)
        // https://drupal.org/node/1118218#comment-8315581
        // Webel: ERROR ! Ignored: there is no title since not themed.
        '#markup' => t('No posts available.'),
      );
      return $page_array;
    }
    else {
      $title = t('All posts from the last !number-of-years years', array('!number-of-years' => self::$NUM_YEARS));
      $page_array[$key_page_array] = array(
        // Webel: title serves as the LIST subtitle !
        '#title' => $title,
        // Adapted from: https://drupal.org/node/1118218
        // 'The next two lines are the render
        // array approach to calling theme_item_list(),
        // the same function we called in ooe_block_view()
        // with the following code:
        // theme('item_list', array('items' => $items));.
        // The variable $items, with its array of links,
        // is assigned to the items property,
        // and the theme property is assigned its item list default theme hook.'
        '#items' => $items,
        // Theme hook with suggestion.
        '#theme' => 'item_list__' . Common::MODULE,
          // 'There is a notable difference.
          // We have added two underscores and our module name
          // to the item list theme hook call. By doing this,
          // we have given more flexibility to
          // any themer using the module. If we use only the default theme hook,
          // any change a themer
          // makes to that default will appear wherever
          // the hook is used, not just on our item list.
          // With this addition, a theme can specify markup for our output only.
          //
      // The two underscores tell Drupal that this
          // is a theme hook suggestion, using a pattern
          // that Drupal recognizes. You can write as many
          // suggestions as you like. If you need more,
          // add additional suggestions preceded by double underscores.
          // Drupal will start looking for
          // an implementation of the suggestion on the right,
          // then continue checking right to left
          // until it finds an actual implementation.
          // This module doesn't implement the suggestion,
          // so the look of the output won't change.
          // But the option is now available.'
          // Webel: note that in the OO version of this the
          // double underscore __ theme hint policy is encapsulated !
      );
      return $page_array;
    }
  }

}
