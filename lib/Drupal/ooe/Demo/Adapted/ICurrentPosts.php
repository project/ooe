<?php

/**
 * @file
 * ICurrentPosts.
 */

namespace Drupal\ooe\Demo\Adapted;

use Drupal\ooe\Project\IProject;

/**
 * Encapsulates the Current Posts example as an OOE project.
 *
 * @author darrenkelly
 */
interface ICurrentPosts extends IProject {

  /**
   * A Drupal page render array.
   *
   * @return array
   *   A Drupal page render array.
   */
  public function getPage();

  /**
   * Delegate for page callback for drupal_get_form(): settings.
   *
   * @param array $form
   *   A Drupal form array.
   * @param array $form_state
   *   A Drupal form state array.
   *
   * @return array
   *   The admin form structure with current posts setting.
   */
  public function getFormAdmin(array $form, array &$form_state);

  /**
   * Implements validation from the Form API for the admin settings form.
   *
   * Adapted
   * from @link https://drupal.org/node/1111280 Validating the data @endlink.
   *
   * @param array $form
   *   A structured array containing the elements and properties of the form.
   * @param array $form_state
   *   An array that stores information about the form's current state
   *   during processing.
   */
  public function validateAdmin(array $form, array &$form_state);

}
