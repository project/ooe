<?php

/**
 * @file
 * CurrentPostsBlockView.
 */

namespace Drupal\ooe\Demo\Adapted;

use Drupal\ooe\Block\AbstractBlockView;
use Drupal\ooe\Common;

/**
 * A block view adapted from the 'current_posts' demo.
 *
 * Visit also:
 * @link https://drupal.org/node/1104498 Generating block content @endlink.
 *
 * !!! PLEASE DO NOT CODE LIKE THIS WHEN WORKING WITH OOE,
 * IT IS PROVIDED AS A "COUNTER-EXAMPLE" ONLY !!!
 *
 * @author darrenkelly
 */
class CurrentPostsBlockView extends AbstractBlockView {

  /**
   * Adapted from the 'current posts' example.
   *
   * See @link https://drupal.org/node/1104498 Generating block content @endlink
   * and @link https://drupal.org/node/1128278 Adding a 'More' link @endlink.
   *
   * Prepares the contents of the 'Current posts' block.
   *
   * !!! PLEASE DO NOT CODE THIS WAY USING EXPLICIT DRUPAL ARRAY KEYS LIKE
   * THIS THIS WHEN WORKING WITH OOE, IT IS PROVIDED AS A "COUNTER-EXAMPLE" !!!
   *
   * For an example of the use of OOE render objects (implementations
   * of @end IRender @endlink) to handle the render arrays please visit
   * @link CurrentPostsHelper::getPage() @endlink.
   *
   * For an example of the use of (only) render arrays with explicit
   * Drupal structured render array keys please visit
   * @link CurrentPostsHelper::getPageNonOO() @endlink.
   *
   * @see theme_item_list
   *
   * @see theme_more_link
   *
   * @return array
   *   A Drupal render array.
   */
  protected function myContentRenderArray() {

    $content = NULL;
    if (user_access('access content')) {
      // Webel: NB: still WET: Drupal array key not encapsulated !
      // 'Use our custom function to retrieve data.'
      $result = CurrentPostsHelper::getContents(CurrentPostsHelper::DISPLAY_BLOCK);
      // 'Array to contain items for the block to render.'
      $items = array();
      // 'Iterate over the resultset and format as links.'
      foreach ($result as $node) {
        $items[] = array(
          'data' => l($node->title, 'node/' . $node->nid),
        );
      }

      // 'No content in the last week.'
      $key_posts = 'posts';
      // Webel: DRY !

      if (empty($items)) {
        $content[$key_posts] = array(
          '#markup' => t('No posts available.'),
        );
      }
      else {
        // 'Pass data through theme function.'

        /* Non Render Array version:
        $content[$key_posts] = theme('item_list', array(
        'items' => $items,
        'type' => 'ol'
        // Webel: demonstrate passing options
        // to default theme_item_list function
        )
        );
         */
        $content[$key_posts] = array(
          // '#theme' => 'item_list__current_posts__block', // ERROR __block ?
          '#theme' => 'item_list__' . Common::MODULE,
          '#items' => $items,
          '#type' => 'ol',
            // Webel: extra: demonstrate passing options
            // to default theme_item_list function.
        );

        $content['more'] = array(
          '#theme' => 'more_link__' . Common::MODULE,
          '#url' => Common::MODULE . '/current_posts',
          // @todo inject URL
          '#title' => t('See the full list of current posts.'),
        );
      }
    }
    return $content;
  }

}
