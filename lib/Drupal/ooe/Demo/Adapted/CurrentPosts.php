<?php

/**
 * @file
 * CurrentPosts.
 */

namespace Drupal\ooe\Demo\Adapted;

use Drupal\ooe\Project\AbstractProject;
use Drupal\ooe\Block\IBlock;
use Drupal\ooe\Demo\Adapted\CurrentPostsFactory;
use Drupal\ooe\Common;

/**
 * A current posts implementation of an @link IProject @endlink.
 *
 * Encapsulates the well-known
 * @link https://drupal.org/node/1074360 Creating modules - a tutorial: Drupal 7.x @endlink
 * example in a (more) object-oriented fashion.
 *
 * IMPORTANT: In order to afford comparison with other more completely
 * object-oriented examples here, some aspects of this implementation
 * are deliberately not fully OOE-style object-oriented.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/1567 CurrentPosts @endlink
 *
 * @author darrenkelly
 */
class CurrentPosts extends AbstractProject implements ICurrentPosts {

  const NAME = 'current_posts';
  // @todo promote to getName()

  const DISPLAY = 'Current posts';

  // @todo promote to getDisplayName()

  /**
   * A factory for all instances of this.
   *
   * @var \Drupal\ooe\Factory\IFactory
   */
  static private $FACTORY;

  /**
   * Lazily creates and configures a @link CurrentPostsFactory @endlink.
   *
   * @return \Drupal\ooe\Factory\IFactory
   *   A @link CurrentPostsFactory @endlink for all instances of this.
   */
  static protected function MYFACTORY() {
    if (empty(self::$FACTORY)) {
      self::$FACTORY = new CurrentPostsFactory(Common::MODULE);
    }
    return self::$FACTORY;
  }

  /**
   * Constructor.
   *
   * Configures this with a @link CurrentPostsFactory @endlink.
   *
   * @param string $module
   *   The machine name of the module.
   * @param array $accessArguments
   *   Optional Drupal access arguments array.
   * @param \Drupal\ooe\Layout\IRegion $blockRegion
   *   Optional theme layout region for the default block.
   */
  public function __construct(
  $module, array $accessArguments = NULL, IRegion $blockRegion = NULL
  ) {
    parent::__construct($module, self::MYFACTORY(), $accessArguments, $blockRegion);
  }

  /**
   * Creates and configures a new menu item.
   *
   * @return \Drupal\ooe\Menu\IMenuItem
   *   Creates and configures a new menu item.
   */
  protected function myMenuItem() {
    $title = self::DISPLAY;
    $path = $this->getModule() . '/' . self::NAME;
    $callbackSuffix = 'page_' . self::NAME;
    return $this->newMenuItem($title, $path, $callbackSuffix)->setWeight(-2);
    // @codingStandardsIgnoreStart
    //->setAccessArguments($this->accessArguments());
    // @codingStandardsIgnoreEnd
  }

  /**
   * A new configured current posts block.
   *
   * @return \Drupal\ooe\Block\IBlock
   *   A new configured current posts block.
   */
  protected function myBlock() {
    $deltaSuffix = 'block_' . self::NAME;

    // $info = t(self::DISPLAY);
    // No, DRY, but will not be caught by translator.
    $info = t('Current posts');
    // WET Drupal.

    return $this->newBlock($deltaSuffix, $info);
  }

  /**
   * Implemented to obtain a block view product from a factory.
   *
   * The factory should be this stage by a @link CurrentPostsFactory @endlink.
   *
   * (One could of course instead by-pass the factory and simply
   * choose a @link CurrentPostsBlockView @endlink.)
   *
   * @param \Drupal\ooe\Block\IBlock $block
   *   Usually the primary block for which this must create a new block view.
   *
   * @return \Drupal\ooe\Block\IBlockView
   *   A new configured current posts block view.
   */
  protected function myBlockView(IBlock $block) {
    $subject = t('!module-display-name: Current posts', array('!module-display-name' => $this->getModuleDisplayName()));
    return $this->factory()->newBlockView($block, $subject);
  }

  /**
   * For the custom Current Posts page callback function.
   *
   * As declared in @link OoeBridge::menu @endlink() via ooe_menu().
   *
   * Adapted from the Current Posts example:
   * @link https://drupal.org/node/1118218 Theming the page @endlink
   *
   * This OOE version encapsulates the database query and the
   * page construction in a @link CurrentPostsHelper @endlink.
   *
   * Visit also
   * @link https://drupal.org/node/930760 Render Arrays in Drupal 7 @endlink
   *
   * See it running live at
   * @link http://drupal7demo.webel.com.au/ooe/current_posts @endlink
   *
   * @return array
   *   A Drupal page render array for the Current Posts example.
   */
  public function getPage() {
    return CurrentPostsHelper::getPage(
            CurrentPostsHelper::getContents(CurrentPostsHelper::DISPLAY_PAGE)
    );
  }

  /**
   * Delegate for a page callback for the admin form settings.
   *
   * Adapted from the Current Posts example:
   * @link https://drupal.org/node/1111260 Creating the configuration form @endlink.
   *
   * IMPORTANT: Please DO NOT create forms this way when working with OOE !
   * This implementation is merely here to show you the "old" Drupal7
   * way of doing it using "by convention/documented"
   * Drupal form array keys in structured arrays.
   *
   * In OOE one should instead use form building classes.
   * Please compare with implementations of
   * @link IFormController::build @endlink($form, &$form_submit)
   * such as in @link DemoFormController @endlink.
   *
   * Notice however that this adaptation is still made more
   * @link http://drupal7demo.webel.com.au/node/10 DRY @endlink
   * and hence safer by encapsulating the settings variable name
   * as @link CurrentPostsHelper::SETTING_POSTS_MAX @endlink.
   * It is used in 2 places in this code, and in many other
   * places in other methods and classes so it is encapsulated !
   *
   * @param array $form
   *   A Drupal form array.
   * @param array $form_state
   *   A Drupal form state array.
   *
   * @return array
   *   The admin form structure (so far only with current posts settings).
   *
   * @see system_settings_form
   *
   * @see ooe_form_admin
   */
  public function getFormAdmin(array $form, array &$form_state) {

    $form[CurrentPostsHelper::SETTING_POSTS_MAX] = array(
      '#type' => 'textfield',
      '#title' => t('Maximum number of posts in current posts block'),
      '#default_value' =>
      variable_get(CurrentPostsHelper::SETTING_POSTS_MAX, 3),
      '#size' => 2,
      '#maxlength' => 2,
      '#description' =>
      t('The maximum number of links to display in the OOE current posts block.'),
      '#required' => TRUE,
    );

    return system_settings_form($form);
  }

  /**
   * Implements validation of the admin form.
   *
   * Adapted from
   * @link https://drupal.org/node/1111280 Validating the data @endlink:
   *
   * 'Notice the $form_state array variable,
   * an argument here and also for our form function.
   * $form_state is passed by reference along through
   * each stage of form processing to capture information
   * about the form's workflow and its current state.
   * Incoming $_POST data is first sanitized and checked
   * against the structure of the form before being handed
   * off to validate and submit handlers.
   * The array's values key is the default key for storing this data.'
   *
   * Visit drupal_build_form() for a list of $form_state keys
   * (and note that in the OOE form builder class approach
   * those keys are instead encapsulated in classes that
   * build valid Drupal form arrays for you.
   *
   * @param array $form
   *   A structured array containing the elements and properties of the form.
   * @param array $form_state
   *   An array that stores information about the
   *   form's current state during processing.
   */
  public function validateAdmin(array $form, array &$form_state) {
    $max_num = $form_state['values'][CurrentPostsHelper::SETTING_POSTS_MAX];
    // @todo encapsulate 'values' key.

    if (!is_numeric($max_num)) {
      form_set_error(CurrentPostsHelper::SETTING_POSTS_MAX, t('You must enter a number for the maximum number of posts to display.')
      );
    }
    elseif ($max_num <= 0) {
      form_set_error(CurrentPostsHelper::SETTING_POSTS_MAX, t('Maximum number of posts to display must be positive.')
      );
    }
  }

}
