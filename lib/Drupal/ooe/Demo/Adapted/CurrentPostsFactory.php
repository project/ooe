<?php

/**
 * @file
 * CurrentPostsFactory.
 */

namespace Drupal\ooe\Demo\Adapted;

use Drupal\ooe\Factory\AbstractFactory;

use Drupal\ooe\Block\IBlock;

/**
 * A factory for the @link CurrentPosts @endlink.
 *
 * The use of a dedicated @link IFactory @endlink
 * implementation for such a simple
 * project may seem like overkill, it is however
 * included for demonstration purposes. More complex
 * projects may have good reason to implement more
 * specific and complex factories.
 *
 * @author darrenkelly
 */
class CurrentPostsFactory extends AbstractFactory {

  /**
   * Implemented to use a @link CurrentPostsBlockView @endlink as product.
   *
   * @param IBlock $block
   *   The block for which this acts as a view.
   * @param string $subject
   *   The (translated) subject of the block.
   *
   * @return \Drupal\ooe\Block\IBlockView
   *   A new current posts block view.
   */
  public function newBlockView(IBlock $block, $subject) {
    return new CurrentPostsBlockView($block, $subject);
  }

}
