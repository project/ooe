<?php

/**
 * @file
 * Demo.
 */

namespace Drupal\ooe\Demo\Project;

use Drupal\ooe\Project\AbstractControlledProject;
use Drupal\ooe\Demo\OoePageController;
use Drupal\ooe\Demo\Block\DemoBlockView;
use Drupal\ooe\Block\IBlock;

/**
 * The primary controlled page demo project for the OOE tutorial.
 *
 * This primary controlled page serves as the welcome page
 * and base for all of the demos. To see it live visit
 * @link http://drupal7demo.webel.com.au/ooe @endlink.
 *
 * @author darrenkelly
 */
class Demo extends AbstractControlledProject {

  /**
   * Implemented to provide a minimal demo block.
   *
   * @return \Drupal\ooe\Block\IBlock
   *   A new configured demo block.
   */
  protected function myBlock() {
    $deltaSuffix = 'block_demo';
    $info = t('Demo block');
    return $this->newBlock($deltaSuffix, $info);
    // Will go via the factory.
  }

  /**
   * Implemented to provide a new @link DemoBlockView @endlink.
   *
   * @param \Drupal\ooe\Block\IBlock $block
   *   The primary block for which this must create a new block view.
   *
   * @return \Drupal\ooe\Block\IBlockView
   *   A new demo block view.
   */
  protected function myBlockView(IBlock $block) {
    $subject = t('!module-display-name: Demo block', array('!module-display-name' => $this->getModuleDisplayName()));
    return new DemoBlockView($block, $subject);
  }

  /**
   * Implemented to provide a new @link OoePageController @endlink.
   *
   * The controlled page acts as a welcome page for all of the tutorial demos.
   *
   * @return \Drupal\ooe\Page\IPageController
   *   A new OOE page controller for the primary page menu item.
   */
  protected function myPageController() {
    return new OoePageController();
  }

  /**
   * Implemented to provide a controlled page menu item configured for the demo.
   *
   * @return \Drupal\ooe\Page\IPageMenuItem
   *   Creates and configures a new demo welcome page menu item.
   */
  protected function myPageMenuItem() {
    $title = 'Demonstrations welcome page';
    $path = $this->getModule();
    return $this->newPageMenuItem(
                $title, $path)
            ->forceAccessCallbackTrue()
            ->setExpanded(TRUE);
  }

}
