<?php

/**
 * @file
 * DemoArgumentsPageController.
 */

namespace Drupal\ooe\Demo\Arguments;

use Drupal\ooe\Common;
use Drupal\ooe\Page\AbstractPageController;
use Drupal\ooe\Render\RenderSet;
use Drupal\ooe\Render\RenderMenuSubtree;
use Drupal\ooe\Demo\DemoCommon;
use Drupal\ooe\Menu\MenuCommon;

/**
 * A IPageController implementation for the top-level arguments demo page.
 *
 * @author darrenkelly
 */
class DemoArgumentsPageController extends AbstractPageController {

  /**
   * Constructor.
   */
  public function __construct() {
    parent::__construct(Common::MODULE);
    // @todo: how inject/propagate module name
  }

  /**
   * The callback method name.
   */
  const CALLBACK = 'page';

  /**
   * Overridden to return the eventual callback method.
   *
   * @return string
   *   The eventual callback method.
   */
  public function getCallbackName() {
    return self::CALLBACK;
  }

  /**
   * Builds a page render array for the set of page argument extraction demos.
   *
   * Uses a @link IRenderFactory @endlink.
   *
   * @return array
   *   A Drupal page render array.
   */
  public function page() {
    $page = $this->rf()->newSet();

    $page->addItem('info', DemoCommon::renderTipHoverLinks());

    $link_path = DemoOfPageArguments::stemDemoPageArgs();
    $page->addItem(
        'links', $this->rf()->newMenuSubtree($link_path, MenuCommon::MENU_NAVIGATION)
    );

    return $page->get();
  }

  /**
   * Builds a page render array for the set of page argument extraction demos.
   *
   * This version does not use a @link IRenderFactory @endlink.
   *
   * @return array
   *   A Drupal page render array.
   */
  public function pageOldNoFactory() {
    $page = new RenderSet();

    $page->addItem('info', DemoCommon::renderTipHoverLinks());

    $link_path = DemoOfPageArguments::stemDemoPageArgs();
    $page->addItem('links', new RenderMenuSubtree($link_path, MenuCommon::MENU_NAVIGATION));

    return $page->get();
  }

}
