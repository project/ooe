<?php

/**
 * @file
 * DemoArgumentsHelper.
 */

namespace Drupal\ooe\Demo\Arguments;

use Drupal\ooe\Utility\Utility;
use Drupal\ooe\Render\RenderHelper;
use Drupal\ooe\Demo\Theme\WebelD7bartikCSS;

/**
 * Encapsulates a demonstration page illustrating page callback arguments.
 *
 * All render arrays are managed by @link IRender @endlink implementations;
 * Not a single Drupal render array #key in sight !
 *
 * @todo Consider giving it a @link IRenderFactory @endlink.
 *
 * @author darrenkelly
 */
class DemoArgumentsHelper extends RenderHelper {

  /**
   * The path base stem for the arguments demo.
   *
   * @var string
   */
  private $stem;

  /**
   * Constructor.
   *
   * @param string $stem
   *   The path base stem for the arguments demo.
   */
  public function __construct($stem) {
    $this->stem = $stem;
  }

  /**
   * A list of slash-separated Drupal argument components for testing.
   *
   * @var array
   */
  private $testArgLinks;

  /**
   * Sets a list of slash-separated Drupal argument components for testing.
   *
   * If provided a list of links for testing
   * the page with different arguments will be output.
   *
   * DO NOT include the page path stem, just the additional argument parts.
   *
   * Example:
   * array( "1st", "1st/2nd", "1st/2nd/3rd");
   *
   * @param array $testArgLinks
   *   An array of path portions to append as test page arguments.
   */
  public function setTestArgLinks(array $testArgLinks) {
    $this->testArgLinks = $testArgLinks;
  }

  /**
   * A map of the named parameters of the original callback with values.
   *
   * @var array
   */
  private $mapArgs;

  /**
   * A map of the named parameters of the original callback with values.
   *
   * This is NOT necessarily the same length
   * as the number of args called on the callback !
   *
   * If this is set the named parameters with values will be output as a list.
   *
   * @param array $mapArgs
   *   The map of arguments to build a page for.
   */
  public function setMapArgs(array $mapArgs) {
    $this->mapArgs = $mapArgs;
  }

  /**
   * Builds a page for the previously set argument map.
   *
   * Pass all page arguments as received by the
   * original callback function to this method.
   *
   * It should be called on using func_get_args()
   * from the calling function/method.
   *
   * This implementation uses OOE render objects.
   *
   * @return array
   *   A Drupal page array.
   */
  public function getPage(array $args) {

    $page = self::rf()->newSet();

    $class = get_class($this);

    $page->addItem(
        'render-info', self::rf()->newSetP()->addItem(
            'iClass', self::rf()->newI("This page is built using OOE render objects by class: $class")
        )
    );

    // Automated argument extraction.
    $demo_css_class = 'li-demo';
    $numargs = count($args);
    $var_items = self::rf()->newListItems();
    for ($i = 0; $i < $numargs; $i++) {
      $li = self::rf()->newListItem($args[$i]);
      $li->addCssClass($demo_css_class);
      $var_items->addItem($li);
    }

    $key_args = 'page_args';
    if (!$var_items->hasItems()) {
      $noargs = self::rf()->newP('No args available.');
      $page->addItem($key_args, $noargs);
    }
    else {
      $list = self::rf()->newList($var_items, 'List of all extracted arguments (as passed to the encapsulated page callback)'
      );
      $list->setTypeOrdered();
      $page->addItem($key_args, $list);
    }

    // List of named parameters.
    if (!empty($this->mapArgs)) {

      $var_items = self::rf()->newListItems();
      foreach ($this->mapArgs as $arg => $value) {
        $li = self::rf()->newListItem("<b>$arg:</b> $value");
        // @todo <b>markup ? Let $data be deep.

        $li->addCssClass($demo_css_class);
        $var_items->addItem($li);
      }
      $var_list = self::rf()->newList(
          $var_items, 'Values of named parameter arguments of the encapsulated page callback method'
      );
      $var_list->setTypeOrdered();
      $page->addItem('args_content', $var_list);
    }
    else {
      $tagDivError = self::rf()->newTagDiv()
          ->addCssClass(WebelD7bartikCSS::ERROR);
      $page->addItem(
          'error', self::newR(
              t('DEBUG: ERROR: Please provide a map of named parameter arguments before calling the page builder !'), $tagDivError)
      );
    }

    // List of test links with different arguments.
    if (!empty($this->testArgLinks)) {

      $page->addItem('hr', self::rf()->newHr());
      $page->addItem(
          'tryLinksHeader', self::rf()->newH('Try these test links', 3)
      );

      $links = self::rf()->newListItems();
      foreach ($this->testArgLinks as $link) {
        $links->addItem(self::rf()->newListItem(Utility::link("$this->stem/$link")));
      }
      $link_list = self::rf()->newList($links);
      $link_list->setTypeOrdered();
      $page->addItem('test_links', $link_list);
    }
    return $page->get();
  }

}

// @codingStandardsIgnoreStart
// Consider providing a helper for automated provision of named arg map
// http://www.php.net/manual/en/reflectionfunctionabstract.getparameters.php
//
//static function findMapArgs($funcName) {
//    $f = new ReflectionFunction($funcName);
//    $result = array();
//    foreach ($f->getParameters() as $param) {
//    $result[] = $param->name;
//    }
//    return $result;
//}
// @codingStandardsIgnoreEnd
