<?php

/**
 * @file
 * DemoOfPageArguments.
 */

namespace Drupal\ooe\Demo\Arguments;

use Drupal\ooe\Project\AbstractControlledProject;
use Drupal\ooe\Block\BlockVisibilityKind;
use Drupal\ooe\Demo\Block\DemoBlockView;
use Drupal\ooe\Block\IBlock;
use Drupal\ooe\Common;
use Drupal\ooe\Demo\Page\DemoPageController;
use Drupal\ooe\Page\IPageController;

/**
 * A @link IProject project @endlink for demonstration
 * of Drupal page argument extraction for various argument cases.
 *
 * @author darrenkelly
 */
class DemoOfPageArguments extends AbstractControlledProject implements IDemoOfPageArguments {

  /**
   * Creates a path stem shared by all page argument demos.
   *
   * Using this ensures that all page argument demos
   * are correctly grouped under one menu item.
   *
   * @todo PROBLEM: the DemoPageController can't take a
   * $module in constructor (must be noargs)
   * so have to use shared static.
   *
   * @return string
   *   The path stem shared by all page argument demos.
   */
  static public function stemDemoPageArgs() {
    return Common::MODULE . '/demo_page_arguments';
  }

  /**
   * Implemented to create and configure an unlisted dummy block.
   *
   * [It is by default disabled, and is not yet intended for use as
   * it will not yet be populated with anything relevant to this demo.]
   *
   * @return \Drupal\ooe\Block\IBlock
   *   A new configured demo block [as a dummy block].
   */
  protected function myBlock() {
    $deltaSuffix = 'block_demo_page_arguments';
    $info = t('Page arguments demo block');
    return $this->newBlock($deltaSuffix, $info)
            ->setVisibility(BlockVisibilityKind::NOTLISTED);
  }

  /**
   * Implemented to create a new @link DemoBlockView @endlink.
   *
   * [It is by default disabled, and is not yet intended for use
   * as it is not yet populated with anything relevant to this demo.]
   *
   * @param \Drupal\ooe\Block\IBlock $block
   *   Usually the primary block for which this must create a new block view.
   *
   * @return \Drupal\ooe\Block\IBlockView
   *   A new configured demo block view.
   */
  protected function myBlockView(IBlock $block) {
    $subject = t('!module-display-name: page arguments demo block',
        array('!module-display-name' => $this->getModuleDisplayName())
        );
    return new DemoBlockView($block, $subject);
  }

  /**
   * Implemented to create a new @link DemoArgumentsPageController @endlink.
   *
   * @return \Drupal\ooe\Page\IPageController
   *   A new page arguments demo primary menu page controller.
   */
  protected function myPageController() {
    return new DemoArgumentsPageController();
  }

  /**
   * Implemented to create a new configured page arguments demo page menu item.
   *
   * @return \Drupal\ooe\Page\IPageMenuItem
   *   A new configured page arguments demo page menu item.
   */
  protected function myPageMenuItem() {

    $path = self::stemDemoPageArgs();

    $title = $this->titleStem() . 's';
    // Plural.

    $this->setDoPrefixMenuTitles(FALSE);
    // Since titleStem() already has module name prefix.

    return $this->newPageMenuItem($title, $path)
            ->setAccessArguments($this->accessArguments());
  }

  /**
   * Implemented to fill the menu set.
   *
   * Fills it with the
   * various page argument extraction demos.
   */
  protected function myFillMenuItemSet() {

    // Demos: page arguments.

    $this->addMenuItem($this->menuItemAuto());

    $this->addMenuItem($this->menuItemForced());

    $this->addMenuItem($this->menuItemControlled());

    $this->addMenuItem($this->menuItemPageControllerSkene());
  }

  /**
   * Creates a new plain page callback sub menu item (no controller).
   *
   * The machine demo name will be added to the @link titleStem @endlink()
   * after a ': ' and space.
   *
   * @param string $demo_name
   *   The machine name path segment of the demo
   *   that will be appended to the root menu path.
   * @param string $description
   *   The description of the menu item.
   *
   * @return \Drupal\ooe\Menu\IMenuItem
   *   A new sub menu item.
   */
  private function newSubMenuItem($demo_name, $description) {
    $this->setDoPrefixMenuTitles(FALSE);
    // Since titleStem() already has module name prefix.

    return $this->factory()->newMenuItem(
            $this->titleStem() . ": $demo_name", self::stemDemoPageArgs() . "/$demo_name"
        )->setDescription($description);
  }

  /**
   * Creates a new controlled page sub menu item.
   *
   * The machine demo name will be added to the @link titleStem @endlink()
   * after a ': ' colon and space.
   *
   * @param \Drupal\ooe\Page\IPageController $controller
   *   The page controller for the controlled menu item.
   * @param string $demo_name
   *   The machine name path segment of the demo,
   *   will be appended to the root menu path.
   * @param string $description
   *   The description of the menu item.
   *
   * @return \Drupal\ooe\Page\IPageMenuItem
   *   A new controlled page sub menu item.
   */
  private function newSubPageMenuItem(IPageController $controller, $demo_name, $description) {
    $this->setDoPrefixMenuTitles(FALSE);
    // Since titleStem() already has module name prefix.

    return $this->factory()->newPageMenuItem(
            $controller, $this->titleStem() . ": $demo_name", self::stemDemoPageArgs() . "/$demo_name"
        )->setDescription($description);
  }

  /**
   * A counter for the weight of page argument demo menu items.
   *
   * @var int
   */
  private $menuWeightCounter = 0;

  /**
   * A counter for the weight of page argument demo menu items.
   *
   * Incremented each access.
   *
   * @return int
   *   A counter for the weight of page argument demo menu items.
   */
  protected function incMenuWeightCounter() {
    return $this->menuWeightCounter++;
  }

  /**
   * Stem of the .module file callback function name.
   *
   * Shared by all non controlled page argument demos.
   *
   * @return string
   *   Stem of the .module file callback function name
   *   shared by all non controlled page argument demos.
   */
  protected function funcStem() {
    return '_' . $this->getModule() . '_demo_page_arguments';
    // @todo Lazily?
  }

  /**
   * Stem of the page title shared by all page argument demos.
   *
   * @return string
   *   Stem of the page title shared by all page argument demos.
   */
  protected function titleStem() {
    return $this->getModuleDisplayName() . ": page path argument extraction demo";
    // @todo Lazily?
  }

  /**
   * Menu item for demo of automatic page argument extraction.
   *
   * @var \Drupal\ooe\Menu\IMenuItem
   *   Menu item for demo of automatic page argument extraction.
   */
  private $menuItemAuto;

  /**
   * Lazily creates and configures a plain menu item (no page controller).
   *
   * The menu item will have a .module file callback function
   * for experimenting with automatic page argument extraction.
   *
   * @return \Drupal\ooe\Menu\IMenuItem
   *   Lazily created and configured menu item
   *   for demo of automatic page argument extraction.
   */
  protected function menuItemAuto() {
    if (empty($this->menuItemAuto)) {
      $demo_name = "auto";
      $description = "Demonstrates extraction of automatic page callback path arguments for a menu item with no page controller";
      $this->menuItemAuto = $this->newSubMenuItem($demo_name, $description);
      $this->menuItemAuto->setPageCallback($this->funcStem() . "_$demo_name");
      $this->menuItemAuto->setAccessArguments($this->accessArguments());
      // @codingStandardsIgnoreStart
      //$this->menuItemAuto->setTypeNormal(TRUE);//Redundant default.
      // @codingStandardsIgnoreEnd
      $this->menuItemAuto->setWeight($this->incMenuWeightCounter());
    }
    return $this->menuItemAuto;
  }

  /**
   * Delegate for page callback to demo of auto argument extraction.
   *
   * It leverages @link DemoArguments @endlinks to format
   * a standard arguments extraction test page.
   *
   * From hook_menu():
   * 'Note that this automatic passing of optional path arguments
   * applies only to page and theme callback functions.'
   *
   * Visit also: @link http://en.wikipedia.org/wiki/Foobar Foobar @endlink.
   *
   * @param string $foo
   *   The 1st argument.
   * @param string $bar
   *   The 2nd argument.
   *
   * @return array
   *   A Drupal page render array with diagnostics about extracted arguments.
   *
   * @see func_get_args()
   */
  public function getPageArgumentsAuto($foo = "[foo]", $bar = "[bar]") {

    $demo = new DemoArgumentsHelper(self::stemDemoPageArgs() . '/auto');

    $demo->setMapArgs(
        array(
          "foo" => $foo,
          "bar" => $bar,
        )
    );
    // Consider also:
    // http://stackoverflow.com/questions/2692481/getting-functions-argument-names

    $arg = '';
    $demo->setTestArgLinks(
        array(
          $arg,
          $arg .= '1st',
          $arg .= '/2nd',
          $arg .= '/3rd',
          $arg .= '/4th',
        )
    );

    return $demo->getPage(func_get_args());
  }

  /**
   * Menu item for demo of page arguments with some forced arguments.
   *
   * @var \Drupal\ooe\Menu\IMenuItem
   *   Menu item for demo of page arguments with some forced arguments.
   */
  private $menuItemForced;

  /**
   * Lazily creates and configures a plain menu item (no page controller).
   *
   * Configures the menu item with some forced arguments
   * and a .module file callback function.
   *
   * The page arguments set to array(1, 'force2', 3, 'force4') to match
   * the $fi and $fum of in the eventual page callback
   * delegate @link getPageArgumentsForced @endlink($fe, $fi, $fo, $fum).
   *
   * @return \Drupal\ooe\Menu\IMenuItem
   *   Lazily created and configured menu item for
   *   demo of page arguments with some forced arguments.
   */
  protected function menuItemForced() {
    if (empty($this->menuItemForced)) {
      $demo_name = "forced";
      $description = "Demonstrates extraction of partially forced page callback path arguments for a menu item with no page controller";
      $this->menuItemForced = $this->newSubMenuItem($demo_name, $description);
      $this->menuItemForced->setPageCallback($this->funcStem() . "_$demo_name");
      $this->menuItemForced->setAccessArguments($this->accessArguments());
      $this->menuItemForced->setPageArguments(array(1, 'force2', 3, 'force4'));
      // @codingStandardsIgnoreStart
      //$this->menuItemForced->setTypeNormal(true);// Redundant, default.
      // @codingStandardsIgnoreEnd
      $this->menuItemForced->setWeight($this->incMenuWeightCounter());
    }
    return $this->menuItemForced;
  }

  /**
   * Delegate for page callback to demo of forced arguments.
   *
   * The forced arguments pattern was defined in an IMenuItem
   * in @link getMenuItemForced @endlink().
   *
   * It leverages @link DemoArguments @endlink to format
   * a standard arguments extraction test page.
   *
   * Forced arguments are explained at hook_menu().
   *
   * Visit also:
   * @link http://en.wikipedia.org/wiki/Fee-fi-fo-fum Fee-fi-fo-fum @endlink.
   *
   * @param string $fe
   *   The 1st argument.
   * @param string $fi
   *   The 2nd argument.
   * @param string $fo
   *   The 3rd argument.
   * @param string $fum
   *   The 4th argument.
   *
   * @return array
   *   A Drupal page render array with diagnostics about extracted arguments.
   *
   * @see func_get_args()
   */
  public function getPageArgumentsForced($fe = '[fe]', $fi = '[fi]', $fo = '[fo]', $fum = '[fum]') {

    $demo = new DemoArgumentsHelper(self::stemDemoPageArgs() . '/forced');

    $demo->setMapArgs(
        array(
          "fe" => $fe,
          "fi" => $fi,
          "fo" => $fo,
          "fum" => $fum,
        )
    );

    $arg = '';
    $demo->setTestArgLinks(
        array(
          $arg,
          $arg .= '1st',
          $arg .= '/2nd',
          $arg .= '/3rd',
          $arg .= '/4th',
          $arg .= '/5th',
        )
    );

    return $demo->getPage(func_get_args());
  }

  /**
   * Page controller for OOE controlled page demo.
   *
   * @var \Drupal\ooe\Page\IPageController
   */
  private $demoPageController;

  /**
   * A lazily created and configured page controller.
   *
   * For the OOE controlled page demo.
   *
   * @return \Drupal\ooe\Page\IPageController
   *   Lazily created and configured page controller
   *    for OOE controlled page demo.
   */
  protected function demoPageController() {
    if (empty($this->demoPageController)) {
      $this->demoPageController = new DemoPageController();
      // @todo How inject MODULE when PageController
      // creates with noargs constructor ?
    }
    return $this->demoPageController;
  }

  /**
   * Menu item for OOE controlled page demo.
   *
   * @var \Drupal\ooe\Page\IPageMenuItem
   */
  private $menuItemControlled;

  /**
   * Lazily created page menu item for OOE controlled page demo.
   *
   * @return \Drupal\ooe\Page\IPageMenuItem
   *   Lazily created page menu item for OOE controlled page demo.
   */
  protected function menuItemControlled() {
    if (empty($this->menuItemControlled)) {
      $description = "Demonstrates extraction of page callback path arguments for a menu item with an OOE IPageController";
      $this->menuItemControlled = $this->newSubPageMenuItem($this->demoPageController(), DemoPageController::DEMO_NAME, $description);
      $this->menuItemControlled->setAccessArguments($this->accessArguments());
      $this->menuItemControlled->setWeight($this->incMenuWeightCounter());
    }
    return $this->menuItemControlled;
  }

  /**
   * Menu item for adapted the Skene page_controller demo.
   *
   * @var \Drupal\ooe\Menu\IMenuItem
   */
  private $menuItemPageControllerSkene;

  /**
   * Example using page_controller project (adapted from C. Skene).
   *
   * Project page: @link http://drupal.org/project/Page_controller @endlink.
   *
   * It is harder to code than using a controlled
   * OOE @link IPageMenuItem @endlink, because
   * the page arguments convention for the controller is not encapsulated.
   *
   * IMPORTANT: the page path has 2 string args added to the registered path
   * otherwise get a warning from the original Skene ExampleController,
   * noting the callback defined there is
   * ExampleController::myPageControllerViewCallback($arg1, $arg2)
   *
   * @return \Drupal\ooe\Menu\IMenuItem
   *   Lazily created menu item for the adapted Skene page_controller demo.
   */
  protected function menuItemPageControllerSkene() {
    if (empty($this->menuItemPageControllerSkene)) {
      $demo_name = "skene";
      $description = "Demonstrates extraction of automatic page callback path arguments for a menu item with a page_controller project PageController (adapted from Chris Skene)";
      $this->menuItemPageControllerSkene = $this->newSubMenuItem($demo_name, $description);
      $path = $this->menuItemPageControllerSkene()->getPath();

      $this->menuItemPageControllerSkene->setPath($path);
      // !

      $this->menuItemPageControllerSkene->setPageCallback('\Drupal\\page_controller\\Controller\\PageController::createPage');
      $this->menuItemPageControllerSkene->setAccessArguments($this->accessArguments());
      $this->menuItemPageControllerSkene->setPageArguments(
          // Will be popped using array_shift.
          array(
            '\Drupal\\page_controller\\Example\\ExampleController',
            'myPageControllerViewCallback',
            '1st',
            '2nd',
          )
      );
      $this->menuItemPageControllerSkene->setWeight($this->incMenuWeightCounter());
    }
    return $this->menuItemPageControllerSkene;
  }

}
