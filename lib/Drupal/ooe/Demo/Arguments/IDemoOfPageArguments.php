<?php

/**
 * @file
 * IDemoOfPageArguments.
 */

namespace Drupal\ooe\Demo\Arguments;

use Drupal\ooe\Project\IControlledProject;

/**
 * A controlled project for demos of Drupal page argument extraction.
 *
 * @author darrenkelly
 */
interface IDemoOfPageArguments extends IControlledProject {

  /**
   * Encapsulates the name of a delegate page callback.
   *
   * The reason this is included in here (rather than say directly
   * as a string in a client of this) is that if the
   * delegate callback method name changes here
   * then clients will automatically
   * catch the change (after
   * the @link DRY http://drupal7demo.webel.com.au/node/10 @endlink.
   * coding principle).
   */
  const FUNC_PAGE_ARGUMENTS_AUTO = 'getPageArgumentsAuto';

  /**
   * Delegate for page callback to demo of auto argument extraction.
   *
   * From hook_menu():
   * 'Note that this automatic passing of optional path arguments
   * applies only to page and theme callback functions.'
   *
   * Visit
   * also: @link http://en.wikipedia.org/wiki/Foobar Foobar @endlink
   *
   * @param string $foo
   *   The 1st parameter.
   * @param string $bar
   *   The 2nd parameter.
   *
   * @return array
   *   A Drupal page render array
   */
  public function getPageArgumentsAuto($foo, $bar);

  /**
   * Encapsulates the name of a delegate page callback.
   *
   * The reason this is included here (rather than say directly
   * as a string in a client of this) is that if the
   * delegate callback method name changes here
   * then clients will automatically
   * catch the change (after
   * the @link DRY http://drupal7demo.webel.com.au/node/10 @endlink
   * coding principle).
   */
  const FUNC_PAGE_ARGUMENTS_FORCED = 'getPageArgumentsForced';

  /**
   * Delegate for page callback to demo of forced arguments.
   *
   * Forced arguments are explained at hook_menu().
   *
   * Visit also:
   * @link http://en.wikipedia.org/wiki/Fee-fi-fo-fum Fee-fi-fo-fum @endlink.
   *
   * @param string $fe
   *   The 1st parameter.
   * @param string $fi
   *   The 2nd parameter.
   * @param string $fo
   *   The 3rd parameter.
   * @param string $fum
   *   The 4th parameter.
   *
   * @return array
   *   A Drupal page render array
   */
  public function getPageArgumentsForced($fe, $fi, $fo, $fum);

}
