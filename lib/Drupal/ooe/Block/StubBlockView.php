<?php

/**
 * @file
 * StubBlockView.
 */

namespace Drupal\ooe\Block;

use Drupal\ooe\Demo\Theme\WebelD7bartikCSS;
// @codingStandardsIgnoreStart
// Issue: coupling, import from Demo.
// @codingStandardsIgnoreEnd

/**
 * A placeholder @link IBlockView @endlink implementation.
 *
 * It is provided for those @link IProject @endlink
 * implementations that don't require a visible block.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2018 StubBlockView @endlink
 *
 * @author darrenkelly
 */
class StubBlockView extends AbstractBlockView {

  /**
   * Implemented to return a placeholder block view stating this is a stub.
   *
   * @return array
   *   A Drupal render array with stub content.
   */
  protected function myContentRenderArray() {
    $tagDiv = self::rf()->newTagDiv();
    $tagDiv->addCssClass(WebelD7bartikCSS::STUB);
    return self::rf()->newR(t('[STUB BLOCK VIEW]'), $tagDiv)->get();
  }

}
