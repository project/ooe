<?php

/**
 * @file
 * BlockCacheKind.
 */

namespace Drupal\ooe\Block;

/**
 * Encapsulates some block caching bitmask constants
 * referenced in hook_block_info().
 *
 * 'cache: (optional) A bitmask describing what kind
 * of caching is appropriate for the block. Drupal provides
 * the following bitmask constants for defining cache granularity:
 *
 *  DRUPAL_CACHE_PER_ROLE (default): The block can change depending
 *  on the roles the user viewing the page belongs to.
 *
 *  DRUPAL_CACHE_PER_USER: The block can change depending on the
 *  user viewing the page. This setting can be resource-consuming
 *  for sites with large number of users, and should only be
 *  used when DRUPAL_CACHE_PER_ROLE is not sufficient.
 *
 *  DRUPAL_CACHE_PER_PAGE: The block can change
 *  depending on the page being viewed.
 *
 *  DRUPAL_CACHE_GLOBAL: The block is the same for every user
 *  on every page where it is visible.
 *
 *  DRUPAL_CACHE_CUSTOM: The module implements
 *  its own caching system.
 *
 *  DRUPAL_NO_CACHE: The block should not get cached.'
 *
 * UML: @link http://drupal7demo.webel.com.au/node/1148 BlockCacheKind @endlink
 *
 * @author darrenkelly
 */
class BlockCacheKind {

  /**
  * '(default): The block can change depending on
  * the roles the user viewing the page belongs to'.
  */
  const CACHE_PER_ROLE = DRUPAL_CACHE_PER_ROLE;

  /**
   * 'The block can change depending on the user viewing the page.
   * This setting can be resource-consuming
   * for sites with large number of users,
   * and should only be used when DRUPAL_CACHE_PER_ROLE is not sufficient'.
   */
  const CACHE_PER_USER = DRUPAL_CACHE_PER_USER;

  /**
   * 'The block can change depending on the page being viewed'.
   */
  const CACHE_PER_PAGE = DRUPAL_CACHE_PER_PAGE;

  /**
   * 'The block is the same for every user
   *  on every page where it is visible'.
   */
  const CACHE_GLOBAL = DRUPAL_CACHE_GLOBAL;

  /**
   * 'The module implements its own caching system'.
   */
  const CACHE_CUSTOM = DRUPAL_CACHE_CUSTOM;

  /**
   * 'The block should not get cached'.
   */
  const NO_CACHE = DRUPAL_NO_CACHE;

}
