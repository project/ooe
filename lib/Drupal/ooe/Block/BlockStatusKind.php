<?php

/**
 * @file
 * BlockStatusKind.
 */

namespace Drupal\ooe\Block;

/**
 * Encapsulates the possible enabled/disabled status of blocks
 * referenced in hook_block_info().
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/1150 BlockStatusKind @endlink.
 *
 * @author darrenkelly
 */
class BlockStatusKind {

  /**
   * State: block is enabled.
   *
   * @var bool
   */
  const ENABLED = 1;

  /**
   * State: block is disabled.
   *
   * @var bool
   */
  const DISABLED = 0;

}
