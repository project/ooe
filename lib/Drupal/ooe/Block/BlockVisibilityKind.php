<?php

/**
 * @file
 * BlockVisibilityKind.
 */

namespace Drupal\ooe\Block;

/**
 * Encapsulates the possible visibility status options
 * of blocks as referenced in hook_block_info().
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/1151 BlockVisibilityKind @endlink.
 *
 * @author darrenkelly
 */
class BlockVisibilityKind {

  /**
   * 'Show on all pages except listed pages.
   * "pages" lists the paths where the block should not be shown'.
   */
  const NOTLISTED = BLOCK_VISIBILITY_NOTLISTED;

  /**
   * 'Show only on listed pages.
   * "pages" lists the paths where the block should be shown'.
   */
  const LISTED = BLOCK_VISIBILITY_LISTED;

  /**
   * 'Use custom PHP code to determine visibility.
   * "pages" gives the PHP code to use'.
   */
  const PHP = BLOCK_VISIBILITY_PHP;

}
