<?php

/**
 * @file
 * IBlock.
 */

namespace Drupal\ooe\Block;

use Drupal\ooe\Layout\IRegion;

/**
 * Block encapsulation.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/1063 IBlock @endlink.
 *
 * @see hook_block_info()
 *
 * @author darrenkelly
 */
interface IBlock {

  /**
   * Array key.
   *
   * @var
   */
  const CONTENT = 'content';

  /**
   * Array key.
   *
   * @var
   */
  const SUBJECT = 'subject';

  /**
   * Array key.
   *
   * @var
   */
  const INFO = 'info';

  /**
   * Array key.
   *
   * @var
   */
  const CACHE = 'cache';

  /**
   * Unique delta identifier for the block.
   *
   * Only string identifiers are supported here.
   *
   * @return string
   *   The unique delta identifier for the block.
   */
  public function getDelta();

  /**
   * Sets the cache bitmask.
   *
   * From hook_block_info():
   *
   * 'cache: (optional) A bitmask describing what kind of caching is
   *  appropriate for the block. Drupal provides the following bitmask
   *  constants for defining cache granularity:
   *
   *  DRUPAL_CACHE_PER_ROLE (default): The block can change depending
   *  on the roles the user viewing the page belongs to.
   *
   *  DRUPAL_CACHE_PER_USER: The block can change depending
   *  on the user viewing the page. This setting can be resource-consuming
   *  for sites with large number of users, and should only be used when
   *  DRUPAL_CACHE_PER_ROLE is not sufficient.
   *
   *  DRUPAL_CACHE_PER_PAGE: The block can change
   *  depending on the page being viewed.
   *
   *  DRUPAL_CACHE_GLOBAL: The block is the same for
   *  every user on every page where it is visible.
   *
   *  DRUPAL_CACHE_CUSTOM: The module implements its own caching system.
   *
   *  DRUPAL_NO_CACHE: The block should not get cached.'
   *
   * @param int $cache
   *   A bitmask integer: for valid options see @link BlockCacheKind @endlink.
   *
   * @return IBlock
   *   This.
   */
  public function setCache($cache);

  /**
   * Sets an optional array of additional metadata to add to the block.
   *
   * From hook_block_info():
   * 'properties: (optional) Array of additional metadata to add to the block.'
   *
   * 'Common properties include:
   *
   *   administrative: Boolean that categorizes this block
   *   as usable in an administrative context.
   *   This might include blocks that help an administrator
   *   approve/deny comments, or view recently created
   *   user accounts.'
   *
   * @param array $properties
   *   An array of properties for the block.
   *
   * @return IBlock
   *   This.
   *
   * @see IBlock::setPropertyAdministrative(bool)
   */
  public function setProperties(array $properties);
  // @todo: migrate to use IProperty.

  /**
   * Sets the administrative property.
   *
   * From hook_block_info():
   * 'Boolean that categorizes this block as
   * usable in an administrative context.
   * This might include blocks that help an
   * administrator approve/deny comments,
   * or view recently created user accounts'.
   *
   * @param bool $administrative
   *   'Boolean that categorizes this block as
   *   usable in an administrative context'.
   *
   * @return IBlock
   *   This.
   *
   * @see IBlock::setProperties(array)
   */
  public function setPropertyAdministrative($administrative);

  /**
   * Sets the block weight.
   *
   *   From hook_block_info():
   *   '(optional) Initial value for the ordering weight of this block.
   *   Most modules do not provide an initial value, and any value provided
   *   can be modified by a user on the block configuration screen.'
   *
   * @param int $weight
   *   '(optional) Initial value for the ordering weight of this block'.
   *
   * @return IBlock
   *   This.
   */
  public function setWeight($weight);

  /**
   * Sets the initial value for the block enabled status.
   *
   *  From hook_block_info(): 'status: (optional) Initial value for
   *  block enabled status. (1 = enabled, 0 = disabled). Most modules
   *  do not provide an initial value, and any value provided can be modified
   *  by a user on the block configuration screen.'
   *
   * @param int $status
   *   The initial value for block enabled status;
   *   valid values are provided by @link BlockStatusKind @endlink.
   *
   * @return IBlock
   *   This.
   */
  public function setStatus($status);

  /**
   * Sets the initial block reqion.
   *
   * From hook_block_info():
   * 'region: (optional) Initial value for theme region
   *  within which this block is set. Most modules
   *  do not provide an initial value,
   *  and any value provided can be modified by a user
   *  on the block configuration screen.
   *   Note: If you set a region that isn't available in the
   *   currently enabled theme, the block will be disabled.'
   *
   * @param \Drupal\ooe\Layout\IRegion $region
   *   The initial value for theme region within which this block is set.
   *
   * @return IBlock
   *   This.
   */
  public function setRegion(IRegion $region);

  /**
   * Sets the initial block visibility status.
   *
   * From hook_block_info():
   *
   * 'visibility: (optional) Initial value for the visibility flag,
   * which tells how to interpret the 'pages' value. Possible values are:
   *
   *    BLOCK_VISIBILITY_NOTLISTED: Show on all pages except listed pages.
   *    'pages' lists the paths where the block should not be shown.
   *
   *    BLOCK_VISIBILITY_LISTED: Show only on listed pages. 'pages' lists
   *    the paths where the block should be shown.
   *
   *    BLOCK_VISIBILITY_PHP: Use custom PHP code to determine visibility.
   *    'pages' gives the PHP code to use.
   *
   * Most modules do not provide an initial value for 'visibility' or 'pages',
   * and any value provided can be modified by a user
   * on the block configuration screen.'
   *
   * @param int $visibility
   *   The initial block visibility kind; Valid values are given
   *   by @link BlockVisibilityKind @endlink.
   *
   * @return IBlock
   *   This.
   */
  public function setVisibility($visibility);

  /**
   * Adds a page path.
   *
   * @param string $pagePath
   *   A page path.
   *
   * @return IBlock
   *   This.
   *
   * @see IBlock::getPages()
   */
  public function addPagePath($pagePath);

  /**
   * Adds a '<front>' page path.
   *
   * @see IBlock::addPagePath(string)
   * @see IBlock::getPages()
   *
   * @return IBlock
   *   This.
   */
  public function addPagePathFront();

  /**
   * A string that contains one or more page paths separated by '\n'.
   *
   * From hook_menu_info():
   *   'pages: (optional) See 'visibility' above. A string that contains
   *    one or more page paths separated by '\n', '\r', or '\r\n' when
   *    'visibility' is set to BLOCK_VISIBILITY_NOTLISTED
   *   or BLOCK_VISIBILITY_LISTED,
   *    or custom PHP code when 'visibility' is set to BLOCK_VISIBILITY_PHP.
   *   Paths may use '*' as a wildcard (matching any number of characters);
   *   '<front>' designates the site's front page. For BLOCK_VISIBILITY_PHP,
   *   the PHP code's return value should be TRUE if the block is to be made
   *   visible or FALSE if the block should not be visible.'
   *
   * @see IBlock::addPagePath(string)
   * @see IBlock::addPagePathFront()
   *
   * @return string
   *   A string that contains one or more page paths separated by '\n'.
   */
  public function getPages();

  /**
   * A Drupal array compatible with hook_block_info().
   *
   * @return array
   *   A Drupal array compatible with hook_block_info().
   */
  public function get();

}
