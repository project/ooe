<?php

/**
 * @file
 * DefaultBlock.
 */

namespace Drupal\ooe\Block;

use Drupal\ooe\Layout\IRegion;

/**
 * A default block implementation.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/1064 DefaultBlock @endlink.
 *
 * @author darrenkelly
 */
class DefaultBlock implements IBlock {

  /**
   * The unique delta identifier for the block.
   *
   * Only strings are supported here;
   * Numbers can always simply be integrated into strings anyway.
   *
   * From hook_block_info():
   * 'The values of delta can be strings or numbers,
   * but ... it is preferable to use descriptive strings whenever possible,
   * and only use a numeric identifier if you have to
   * (for instance if your module allows users to create several
   * similar blocks that you identify within your
   * module code with numeric IDs).
   * The maximum length for delta values is 32 bytes.'
   *
   * @var string
   */
  private $delta;

  /**
   * The unique delta identifier for the block.
   *
   * @return string
   *   Unique delta identifier for the block.
   */
  public function getDelta() {
    return $this->delta;
  }

  /**
   * The human-readable administrative name of the block.
   *
   * From hook_block_info():
   * 'The human-readable administrative name of the block.'
   *
   * 'This is used to identify the block on administration screens,
   * and is not displayed to non-administrative users.'
   *
   * @var string
   */
  private $info;

  // @codingStandardsIgnoreStart
  // public function getInfo() { return $this->info;}
  // @codingStandardsIgnoreEnd

  /**
   * From hook_block_info(): 'The maximum length for delta values is 32 bytes'.
   */
  const DELTA_MAX_BYTELENGTH = 32;

  /**
   * Constructor.
   *
   * Please note that for delta block identifiers
   * only strings are supported here;
   * Numbers can always simply be integrated into strings anyway.
   *
   * From hook_block_info():
   * 'The values of delta can be strings or numbers,
   * but ... it is preferable to use descriptive strings whenever possible,
   * and only use a numeric identifier if you have to
   * (for instance if your module allows users to create several
   * similar blocks that you identify within your module code with numeric IDs).
   * The maximum length for delta values is 32 bytes.'
   *
   * @param string $delta
   *   Unique delta identifier for the block (required).
   * @param string $info
   *   '(required) The human-readable administrative name of the block.
   *   This is used to identify the block on administration screens,
   *   and is not displayed to non-administrative users.'. Translated.
   *
   * @throws \Exception
   *   If either $delta or $info are empty or not strings,
   *   or if the byte length of $delta is greater than 32.
   *
   * @see hook_block_info()
   */
  public function __construct(
  $delta, $info) {
    if (empty($delta) || !is_string($delta)) {
      throw new \Exception('$delta must be a non empty string !');
    }
    $mb = mb_strlen($delta);
    if ($mb > self::DELTA_MAX_BYTELENGTH) {
      throw new \Exception("The byte length ($mb) of delta($delta) is greater than " . self::DELTA_MAX_BYTELENGTH . ' !');
    }
    if (empty($info) || !is_string($info)) {
      throw new \Exception('$info must be a non empty string !');
    }
    $this->delta = $delta;
    $this->info = $info;
  }

  /**
   * The cache policy bitmask integer.
   *
   * @var int
   */
  private $cache;

  // @codingStandardsIgnoreStart
  // public function getCache() { return $this->cache;}
  // @codingStandardsIgnoreEnd

  /**
   * Set the cache bitmask.
   *
   * @param int $cache
   *   A bitmask integer: for valid options see @link BlockCacheKind @endlink.
   *
   * @return IBlock
   *   This.
   */
  public function setCache($cache) {
    if (empty($cache) || !is_int($cache)) {
      throw new \Exception('$cache must be a non empty integer !');
    }
    if (!($cache == BlockCacheKind::CACHE_CUSTOM ||
        $cache == BlockCacheKind::CACHE_GLOBAL ||
        $cache == BlockCacheKind::CACHE_PER_PAGE ||
        $cache == BlockCacheKind::CACHE_PER_ROLE ||
        $cache == BlockCacheKind::CACHE_PER_USER ||
        $cache == BlockCacheKind::NO_CACHE)) {
      throw new \Exception('$cache(' . $cache . ') is not an allowed cache bitmask value !');
    }
    $this->cache = $cache;
    return $this;
  }

  /**
   * Optional array of additional metadata to add to the block.
   *
   * From hook_block_info():
   * 'properties: (optional) Array of additional metadata to add to the block'.
   *
   * 'Common properties include:
   *
   *   administrative: Boolean that categorizes this block
   *   as usable in an administrative context.
   *   This might include blocks that help an administrator
   *   approve/deny comments, or view recently created
   *   user accounts.'
   *
   * @var array
   */
  private $properties;

  /**
   * Sets optional array of additional metadata to add to the block.
   *
   * From hook_block_info():
   * 'properties: (optional) Array of additional metadata to add to the block.'
   *
   * 'Common properties include:
   *
   *   administrative: Boolean that categorizes this block
   *   as usable in an administrative context.
   *   This might include blocks that help an administrator
   *   approve/deny comments, or view recently created
   *   user accounts.'
   *
   * @param array $properties
   *   An array of properties for the block.
   *
   * @return IBlock
   *   This.
   *
   * @see IBlock::setPropertyAdministrative(bool)
   */
  public function setProperties(array $properties) {
    $this->properties = $properties;
    return $this;
  }

  /**
   * Sets the administrative property of the block.
   *
   * @param bool $administrative
   *   From hook_block_info(): 'Boolean that categorizes this block as
   *   usable in an administrative context. This might include blocks
   *   that help an administrator approve/deny comments,
   *   or view recently created user accounts'.
   *
   * @return IBlock
   *   This.
   *
   * @see IBlock::setProperties(array)
   */
  public function setPropertyAdministrative($administrative = TRUE) {

    if (!is_bool($administrative)) {
      throw new \Exception('$administrative must be boolean !');
    }

    if (empty($this->properties) || !isset($this->properties)) {
      $this->properties = array();
    }

    $this->properties['administrative'] = $administrative;
    return $this;
  }

  /**
   * The optional initial value for the ordering weight of this block.
   *
   * From hook_block_info():
   * '(optional) Initial value for the ordering weight of this block'.
   *
   * 'Most modules do not provide an initial value, and any value provided
   * can be modified by a user on the block configuration screen'.
   *
   * @var int
   */
  private $weight;

  /**
   * Sets the block weight.
   *
   *   From hook_block_info():
   *   '(optional) Initial value for the ordering weight of this block.
   *   Most modules do not provide an initial value, and any value provided
   *   can be modified by a user on the block configuration screen.'
   *
   * @param int $weight
   *   '(optional) Initial value for the ordering weight of this block'.
   *
   * @throws Exception
   *   If the $weight is not an integer.
   *
   * @return IBlock
   *   This.
   */
  final public function setWeight($weight) {
    if (!empty($weight) && !is_int($weight)) {
      throw new \Exception('$weight must be an integer !');
    }
    $this->weight = $weight;
    return $this;
  }

  /**
   * Block status from From hook_block_info().
   *
   * From hook_block_info():
   * 'status: (optional) Initial value for block enabled status.
   * (1 = enabled, 0 = disabled)'.
   *
   * 'Most modules do not provide an initial value,
   * and any value provided can be modified by a user on
   * the block configuration screen.'
   *
   * @var int
   */
  private $status;

  /**
   * Sets the initial value for the block enabled status.
   *
   *  From hook_block_info(): 'status: (optional) Initial value for
   *  block enabled status. (1 = enabled, 0 = disabled). Most modules
   *  do not provide an initial value, and any value provided can be modified
   *  by a user on the block configuration screen.'
   *
   * @param int $status
   *   The initial value for block enabled status;
   *   valid values are provided by @link BlockStatusKind @endlink.
   *
   * @return IBlock
   *   This.
   */
  final public function setStatus($status) {
    if (is_null($status)) {
      throw new \Exception('$status must not be null !');
    }
    if (!is_int($status)) {
      throw new \Exception('$status must be an integer !');
    }
    if (!($status == 0 || $status == 1)) {
      throw new \Exception('$status must be 0 or 1, got (' . $status . ' !');
    }
    $this->status = $status;
    return $this;
  }

  /**
   * Initial value for theme region within which this block is set.
   *
   * From hook_block_info():
   * 'region: (optional) Initial value for theme region
   *  within which this block is set.'
   *
   * 'Most modules do not provide an initial value,
   *  and any value provided can be modified by a user
   *  on the block configuration screen.
   *   Note: If you set a region that isn't available in the
   *   currently enabled theme, the block will be disabled.'
   *
   * @var \Drupal\ooe\Layout\IRegion
   */
  private $region;

  /**
   * Sets the initial block reqion.
   *
   * From hook_block_info():
   * 'region: (optional) Initial value for theme region
   *  within which this block is set.
   *  Most modules do not provide an initial value,
   *  and any value provided can be modified by a user
   *  on the block configuration screen.
   *  Note: If you set a region that isn't available in the
   *  currently enabled theme, the block will be disabled.'
   *
   * @param \Drupal\ooe\Layout\IRegion $region
   *   The initial value for theme region within which this block is set.
   *
   * @return IBlock
   *   This.
   */
  final public function setRegion(IRegion $region) {
    if (empty($region)) {
      throw new \Exception('IRegion $region must be not empty !');
    }
    $this->region = $region;
    return $this;
  }

  /**
   * The initial block visibility status.
   *
   * @var int
   */
  private $visibility;

  /**
   * Sets the initial block visibility status.
   *
   * From hook_block_info():
   *
   * 'visibility: (optional) Initial value for the visibility flag,
   * which tells how to interpret the 'pages' value. Possible values are:
   *
   *    BLOCK_VISIBILITY_NOTLISTED: Show on all pages except listed pages.
   *    'pages' lists the paths where the block should not be shown.
   *
   *    BLOCK_VISIBILITY_LISTED: Show only on listed pages. 'pages' lists
   *    the paths where the block should be shown.
   *
   *    BLOCK_VISIBILITY_PHP: Use custom PHP code to determine visibility.
   *    'pages' gives the PHP code to use.
   *
   * Most modules do not provide an initial value for 'visibility' or 'pages',
   * and any value provided can be modified by a user
   * on the block configuration screen.'
   *
   * @param int $visibility
   *   The initial block visibility kind. Valid values are given
   *   by @link BlockVisibilityKind @endlink.
   *
   * @return IBlock
   *   This.
   */
  final public function setVisibility($visibility) {
    if (is_null($visibility)) {
      throw new \Exception('$visibility must not be null !');
    }

    if (!is_int($visibility)) {
      throw new \Exception('$visibility must be an integer !');
    }
    if (!(
        $visibility == BLOCK_VISIBILITY_NOTLISTED ||
        $visibility == BLOCK_VISIBILITY_LISTED ||
        $visibility == BLOCK_VISIBILITY_PHP
        )) {
      throw new \Exception('$visibility must be one of (' .
      BLOCK_VISIBILITY_NOTLISTED . ', ' .
      BLOCK_VISIBILITY_LISTED . ', ' .
      BLOCK_VISIBILITY_PHP . '), got(' . $visibility . ')'
      );
      // @codingStandardsIgnoreStart
      // @codingStandardsIgnoreEnd
    }
    $this->visibility = $visibility;
    return $this;
  }

  /**
   * An array for holding page paths.
   *
   * From hook_menu_info():
   * 'pages: (optional) See 'visibility' above. A string that contains
   *  one or more page paths separated by '\n', '\r', or '\r\n' when
   *  'visibility' is set to BLOCK_VISIBILITY_NOTLISTED
   *  or BLOCK_VISIBILITY_LISTED,
   *  or custom PHP code when 'visibility' is set to BLOCK_VISIBILITY_PHP.
   *  Paths may use '*' as a wildcard (matching any number of characters);
   *  '<front>' designates the site's front page. For BLOCK_VISIBILITY_PHP,
   *  the PHP code's return value should be TRUE if the block is to be made
   *  visible or FALSE if the block should not be visible.'
   *
   * @var array
   */
  private $pagePaths = array();

  /**
   * Adds a page path.
   *
   * @param string $pagePath
   *   A page path.
   *
   * @throws Exception
   *   If the $pagePath is not a string.
   *
   * @return IBlock
   *   This.
   *
   * @see IBlock::getPages()
   */
  public function addPagePath($pagePath) {
    if (!is_string($pagePath)) {
      throw new \Exception('$pagePath must be a string !');
    }
    $this->pagePaths[] = $pagePath;
    return $this;
  }

  /**
   * Adds a '<front>' page path.
   *
   * @see IBlock::addPagePath(string)
   * @see IBlock::getPages()
   *
   * @return IBlock
   *   This.
   */
  public function addPagePathFront() {
    $this->addPagePath('<front>');
    // @todo Check not already present ?

    return $this;
  }

  /**
   * A string that contains one or more page paths separated by '\n'.
   *
   * From hook_menu_info():
   *   'pages: (optional) See 'visibility' above. A string that contains
   *    one or more page paths separated by '\n', '\r', or '\r\n' when
   *    'visibility' is set to BLOCK_VISIBILITY_NOTLISTED
   *    or BLOCK_VISIBILITY_LISTED,
   *    or custom PHP code when 'visibility' is set to BLOCK_VISIBILITY_PHP.
   *   Paths may use '*' as a wildcard (matching any number of characters);
   *   '<front>' designates the site's front page. For BLOCK_VISIBILITY_PHP,
   *   the PHP code's return value should be TRUE if the block is to be made
   *   visible or FALSE if the block should not be visible.'
   *
   * @see IBlock::addPagePath(string)
   * @see IBlock::addPagePathFront()
   *
   * @return string
   *   A string that contains one or more page paths separated by '\n'.
   */
  public function getPages() {
    $pages = NULL;
    foreach ($this->pagePaths as $p) {
      if (!is_null($pages)) {
        $pages = "$pages\n$p";
      }
      else {
        $pages = $p;
      }
    }
    return $pages;
  }

  /**
   * Builds and returns a Drupal array compatible with hook_block_info().
   *
   * @return array
   *   A Drupal array compatible with hook_block_info().
   */
  public function get() {
    $out = array();

    $out['delta'] = $this->delta;

    $out['info'] = $this->info;
    // Must already be translated !

    if (!empty($this->cache)) {
      $out['cache'] = $this->cache;
    }
    if (!empty($this->properties)) {
      $out['properties'] = $this->properties;
    }
    if (!empty($this->weight)) {
      $out['weight'] = $this->weight;
    }
    if (!empty($this->status)) {
      $out['status'] = $this->status;
    }
    if (!empty($this->region)) {
      $out['region'] = $this->region->getName();
    }
    if (!empty($this->visibility)) {
      $out['visibility'] = $this->visibility;
    }

    $pages = $this->getPages();

    if (!empty($pages)) {
      $out['pages'] = $pages;
    }
    return $out;
  }

}
