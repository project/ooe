<?php

/**
 * @file
 * IBlockView.
 */

namespace Drupal\ooe\Block;

/**
 * Block view encapsulation.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/1127 IBlockView @endlink.
 *
 * @see hook_block_view($delta)
 *
 * @author darrenkelly
 */
interface IBlockView {

  /**
   * The unique block identifier (of the block of this).
   *
   * @return string
   *   The unique block identifier (of the block of this).
   */
  public function getDelta();

  // @codingStandardsIgnoreStart
  // public function getSubject();
  // @codingStandardsIgnoreEnd

  /**
   * The block of this.
   *
   * @return IBlock
   *   The block of this.
   */
  public function getBlock();

  /**
   * A block view array compatible with hook_block_view($delta).
   *
   * @return array
   *   A block view array with optional subject and content
   *   compatible with hook_block_view($delta):
   */
  public function get();

}
