<?php

/**
 * @file
 * IBlockSet.
 */

namespace Drupal\ooe\Block;

/**
 * Manages a set/group of blocks.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/1067 IBlockSet @endlink.
 *
 * @todo Ordering and uniqueness policy.
 *
 * @author darrenkelly
 */
interface IBlockSet {

  /**
   * Add a block.
   *
   * @param IBlock $block
   *   The block to add.
   *
   * @return IBlockSet
   *   This.
   */
  public function addBlock(IBlock $block);

  /**
   * Add each block in an array of blocks.
   *
   * @param IBlock[] $blocks
   *   The array of blocks to add.
   *
   * @return IBlockSet
   *   This.
   */
  public function addBlocks(array $blocks);

  /**
   * The array of blocks.
   *
   * @return IBlock[]
   *   The array of blocks.
   */
  public function getBlocks();

  /**
   * A Drupal array compatible with hook_block_info().
   *
   * Uses the 'delta' of the blocks as keys.
   *
   * @return array
   *   A Drupal array compatible with hook_block_info(),
   *   with the 'delta' of the blocks as keys.
   */
  public function get();

}
