<?php

/**
 * @file
 * AbstractBlockView.
 */

namespace Drupal\ooe\Block;

use Drupal\ooe\Render\RenderHelper;

/**
 * Abstract implementation of an encapsulated block view.
 *
 * Collects those aspects typically common to all implementations.
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/1128 AbstractBlockView @endlink.
 *
 * @todo Consider forcing use of @link IRender @endlink objects
 * instead of permitting any render array return values.
 *
 * @author darrenkelly
 */
abstract class AbstractBlockView extends RenderHelper implements IBlockView {

  /**
   * The block identifier of the block of this.
   *
   * @return string
   *   The block identifier of the block of this.
   */
  public function getDelta() {
    return $this->block->getDelta();
  }

  /**
   * The block of this (set by construction).
   *
   * @var IBlock
   */
  private $block;

  /**
   * The block of this.
   *
   * Please note that no setter is provided,
   * it can only be injected via the constructor !
   *
   * @return IBlock
   *   The block of this.
   */
  public function getBlock() {
    return $this->block;
  }

  /**
   * Constructor. It will bind this to the provided @link IBlock @endlink.
   *
   * @param IBlock $block
   *   Required. The block of this.
   * @param string $subject
   *   The optional (translated) subject.
   *
   * @throws \Exception
   *   If $block is empty.
   *   If $subject is not empty but not a string.
   */
  public function __construct(IBlock $block, $subject = '<NONE>') {
    if (empty($block)) {
      throw new \Exception('IBlock $block must not be empty !');
    }
    $this->block = $block;
    if (!empty($subject) && !is_string($subject)) {
      throw new \Exception('$subject must null or a string !');
    }
    $this->subject = $subject;
  }

  /**
   * A block view array with optional subject and content.
   *
   * Compatible with hook_block_view($delta).
   *
   * @return array
   *   A block view array with optional subject and content
   *   compatible with hook_block_view($delta).
   */
  final public function get() {
    $out = array();
    if (!empty($this->subject)) {
      $out['subject'] = $this->subject;
    }
    $out['content'] = $this->getContent();
    return $out;
  }

  /**
   * The content of the block to render.
   *
   * Compatible with the
   * block view array for hook_block_view($delta).
   *
   * If there is any "freestyle" HTML content defined in reimplementations of
   * @link AbstractBlockView::myContentHTML @endlink()
   * it will be taken first, otherwise the render array (if any)
   * from @link AbstractBlockView::myContentRenderArray @endlink()
   * is taken.
   *
   * @todo Consider simply merging them both into a single
   * render array: need policy for where the HTML would go
   * (perhaps as a markup sub render array).
   *
   * @return string|array
   *   An HTML markup string or a Drupal render array.
   */
  final protected function getContent() {
    $html = $this->myContentHtml();
    if (!empty($html)) {
      return $html;
    }
    return $this->myContentRenderArray();
  }

  /**
   * Concrete implementations should implement this.
   *
   * Implement this to return a Drupal render array,
   * or null (in which case the HTML content of implementations
   * of @link AbstractBlockView::myContentHTML @endlink()
   * if any, will be taken as the block content).
   *
   * It is highly recommended that implementations use objects
   * within the OOE family of @link IRender @endlink
   * classes to generate render array content !
   *
   * @return array
   *   The Drupal render array encapsulated by this (if any).
   */
  abstract protected function myContentRenderArray();

  /**
   * This is implemented by default to return null.
   *
   * In the case this returns null
   * the result of implementations of
   * @link AbstractBlockView::myContentRenderArray @endlink()
   * will be used in @link AbstractBlockView::getContent @endlink().
   * This is to encourage use of render arrays.
   *
   * Concrete implementations may override this to return
   * any freestyle HTML content string (in which case this will be
   * taken as the block view content). This is however not encouraged.
   *
   * @return string
   *   NULL ! Subclass reimplementations may provide freestyle HTML markup.
   */
  protected function myContentHtml() {
    return NULL;
  }

  // @codingStandardsIgnoreStart
  /*
   * NO SETTER ! Deliberate, force encapsulate render content
   * in subclass implementation.
   * @param string $content HTML content
   */
  // public function setContentHTML($content);

  /*
   * NO SETTER ! Deliberate, force encapsulate
   * render content in subclass implementation.
   * @param array $content Render array
   */
  // public function setContentRenderArray(array $content);
  // @codingStandardsIgnoreEnd

}
