<?php

/**
 * @file
 * BlockSet.
 */

namespace Drupal\ooe\Block;

/**
 * A set/group of blocks.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/1068 BlockSet @endlink.
 *
 * @todo Enforce set not list (uniqueness).
 *
 * @todo Ordering policy.
 *
 * @author darrenkelly
 */
class BlockSet implements IBlockSet {

  /**
   * The blocks.
   *
   * @var IBlock[]
   */
  private $blocks = array();

  /**
   * Add a block.
   *
   * @param IBlock $block
   *   The block to add.
   *
   * @return IBlockSet
   *   This.
   */
  public function addBlock(IBlock $block) {
    $this->blocks[] = $block;
    // @codingStandardsIgnoreStart
    // return (IBlockSet) $this; //NO http://drupal7demo.webel.com.au/node/1066
    // http://stackoverflow.com/questions/2226103/how-to-cast-objects-in-php
    // http://stackoverflow.com/questions/1147109/type-casting-for-user-defined-objects
    // @codingStandardsIgnoreEnd

    return $this;
  }

  /**
   * Add each block in an array of blocks.
   *
   * @param IBlock[] $blocks
   *   The array of blocks to add.
   *
   * @return IBlockSet
   *   This.
   */
  public function addBlocks(array $blocks) {
    foreach ($blocks as $mi) {
      if ($mi instanceof IBlock) {
        $this->addBlock($mi);
      }
      // @codingStandardsIgnoreStart
      // else // @todo warn.
      // @codingStandardsIgnoreEnd
    }
    return $this;
  }

  /**
   * Builds a Drupal array compatible with hook_block_info().
   *
   * Uses the 'delta' of the blocks as keys.
   *
   * @return array
   *   A Drupal array compatible with hook_block_info(),
   *   with the 'delta' of the blocks as keys.
   */
  public function get() {
    $items = array();
    foreach ($this->blocks as $b) {
      $items[$b->getDelta()] = $b->get();
    }
    return $items;
  }

  /**
   * The blocks as an array.
   *
   * @return IBlock[]
   *   The blocks as an array.
   */
  public function getBlocks() {
    return $this->blocks;
  }

}
