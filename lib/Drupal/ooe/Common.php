<?php

/**
 * @file
 * Common.
 */

namespace Drupal\ooe;

/**
 * Constants common to entire module and all demos.
 *
 * Promotes DRY (not WET) coding.
 *
 * Makes it easier to adapt the entire module Class code to other modules.
 *
 * In OOE, no Class should ever (!) explicitly refer to the module by string;
 * it should instead leverage this common declaration of the module name.
 *
 * @author darrenkelly
 */
class Common {

  /**
   * The module machine name.
   * @var string
   */
  const MODULE = 'ooe';

  /**
   * The module display name.
   * @var string
   */
  const MODULE_NAME = 'OOE';

  // @codingStandardsIgnoreStart
  // FAILS: const MODULE_NAME = strtoupper(MODULE);
  // http://stackoverflow.com/questions/5526365/php-assign-value-of-function-to-class-constant
  // @codingStandardsIgnoreEnd

  /**
   * The URL of the D7 demonstration site for this educational tutorial module.
   *
   * @var string
   */
  const URL_DEMO_SITE = 'http://drupal7demo.webel.com.au';

  /**
   * The forms demos path stem.
   * @var string
   */
  const STEM_DEMO_FORMS = 'ooe/demo_forms';
  // @todo move to demo class.

  /**
   * Whether to translate by default in rendering.
   *
   * Since the Drupal localization registry does not catch
   * strings passed to t() via class variables this should
   * usually be FALSE, it is only kept as a legacy switch.
   *
   * Where possible, strings should be translated using
   * t() before passing to class constructors, setters,
   * or other methods (possibly also using placeholders).
   *
   * @var bool
   */
  const TRANSLATE_DEFAULT = FALSE;

  // @codingStandardsIgnoreStart
  // http://stackoverflow.com/questions/3823018/default-method-argument-with-class-property
  // http://de.php.net/manual/en/functions.arguments.php
  // 'The default value must be a constant expression, not
  // (for example) a variable, a class member or a function call.'
  //
  //  /**
  //  * Whether markup strings passed to render objects
  //  * should be translated by default.
  //  *
  //  * @var bool
  //  */
  //  static public $translateDefault = TRANSLATE_DEFAULT;
  //
  //  /**
  //  * Whether markup strings passed to render objects
  //  * should be translated by default.
  //  *
  //  * @return bool
  //  *   Whether markup strings passed to render
  //  *   objects should be translated by default.
  //  */
  //  static public function getTranslateDefault() {
  //  return self::$translateDefault;
  //  }
  //
  //  /**
  //  * Sets whether markup strings passed to render objects
  //  * should be translated by default.
  //  *
  //  * @param bool $translateDefault
  //  *   Whether markup strings passed to render objects
  //  *   should be translated by default.
  //  */
  //  static public function setTranslateDefault($translateDefault) {
  //  self::$translateDefault = $translateDefault;
  //  }
  // @codingStandardsIgnoreEnd

}
