<?php

/**
 * @file
 * TagManager.
 */

namespace Drupal\ooe\Xhtml;

/**
 * Implements management of XHTML tag attributes.
 *
 * This is a support class for other classes
 * that are either explicitly or implicitly
 * associated with an XHTML tag.
 *
 * @todo Consider using XHTML and CSS validation libs
 * instead of Drupal-style attributes for input.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2051 TagManager @endlink
 *
 * @author darrenkelly
 */
class TagManager implements ITagManager {

  /**
   * An array populated with XHTML tag attributes.
   *
   * Not usually used alone, usually for merging with other arrays.
   *
   * @return array
   *   An array populated with HTML tag attributes.
   */
  public function getAttributes() {
    return $this->attributes;
  }

  /**
   * Sets the entire tag attributes array (overwriting any current attributes).
   *
   * @param array $attributes
   *   A Drupal-style XHTML attributes array.
   *
   * @return ITagManager
   *   This.
   */
  public function setAttributes(array $attributes) {
    $this->attributes = $attributes;
    return $this;
  }

  /**
   * XHTML tag attributes.
   *
   * @var array
   */
  private $attributes = array();

  /**
   * Set the CSS classes for this.
   *
   * @param string[] $classes
   *   An array of CSS classes for this.
   *
   * @return ITagManager
   *   This.
   */
  public function setCssClasses(array $classes) {

    if (empty($classes) || !is_array($classes)) {
      throw new \Exception('$class must be a non-empty array of strings !');
    }

    // Check array only contains strings.
    foreach ($classes as $c) {
      if (empty($c)) {
        throw new \Exception('array $classes contains an empty element !');
      }
      if (!is_string($c)) {
        throw new \Exception('array $classes contains a non string element(' . $c . ')');
      }
    }
    // @codingStandardsIgnoreStart
    // See also how one can use filters:
    // http://stackoverflow.com/questions/3559542/more-concise-way-to-check-to-see-if-an-array-contains-only-numbers-integers
    //$only_strings === array_filter($only_strings,'is_string');
    // @codingStandardsIgnoreEnd

    $this->attributes['class'] = $classes;
    return $this;
  }

  /**
   * Add a CSS class to this.
   *
   * @param string $class
   *   A CSS class for this.
   *
   * @return ITagManager
   *   This.
   */
  public function addCssClass($class) {
    if (empty($class) || !is_string($class)) {
      throw new \Exception('$class must be a non-empty string !');
    }
    $this->attributes['class'][] = $class;
    // IMPORTANT: https://drupal.org/node/1084308
    // In Drupal7 class attribute must in array format.
    return $this;
  }

  /**
   * Sets the CSS style of this.
   *
   * @todo check whether valid style, no help at
   * http://au2.php.net/manual/en/filter.filters.validate.php
   *
   * @todo Perhaps CSS regex checker: CSS2 here:
   * @link http://stackoverflow.com/questions/21223418/check-string-for-valid-css-using-regex @endlink
   *
   * @param string $style
   *   The CSS style of this.
   *
   * @return ITagManager
   *   This.
   */
  public function setCssStyle($style) {
    if (empty($style) || !is_string($style)) {
      throw new \Exception('$class must be a non-empty string !');
    }
    $this->attributes['style'] = $style;
    return $this;
  }

  /**
   * Sets an HTML attribute of this with the given name and value.
   *
   * @todo Check valid attribute names and values (challenging).
   *
   * @param string $name
   *   A non empty name of an HTML attribute.
   * @param string $value
   *   A value of an HTML attribute (may be null).
   *
   * @return ITagManager
   *   This.
   */
  public function setAttribute($name, $value) {
    if (empty($name) || !is_string($name)) {
      throw new \Exception('$name must be a non empty string !');
    }
    if (!is_string($value)) {
      throw new \Exception('$value must be a string !');
    }
    $this->attributes[$name] = $value;
    return $this;
  }

}
