<?php

/**
 * @file
 * ITag.
 */

namespace Drupal\ooe\Xhtml;

/**
 * Interface for an XHTML/HTML5 tag.
 *
 * This is only intended so far for simplest use,
 * it still requires some "manual" knowledge of valid XHTML and CSS.
 * It has just enough info on tags so far for simple demos.
 *
 * @todo Integrate with existing XHTML/DOM libs.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2052 ITag @endlink.
 *
 * @author darrenkelly
 */
interface ITag extends ITagManager {

  /**
   * The name of the tag.
   *
   * @return string
   *   The name of the tag.
   */
  public function getName();

  /**
   * Gets the tag prefix portion formatted as HTML including any attributes.
   *
   * Example:
   * <a href="http://www.test.com" class="my-link" style="font-weight: bold;">
   *
   * If the tag can't contain any content returns a closed prefix.
   *
   * Example: <hr/>, <br/>, <img src="http://www.test.com/logo.png"/>
   *
   * @return string
   *   A tag prefix portion.
   */
  public function getAsPrefix();

  /**
   * Gets the tag suffix portion.
   *
   * Example: </a>
   *
   * Not relevant for elements like HR and BR that don't have content,
   * so they should return null.
   *
   * @return string
   *   A tag suffix portion, or NULL for a self-closing tag.
   */
  public function getAsSuffix();

  /**
   * Whether this is self-closing (can't have children).
   *
   * @return bool
   *   Whether this is self-closed (can't have children).
   */
  public function isNoChildren();

}
