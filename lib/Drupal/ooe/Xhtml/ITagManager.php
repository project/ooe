<?php

/**
 * @file
 * ITagManager.
 */

namespace Drupal\ooe\Xhtml;

/**
 * Manages attributes of an XHTML tag.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2032 ITagManager @endlink.
 *
 * @author darrenkelly
 */
interface ITagManager {

  /**
   * An array populated with XHTML tag attributes.
   *
   * Not usually used alone, usually for merging with other arrays.
   *
   * @return array
   *   An array populated with XHTML tag attributes.
   */
  public function getAttributes();

  /**
   * Sets the entire tag attributes array (overwriting any current attributes).
   *
   * @param array $attributes
   *   A Drupal-style attributes array.
   *
   * @return ITagManager
   *   This.
   */
  public function setAttributes(array $attributes);

  /**
   * Sets an XHTML attribute of this with the given name and value.
   *
   * @param string $name
   *   A non empty name of an HTML attribute.
   * @param string $value
   *   A value of an XHTML attribute (may be null).
   *
   * @return ITagManager
   *   This.
   */
  public function setAttribute($name, $value);

  /**
   * Set the CSS classes of this.
   *
   * @param string[] $classes
   *   An array of CSS classes for this.
   *
   * @return ITagManager
   *   This.
   */
  public function setCssClasses(array $classes);

  /**
   * Add a CSS class to this.
   *
   * @param string $class
   *   A CSS class for this.
   *
   * @return ITagManager
   *   This.
   */
  public function addCssClass($class);

  /**
   * Set the CSS style of this.
   *
   * @param string $style
   *   The CSS style of this.
   *
   * @return ITagManager
   *   This.
   */
  public function setCssStyle($style);

}
