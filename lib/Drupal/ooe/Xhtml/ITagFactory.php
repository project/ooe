<?php

/**
 * @file
 * ITagFactory.
 */

namespace Drupal\ooe\Xhtml;

/**
 * A factory for @link ITag @endlink implementations.
 *
 * @todo Cover more convenience cases.
 *
 * @author darrenkelly
 */
interface ITagFactory {

  /**
   * A new XHTML/HTML5 tag with the given name.
   *
   * You can currently choose a valid XHTML tag name
   * with the help of @link XHTML @endlink, or consider
   * using one of the convenient tag creators such
   * as @link IRenderFactory::newTagP @endlink(),
   * @link IRenderFactory::newTagDiv @endlink(), etc.
   *
   * @todo Consider passing instead an OOE IAttributes.
   *
   * @param string $name
   *   An XHTML/HTML5 tag name.
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return ITag
   *   This.
   */
  public function newTag($name, array $attributes);

  /**
   * A new XHTML/HTML5 P tag.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return ITag
   *   This.
   */
  public function newP(array $attributes);

  /**
   * A new XHTML/HTML5 DIV tag.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return ITag
   *   This.
   */
  public function newDiv(array $attributes);

  /**
   * A new XHTML/HTML5 SPAN tag.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return ITag
   *   This.
   */
  public function newSpan(array $attributes);

  /**
   * A new XHTML/HTML5 B tag.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return ITag
   *   This.
   */
  public function newB(array $attributes);

  /**
   * A new XHTML/HTML5 I tag.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return ITag
   *   This.
   */
  public function newI(array $attributes);

  /**
   * A new XHTML/HTML5 EM tag.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return ITag
   *   This.
   */
  public function newEm(array $attributes);

  /**
   * A new XHTML/HTML5 STRONG tag.
   *
   * @param array $attributes
   *   A Drupal-style attributes array.
   *
   * @return ITag
   *   This.
   */
  public function newStrong(array $attributes);

  /**
   * A new XHTML/HTML5 BLOCKQUOTE tag.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return ITag
   *   This.
   */
  public function newBlockquote(array $attributes);

  /**
   * A new XHTML/HTML5 TABLE tag.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return ITag
   *   This.
   */
  public function newTable(array $attributes);

  /**
   * A new XHTML/HTML5 TR tag.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return ITag
   *   This.
   */
  public function newTr(array $attributes);

  /**
   * A new XHTML/HTML5 TH tag.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return ITag
   *   This.
   */
  public function newTh(array $attributes);

  /**
   * A new XHTML/HTML5 TD tag.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return ITag
   *   This.
   */
  public function newTd(array $attributes);

}

// @codingStandardsIgnoreStart
/**
   * A new themed list renderer.
   *
   * @param array $items From theme_item_list:
   * 'An array of items to be displayed in the list.
   * If an item is a string, then it is used as is. If an item is an array,
   * then the "data" element of the array is used
   * as the contents of the list item.
   * If an item is an array with a "children" element,
   * those children are displayed in a nested list.
   * All other elements are treated as attributes of the list item element.'
   *
   * @param string $title
   *   (Optional.) 'The title of the list.' Defaults to null.
   * @return \Drupal\ooe\Render\IRenderList
   */
  //public function newList(array $items, $title);
// @codingStandardsIgnoreEnd