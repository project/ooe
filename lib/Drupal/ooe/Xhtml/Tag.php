<?php

/**
 * @file
 * Tag.
 */

namespace Drupal\ooe\Xhtml;

/**
 * Implements an XHTML/HTML5 tag holder.
 *
 * This is only intended so far for simplest use,
 * it still requires some "manual" knowledge of valid XHTML and CSS.
 * It has just enough info on tags so far for simple demos.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2061 Tag @endlink.
 *
 * @todo Integrate with existing XHTML/DOM libs.
 *
 * @see http://www.w3schools.com/tags/default.asp
 * @see http://www.w3schools.com/html/html_xhtml.asp
 *
 * @author darrenkelly
 */
class Tag extends TagManager implements ITag {

  /**
   * Constructor.
   *
   * @todo XHTML versioning and valid tag names.
   *
   * @todo Consider using XHTML and CSS validation libs
   * instead of Drupal-style attributes for input.
   *
   * @param string $name
   *   The non-empty XHTML tag name; will be converted to lower case.
   * @param array $attributes
   *   A Drupal-style attributes array.
   */
  public function __construct($name, array $attributes = NULL) {
    // @todo valid XHTML tag name.
    if (empty($name) || !is_string($name)) {
      throw new \Exception('$name must be a non empty string !');
    }
    $this->name = strtolower($name);
    if (!empty($attributes)) {
      $this->setAttributes($attributes);
    }
  }

  /**
   * The lower-case name of the tag.
   *
   * @var string
   */
  private $name;

  /**
   * The lower-case name of the tag.
   *
   * @return string
   *   The lower-case name of the tag.
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Gets the tag prefix portion formatted as HTML including any attributes.
   *
   * Example:
   *  <a href="http://www.test.com" class="my-link" style="font-weight: bold;">
   *
   * If the tag can't contain any content returns a closed prefix.
   * Example: <hr/>, <br/>, <img src="http://www.test.com/logo.png"/>
   *
   * @return string
   *   A tag prefix portion.
   */
  public function getAsPrefix() {
    $end = $this->isNoChildren($this->name) ? '/>' : '>';
    $atts = "";
    $attributes = $this->getAttributes();
    if (!empty($attributes)) {
      $atts = ' ' . drupal_attributes($attributes);
    }
    return '<' . $this->name . $atts . $end;
  }

  /**
   * Gets the tag suffix portion.
   *
   * Example: </a>
   *
   * Null for self-closing elements like
   * HR and BR that don't have content
   *
   * @return string
   *   Gets the closing suffix for elements that may have children,
   *   or NULL for self-closing children.
   */
  public function getAsSuffix() {
    return $this->isNoChildren($this->name) ? NULL : '</' . $this->name . '>';
  }

  /**
   * Tags that may not have children (are self-closing).
   *
   * @var array
   *
   * @see http://www.standardista.com/html5/xhtml-elements-their-parent-children-attributes-and-default-browser-presentation/
   */
  static public $TAGS_WITH_NO_CHILDREN = array(
    'br',
    'hr',
    'img',
    'base',
    'link',
    'meta',
    'area',
    'col',
    'param',
  );

  // @codingStandardsIgnoreStart
  // Coder: ERROR | Class property $TAGS_WITH_NO_CHILDREN should use lowerCamel
  // naming without underscores
  // Webel: https://www.drupal.org/node/2306473
  // 'OO: Coding standards: permit CAPITALS_WITH_UNDERSCORES
  // for static class variables'
  // @codingStandardsIgnoreEnd

  /**
   * Whether the given tag by name is self-closing (can't have children).
   *
   * @param string $name
   *   The name of the XHTML tag to query.
   *
   * @return bool
   *   Whether the given tag by name is self-closed (can't have children).
   */
  static public function tagNoChildren($name) {
    $n = strtolower($name);
    return in_array($n, self::$TAGS_WITH_NO_CHILDREN);
  }

  /**
   * Whether this is self-closing (can't have children).
   *
   * @return bool
   *   Whether thisis self-closed (can't have children).
   */
  public function isNoChildren() {
    return self::tagNoChildren($this->getName());
  }

}
