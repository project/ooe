<?php

/**
 * @file
 * TagFactory.
 */

namespace Drupal\ooe\Xhtml;

use Drupal\ooe\Xhtml\XHTML;
use Drupal\ooe\Xhtml\Tag;

/**
 * A factory for @link ITag @endlink implementations.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2207 TagFactory @endlink.
 *
 * @todo Cover more cases. So far it handles just enough
 * XHTML/HTML5 tag cases for the simple demos here.
 * Needs to be extended to cover all cases, and possibly
 * also leveraging an existing XHTML/HTML5 library.
 *
 * @todo Currently all creation methods accept a Drupal-style attributes array.
 * This needs to either be changed to accept attributes from an
 * XHTML/HTML5-aware library, or needs to validate the Drupal-style
 * attributes array against permissible attributes using
 * an XHTML/HTML5-aware library.
 *
 * @author darrenkelly
 */
class TagFactory implements ITagFactory {

  /**
   * A new XHTML/HTML5 tag with the given name.
   *
   * @todo Consider passing instead an OOE IAttributes.
   *
   * @param string $name
   *   An XHTML/HTML5 tag name.
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return ITag
   *   A new tag.
   */
  public function newTag($name, array $attributes = NULL) {
    return new Tag($name, $attributes);
  }

  /**
   * A new XHTML/HTML5 P tag.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return ITag
   *   This.
   */
  public function newP(array $attributes = NULL) {
    return new Tag(XHTML::P, $attributes);
  }

  /**
   * A new XHTML/HTML5 DIV tag.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return ITag
   *   This.
   */
  public function newDiv(array $attributes = NULL) {
    return new Tag(XHTML::DIV, $attributes);
  }

  /**
   * A new XHTML/HTML5 SPAN tag.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return ITag
   *   This.
   */
  public function newSpan(array $attributes = NULL) {
    return new Tag(XHTML::SPAN, $attributes);
  }

  /**
   * A new XHTML/HTML5 B tag.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return ITag
   *   This.
   */
  public function newB(array $attributes = NULL) {
    return new Tag(XHTML::B, $attributes);
  }

  /**
   * A new XHTML/HTML5 I tag.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return ITag
   *   This.
   */
  public function newI(array $attributes = NULL) {
    return new Tag(XHTML::I, $attributes);
  }

  /**
   * A new XHTML/HTML5 EM tag.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return ITag
   *   This.
   */
  public function newEm(array $attributes = NULL) {
    return new Tag(XHTML::Em, $attributes);
  }

  /**
   * A new XHTML/HTML5 STRONG tag.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return ITag
   *   This.
   */
  public function newStrong(array $attributes = NULL) {
    return new Tag(XHTML::STRONG, $attributes);
  }

  /**
   * A new XHTML/HTML5 BLOCKQUOTE tag.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return ITag
   *   This.
   */
  public function newBlockquote(array $attributes = NULL) {
    return new Tag(XHTML::BLOCKQUOTE, $attributes);
  }

  /**
   * A new XHTML/HTML5 TABLE tag.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return ITag
   *   This.
   */
  public function newTable(array $attributes = NULL) {
    return new Tag(XHTML::TABLE, $attributes);
  }

  /**
   * A new XHTML/HTML5 TR tag.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return ITag
   *   This.
   */
  public function newTr(array $attributes = NULL) {
    return new Tag(XHTML::TR, $attributes);
  }

  /**
   * A new XHTML/HTML5 TH tag.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return ITag
   *   This.
   */
  public function newTh(array $attributes = NULL) {
    return new Tag(XHTML::TH, $attributes);
  }

  /**
   * A new XHTML/HTML5 TD tag.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return ITag
   *   This.
   */
  public function newTd(array $attributes = NULL) {
    return new Tag(XHTML::TD, $attributes);
  }

}
