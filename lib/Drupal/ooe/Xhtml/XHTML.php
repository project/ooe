<?php

/**
 * @file
 * XHTML.
 */

namespace Drupal\ooe\Xhtml;

/**
 * Helps with valid XHTML/HTML5 names and constants.
 *
 * Currently just enough to demonstrate some render object cases.
 *
 * UML @link http://drupal7demo.webel.com.au/node/2182 XHTML @endlink.
 *
 * @see http://www.w3schools.com/tags/default.asp
 *
 * @see http://www.w3schools.com/html/html_xhtml.asp
 *
 * @todo leverage existing XHTML/HTML5/DOM libs.
 *
 * @todo cover all valid tags.
 *
 * @author darrenkelly
 */
class XHTML {

  const TABLE = 'table';
  const TR = 'tr';
  const TD = 'td';
  const TH = 'th';
  const A = 'a';
  const P = 'p';
  const B = 'b';
  const STRONG = 'strong';
  const I = 'i';
  const EM = 'em';
  const DIV = 'div';
  const IMG = 'img';
  const SPAN = 'span';
  const HR = 'hr';
  const BR = 'br';
  const BLOCKQUOTE = 'blockquote';
  const OL = 'ol';
  const UL = 'ul';
  const DL = 'dl';

}
