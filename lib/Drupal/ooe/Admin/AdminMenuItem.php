<?php
/**
 * @file
 * AdminMenuItem.
 */

namespace Drupal\ooe\Admin;

use Drupal\ooe\Form\FormMenuItem;

/**
 * Assumes page callback for the menu item will be drupal_get_form($form_id).
 *
 * Assumes an admin form (accessed via the page arguments) will
 * be created at or via the .module file form builder function
 * 'MYMODULE_form_admin', where MYMODULE is the machine name
 * of the module passed on construction.
 *
 * Assumes the access is set to 'access administration pages'.
 *
 * This implementation does not use a @link FormController @endlink,
 * and so it only serves to demonstrate old-style Drupal form builders.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2024 AdminMenuItem @endlink
 *
 * @todo OOE style object-oriented admin version of an IFormControllerMenuItem
 *
 * @author darrenkelly
 */
class AdminMenuItem extends FormMenuItem {

  /**
   * Access permissions "constant".
   *
   * @var string .
   */
  private static $ACCESS = array('access administration pages');
  // @codingStandardsIgnoreStart
  // NB: can't use const with array definition:
  // http://stackoverflow.com/questions/11184469/php-use-array-as-class-constant
  // @codingStandardsIgnoreEnd

  /**
   * Constructor.
   *
   * @param string $module
   *   The module machine name.
   * @param string $title
   *   The title of the admin menu item.
   * @param string $path
   *   The menu path.
   */
  public function __construct($module, $title, $path) {
    parent::__construct($module, $title, $path);

    $this->pageArguments = $this->pageArguments();

    // $this->setAccessArguments(self::$ACCESS);
    $this->accessArguments = self::$ACCESS;

    // 'hook_menu() also provides an important part of Drupal's security,
    // as it performs user access checks.
    // We would like only administrators to be able to access this form,
    // and we'll check that permission here in hook_menu().
    // To minimize the number of permissions an administrator has to deal with,
    // we'll use the core administration permission
    // instead of creating a new custom permission.'
  }

  /**
   * Forces the page arguments to meet a convention MYMODULE_form_admin.
   *
   * @return array
   *   An array containing the page arguments as MYMODULE_form_admin.
   */
  private function pageArguments() {
    return array($this->getModule() . "_form_admin");
  }

  /**
   * Overridden to prevent resetting of locally fixed admin value.
   *
   * Never returns.
   *
   * @param string[] $pageArguments
   *   Page arguments.
   *
   * @throws \Exception
   *   Always thrown to prevent reset.
   */
  public function setPageArguments(array $pageArguments) {
    throw new \Exception("IGNORED: page arguments are forced to '" . $this->pageArguments() . "' !");
  }

  /**
   * Overridden to prevent resetting of locally fixed admin value.
   *
   * Never returns.
   *
   * @param string[] $accessArguments
   *   The access arguments string array.
   *
   * @throws \Exception
   *   Always thrown to prevent reset.
   */
  public function setAccessArguments(array $accessArguments) {
    throw new \Exception("IGNORED: access arguments are forced to '" . self::$ACCESS . "' !");
  }

}
