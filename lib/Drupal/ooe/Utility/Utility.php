<?php

/**
 * @file
 * Utility.
 */

namespace Drupal\ooe\Utility;

use Drupal\ooe\Menu\MenuCommon;

/**
 * Utility methods.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2304 Utility @endlink.
 *
 * @author darrenkelly
 */
class Utility {

  /**
   * Creates a Drupal link with the URL also as the title.
   *
   * @param string $url
   *   The URL. Will be used as both link and title.
   *
   * @return string
   *   A Drupal link.
   */
  static public function link($url) {
    return l($url, $url);
  }

  /**
   * Searches the given menu item for a matching menu item's subtree.
   *
   * Recursively searches the given menu for a menu item
   * with the given link path, and returns its child tree
   * (or null if there is no match).
   *
   * This version only searches the accessible menu_tree_page_data(),
   * not menu_tree_all_data().
   *
   * @param string $link_path
   *   The link path to match against.
   * @param string $menu
   *   The name of the menu to search within.
   *
   * @return array
   *   A Drupal menu tree suitable for
   *   formatting to HTML with menu_tree_output().
   *
   * @see menu_tree_output()
   * @see menu_tree_page_data()
   * @see menu_tree_all_data()
   */
  static public function submenu($link_path, $menu = MenuCommon::MENU_NAVIGATION) {
    // @codingStandardsIgnoreStart
    //$subtree  = self::findSubtreeMenu($link_path,menu_tree_page_data($menu));
    // NO, will not load submenu items unless already expanded in page.
    // http://drupal.stackexchange.com/questions/26648/menu-tree-page-data-is-not-loading-sub-menu-items
    // @codingStandardsIgnoreEnd

    $subtree = self::findSubtreeMenu($link_path, menu_tree_all_data($menu));
    return $subtree;
  }

  /**
   * Searches the given menu tree portion for the subtree of a menu item.
   *
   * Recursively searches the given menu tree portion for a
   * menu item with the given link path, and returns its child tree
   * (or null if there is no match).
   *
   * This version only searches the accessible
   * menu_tree_page_data(), not menu_tree_all_data().
   *
   * @param string $link_path
   *   The link path to match against.
   * @param array $tree
   *   A Drupal menu tree portion.
   *
   * @return array
   *   A Drupal menu tree (if found) suitable for
   *   formatting to HTML with menu_tree_output(), or null.
   *
   * @see menu_tree_output()
   *
   * @see menu_tree_page_data()
   *
   * @see menu_tree_all_data()
   */
  static public function findSubtreeMenu($link_path, array $tree) {
    foreach ($tree as $branch) {
      if ($branch['link']['link_path'] == $link_path) {
        return $branch['below'];
      }
      else {
        $found = self::findSubtreeMenu($link_path, $branch['below']);
        if (!empty($found)) {
          return $found;
        }
      }
    }
    return NULL;
  }

}
