<?php

/**
 * @file
 * IFactory.
 */

namespace Drupal\ooe\Factory;

use Drupal\ooe\Page\IPageController;
use Drupal\ooe\Block\IBlock;

/**
 * Interface for factories for common Drupal items.
 *
 * Includes menu items, blocks, block views and theme regions.
 *
 * Because the factory has knowledge of the module machine name,
 * the signatures of the creation methods are more concise than
 * the matching constructors of the classes for some products
 * (do not have to include the module machine name as a parameter).
 * This is a very simple example of the kind of "policy" value adding
 * a factory can offer.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/1807 IFactory @endlink
 *
 * Visit also:
 *
 * @link http://drupal7demo.webel.com.au/node/1810 Abstract factory pattern @endlink
 *
 * @link IRenderFactory @endlink
 *
 * @author darrenkelly
 */
interface IFactory {

  /**
   * Gets the module machine name.
   *
   * @return string
   *   The module machine name.
   */
  public function getModuleName();

  /**
   * Supplies a basic menu item product.
   *
   * @param string $title
   *   Required !
   * @param string $path
   *   Required !
   *
   * @return \Drupal\ooe\Menu\IMenuItem
   *   A new menu item.
   */
  public function newMenuItem($title, $path);

  /**
   * Supplies a controlled page menu item product.
   *
   * @param IPageController $pageController
   *   Required. The controller for the page.
   * @param string $title
   *   Required. The title of the menu item.
   * @param string $path
   *   Required. The path of the menu item.
   *
   * @return \Drupal\ooe\Page\IPageMenuItem
   *   A new page menu item.
   */
  public function newPageMenuItem(IPageController $pageController, $title, $path);

  /**
   * Supplies an admin menu item product.
   *
   * @param string $title
   *   Required. The title of the menu item.
   * @param string $path
   *   Required. The path of the menu item.
   *
   * @return \Drupal\ooe\Menu\IMenuItem
   *   A new menu item.
   */
  public function newAdminMenuItem($title, $path);

  /**
   * Supplies a form menu item product.
   *
   * @param string $title
   *   The title.
   * @param string $path
   *   The path.
   *
   * @return \Drupal\ooe\Form\IFormMenuItem
   *   A new form menu item.
   */
  public function newFormMenuItem($title, $path);

  /**
   * Supplies a block set/group product.
   *
   * @return \Drupal\ooe\Block\IBlockSet
   *   A new block set.
   */
  public function newBlockSet();

  /**
   * Supplies a block view product.
   *
   * @todo Consider moving the subject into the block.
   *
   * @param IBlock $block
   *   The block for which this block view products acts as a view.
   * @param string $subject
   *   The (translated) subject of the block view.
   *
   * @return \Drupal\ooe\Block\IBlockView
   *   A new block view.
   */
  public function newBlockView(IBlock $block, $subject);

  /**
   * Supplies a block product.
   *
   * @param string $delta
   *   Required. The block delta identifier.
   * @param string $info
   *   Required. Translated ! The block info. From hook_block_info():
   *   'The human-readable administrative name of the block'.
   *
   * @return \Drupal\ooe\Block\IBlock
   *   A new block.
   */
  public function newBlock($delta, $info);

  /**
   * Supplies a menu item set/group product.
   *
   * @return \Drupal\ooe\Menu\IMenuItemSet
   *   A new menu item set.
   */
  public function newMenuItemSet();

  /**
   * Supplies a theme region product.
   *
   * It is recommended that the name be obtained via
   * a class that encapsulates constants representing
   * the names of regions for a given theme.
   *
   * @param string $name
   *   A non-null name that identifies the layout region.
   *
   * @return \Drupal\ooe\Layout\IRegion
   *   A new layout region.
   *
   * @see \Drupal\ooe\Theme\BartikRegions
   */
  public function newRegion($name);

  /**
   * Supplies a new @link MenuTabs @endlink as product.
   *
   * Configured
   * with the base page controller that will act for the base
   * page and the 1st tab.
   *
   * @param string $path
   *   Required. The path of the primary page menu item.
   * @param IPageController $pageControllerBase
   *   Required. The page controller for the primary page.
   * @param string $title
   *   Required. The title of primary page menu item.
   *
   * @return \Drupal\ooe\Menu\IMenuTabs
   *   A new menu tabs.
   */
  public function newMenuTabs($path, IPageController $pageControllerBase, $title);

}
