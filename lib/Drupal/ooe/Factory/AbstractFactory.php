<?php

/**
 * @file
 * AbstractFactory.
 */

namespace Drupal\ooe\Factory;

use Drupal\ooe\Block\BlockSet;
use Drupal\ooe\Menu\DefaultMenuItem;
use Drupal\ooe\Page\PageMenuItem;
use Drupal\ooe\Admin\AdminMenuItem;
use Drupal\ooe\Form\FormMenuItem;
use Drupal\ooe\Menu\MenuTabs;
use Drupal\ooe\Menu\MenuItemSet;
use Drupal\ooe\Page\IPageController;
use Drupal\ooe\Factory\IFactory;
use Drupal\ooe\Block\DefaultBlock;
use Drupal\ooe\Layout\DefaultRegion;

/**
 * Helps implement factory implementations by providing "default" products.
 *
 * Please note that it does not provide a @link IBlockView @endlink product,
 * because the block view will be specific to a given application
 * or @link IProject @endlink implementation.
 *
 * None of the implementations here are 'final', so alternative implementations
 * may choose to extend this and override a subset of these implementations,
 * in order to choose alernative products,
 * or in order to re-configure products.
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/1808 AbstractFactory @endlink
 *
 * Visit also:
 * @link http://drupal7demo.webel.com.au/node/1810 Abstract factory pattern @endlink
 *
 * @author darrenkelly
 */
abstract class AbstractFactory implements IFactory {

  /**
   * Constructor.
   *
   * @todo Consider inject instead IModule
   *
   * @param string $moduleName
   *   The module machine name.
   */
  public function __construct($moduleName) {
    $this->setModuleName($moduleName);
  }

  /**
   * The module machine name.
   *
   * @var string $moduleName
   */
  private $moduleName;

  /**
   * The module machine name.
   *
   * @return string
   *   The module machine name.
   */
  public function getModuleName() {
    return $this->moduleName;
  }

  /**
   * Sets the module machine name.
   *
   * Not exposed, should always inject via constructor.
   *
   * @param string $moduleName
   *   A non empty module machine name.
   *
   * @return IFactory
   *   This.
   */
  private function setModuleName($moduleName) {
    if (empty($moduleName) || !is_string($moduleName)) {
      throw new \Exception('$module must be a non null string');
    }
    $this->moduleName = $moduleName;
    return $this;
  }

  /**
   * Supplies a new @link DefaultBlock @endlink as product.
   *
   * @param string $delta
   *   The block delta.
   * @param string $info
   *   The (translated) block info.
   *
   * @return \Drupal\ooe\Block\IBlock
   *   A new default block.
   */
  public function newBlock($delta, $info) {
    return new DefaultBlock($delta, $info);
  }

  /**
   * Supplies a new @link BlockSet @endlink as product.
   *
   * @return \Drupal\ooe\Block\IBlockSetet
   *   A new block set.
   */
  public function newBlockSet() {
    return new BlockSet();
  }

  // @codingStandardsIgnoreStart
  // abstract public function newBlockView($delta, $subject);
  // @codingStandardsIgnoreEnd

  /**
   * Supplies a new @link DefaultMenuItem @endlink as product.
   *
   * @return \Drupal\ooe\Menu\IMenuItem
   *   A new default menu item.
   */
  public function newMenuItem($title, $path) {
    return new DefaultMenuItem($this->getModuleName(), $title, $path);
  }

  /**
   * Supplies a new @link PageMenuItem @endlink as product.
   *
   * Configured with the given page controller.
   *
   * @param IPageController $pageController
   *   The page controller for the menu item.
   * @param string $title
   *   The title of the menu item.
   * @param string $path
   *   The path of the menu item.
   *
   * @return \Drupal\ooe\Page\IPageMenuItem
   *   A new page menu item.
   */
  public function newPageMenuItem(IPageController $pageController, $title, $path) {
    return new PageMenuItem($pageController, $this->getModuleName(), $title, $path);
  }

  /**
   * Supplies a new @link AdminMenuItem @endlink as product.
   *
   * @param string $title
   *   The title of the menu item.
   * @param string $path
   *   The path of the menu item.
   *
   * @return \Drupal\ooe\Menu\IMenuItem
   *   A new admin menu item.
   */
  public function newAdminMenuItem($title, $path) {
    return new AdminMenuItem($this->getModuleName(), $title, $path);
  }

  /**
   * Supplies a new @link FormMenuItem @endlink as product.
   *
   * @param string $title
   *   The title of the menu item.
   * @param string $path
   *   The path of the menu item.
   *
   * @return \Drupal\ooe\Form\IFormMenuItem
   *   A new form menu item.
   */
  public function newFormMenuItem($title, $path) {
    return new FormMenuItem($this->getModuleName(), $title, $path);
  }

  /**
   * Supplies a new @link MenuTabs @endlink as product.
   *
   * Configured
   * with the base page controller that will act for the base
   * page and the 1st tab.
   *
   * @param string $path
   *   Required. The path of the primary page menu item.
   * @param IPageController $pageControllerBase
   *   Required. The page controller for the primary page.
   * @param string $title
   *   Required. The title of primary page menu item.
   *
   * @return \Drupal\ooe\Menu\IMenuTabs
   *   A new menu tabs.
   */
  public function newMenuTabs($path, IPageController $pageControllerBase, $title) {
    return new MenuTabs($this->getModuleName(), $path, $pageControllerBase, $title);
  }

  /**
   * Supplies a new @link MenuItemSet @endlink as product.
   *
   * @return \Drupal\ooe\Menu\IMenuItemSet
   *   A new menu item set.
   */
  public function newMenuItemSet() {
    return new MenuItemSet();
  }

  /**
   * Supplies a new @link DefaultRegion @endlink as product.
   *
   * It is recommended that the region name be obtained via
   * a class that encapsulates constants representing
   * the names of regions for a given theme.
   *
   * @param string $name
   *   A non-null name that identifies the layout region.
   *
   * @return \Drupal\ooe\Layout\IRegion
   *   A new layout region.
   *
   * @see \Drupal\ooe\Theme\BartikRegions
   */
  public function newRegion($name) {
    return new DefaultRegion($name);
  }

}
