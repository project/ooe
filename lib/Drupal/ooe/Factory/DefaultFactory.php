<?php

/**
 * @file
 * DefaultFactory.
 */

namespace Drupal\ooe\Factory;

use Drupal\ooe\Block\StubBlockView;

use Drupal\ooe\Block\IBlock;

/**
 * Implements an @link IFactory @endlink by providing default products.
 *
 * It provides a @link StubBlockView @endlink
 * as @link IBlockView @endlink product
 * for those @link IProject @endlink implementations that don't intend to
 * use a block, even though a block is provided (in such cases usually hidden).
 *
 * Mostly a real-world @link IProject @endlink implementation should
 * instead use a factory that extends @link AbstractFactory @endlink
 * and provides a block view of use to that project.
 *
 * This default project is provided so that there is at least
 * a concrete factory implementation that can be used by demos.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/1809 DefaultFactory @endlink
 *
 * Visit also:
 * @link http://drupal7demo.webel.com.au/node/1810 Abstract factory pattern @endlink
 *
 * @author darrenkelly
 */
class DefaultFactory extends AbstractFactory {

  /**
   * Constructor.
   *
   * @todo Consider inject instead IModule
   *
   * @param string $moduleName
   *   The module machine name.
   */
  public function __construct($moduleName) {
    parent::__construct($moduleName);
  }

  /**
   * Provides a new @link StubBlockView @endlink as a block view product.
   *
   * An @link IProject @endlink implementation that requires a
   * specific block view could use a factory that
   * overrides this with a more suitable block view,
   * or could instead provide a factory that
   * extends @link AbstractFactory @endlink and provides
   * a block view suitable for that project.
   *
   * @param IBlock $block
   *   The block for which this acts as a view.
   * @param string $subject
   *   The (translated) subject of the block.
   *
   * @return \Drupal\ooe\Block\IBlockView
   *   A stub block with stub content.
   */
  public function newBlockView(IBlock $block, $subject) {
    return new StubBlockView($block, $subject);
  }

}
