<?php

/**
 * @file
 * AbstractModuleMap.
 */

namespace Drupal\ooe\Module;

/**
 * Implements common aspects of a module map (object-oriented module bridge).
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/1026 AbstractModuleMap @endlink.
 *
 * @todo Consider renaming to AbstractModuleBridge.
 *
 * @author darrenkelly
 */
abstract class AbstractModuleMap extends DefaultModuleHelper implements IModuleMap {

  /**
   * Constructor.
   *
   * @param string $module
   *   The module machine name.
   */
  public function __construct($module) {
    // Not protected ! Because parent requires public.

    parent::__construct($module);
    // @todo: consider instead passing in an IModule.
  }

}
