<?php

/**
 * @file
 * DefaultModuleHelper.
 */

namespace Drupal\ooe\Module;

use Drupal\ooe\Factory\IFactory;
use Drupal\ooe\Factory\DefaultFactory;

/**
 * Classes that need to keep a record of the name of
 * the module they serve may choose to extend this.
 *
 * It manages a settable factory, and provides a default factory.
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/1014 DefaultModuleHelper @endlink.
 *
 * @todo Consider instead or also using an IModule delegate instead
 * of just passing a $module string around. Migration required.
 *
 * @author darrenkelly
 */
class DefaultModuleHelper implements IModuleHelper {

  /**
   * The module machine name.
   *
   * @todo Consider rename to $moduleName.
   *
   * @var string $module
   */
  private $module;

  /**
   * Sets the module machine name.
   *
   * Not needed by public, should always inject via constructor.
   *
   * @todo Consider rename to setModuleName.
   *
   * @param string $module
   *   Required. A non empty module machine name.
   *
   * @return IModuleHelper
   *   This.
   */
  private function setModule($module) {
    if (empty($module) || !is_string($module)) {
      throw new \Exception('$module must be a non null string');
    }
    $this->module = $module;
    return $this;
  }

  /**
   * Gets the module machine name.
   *
   * @todo Consider rename to getModuleName.
   *
   * @return string
   *   The module machine name.
   */
  public function getModule() {
    return $this->module;
  }

  /**
   * Constructor.
   *
   * @param string $module
   *   Required. The machine name of the module.
   * @param IFactory $factory
   *   Optional factory; if none given or null
   *   a @link DefaultFactory @endlink will be used.
   */
  public function __construct($module, IFactory $factory = NULL) {
    if (empty($module) || !is_string($module)) {
      throw new \Exception('$module must be a non-null string !');
    }
    $this->setModule($module);
    if (!empty($factory)) {
      $this->setFactory($factory);
    }
  }

  /**
   * The current factory of this.
   *
   * @var \Drupal\ooe\Factory\IFactory
   */
  private $factory;

  /**
   * The current factory.
   *
   * @return \Drupal\ooe\Factory\IFactory
   *   The current factory. Will always be non-null.
   */
  public function getFactory() {
    return $this->factory();
  }

  /**
   * The current factory or a lazily created @link DefaultFactory @endlink.
   *
   * @return \Drupal\ooe\Factory\IFactory
   *   The currently set factory, otherwise
   *   lazily creates a fresh @link DefaultFactory @endlink.
   */
  protected function factory() {
    // @todo Consider promote and use static myFactory to choose default.
    if (empty($this->factory)) {
      $this->factory = new DefaultFactory($this->getModule());
    }
    return $this->factory;
  }

  /**
   * The current factory, or a lazily created @link DefaultFactory @endlink.
   *
   * Convenient shorthand version
   * of @link DefaultModuleHelper::factory() @endlink
   * since used so often.
   *
   * @return \Drupal\ooe\Factory\IFactory
   *   The currently set factory, otherwise a
   *   lazily created @link DefaultFactory @endlink.
   */
  protected function f() {
    return $this->factory();
  }

  /**
   * Sets the factory of this.
   *
   * The module machine name of the provided factory must
   * match the module machine name known to this.
   *
   * @param \Drupal\ooe\Factory\IFactory $factory
   *   The factory.
   *
   * @return \Drupal\ooe\Module\IModuleHelper
   *   This.
   *
   * @throws \Exception
   *   If $factory is empty or the module machine name
   *   of the factory is inconsistent.
   */
  protected function setFactory(IFactory $factory) {
    if (empty($factory)) {
      throw new \Exception('IFactory $factory must not be empty !');
    }
    if ($factory->getModuleName() != $this->getModule()) {
      // @codingStandardsIgnoreStart
      throw new Exception(
      'The module machine name(' . $factory->getModuleName() .
      ') of the provided factory does match' .
      'The module machine name(' . $this->getModule() . ') of this !'
          );
      // Coder: ERROR | String concat is not required here;
      // use a single string instead
      // Webel: Bug report: https://www.drupal.org/node/2305591
      // @codingStandardsIgnoreEnd
    }
    $this->factory = $factory;
    return $this;
  }

  /**
   * A human readable and displayable name of the module.
   *
   * @return string
   *   The module display name (if set) or
   *   the module machine name as upper case.
   */
  public function getModuleDisplayName() {
    if (!empty($this->displayName)) {
      return $this->displayName;
    }
    return strtoupper($this->module);
  }

  /**
   * A human readable and displayable name of the module.
   *
   * @var string $displayName
   */
  private $displayName;

  /**
   * Sets a human readable and displayable name of the module.
   *
   * @param string $displayName
   *   A human readable and displayable name of the module.
   *
   * @return IModuleHelper
   *   This.
   */
  public function setModuleDisplayName($displayName) {
    $this->displayName = $displayName;
    return $this;
  }

}
