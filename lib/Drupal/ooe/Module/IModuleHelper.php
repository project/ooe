<?php

/**
 * @file
 * IModuleHelper.
 */

namespace Drupal\ooe\Module;

/**
 * Interfaces that need to keep a record of the machine name
 * and human readable display name of the module
 * they serve may choose to extend this.
 *
 * Also manages an @link IFactory @endlink.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/1013 IModuleHelper @endlink
 *
 * @todo Consider providing with an IModule delegate.
 *
 * @author darrenkelly
 */
interface IModuleHelper {

  /**
   * The Drupal machine name of the module as a string (all lower case).
   *
   * @todo Consider rename to getModuleName.
   *
   * @return string
   *   The Drupal machine name of the module as a string (all lower case).
   */
  public function getModule();

  /**
   * A human readable and displayable name of the module.
   *
   * @return string
   *   The module display name (if set)
   *   or the module machine name as upper case.
   */
  public function getModuleDisplayName();

  /**
   * Sets a human readable and displayable name of the module.
   *
   * @param string $displayName
   *   A human readable and displayable name of the module.
   *
   * @return IModuleHelper
   *   This.
   */
  public function setModuleDisplayName($displayName);

  /**
   * Either the current factory or a default factory provided on-the-fly.
   *
   * Never NULL !
   *
   * @return \Drupal\ooe\Factory\IFactory
   *   The currently set factory, otherwise some kind
   *   of default concrete factory should be supplied.
   */
  public function getFactory();

}

/**
 * Set the current factory.
 *
 * @param \Drupal\ooe\Factory\IFactory $factory
 *   The factory to set as current.
 *
 * @return \Drupal\ooe\Module\IModuleHelper
 *
 * @throws \Exception If $factory is null or empty.
 */
// @codingStandardsIgnoreStart
// public function setFactory(IFactory $factory);
// @codingStandardsIgnoreEnd

/**
 * Set the Drupal machine name of the module (all lower case).
 *
 * @param string $module
 *
 * @return \Drupal\ooe\Module\IModuleHelper
 */
// @codingStandardsIgnoreStart
// public function setModule($module);//NO always set by construction.
// @codingStandardsIgnoreEnd