<?php

/**
 * @file
 * IModuleMap.
 */

namespace Drupal\ooe\Module;

/**
 * A module map serves only to encapsulate the contents of every
 * method of the the main .module file of a single module.
 *
 * Every single function of an OOE style module should delegate
 * to a matching method here with identical parameter signature.
 *
 * This enables PHP-to-UML extractors to represent and hook functions
 * and "by convention" handler methods of the .module as class methods.
 *
 * The ability to represent Drupal in graphical Unified Modeling Language (UML)
 * is a major contribution and priority of the OOE tutorial module.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/1025 IModuleMap @endlink
 *
 * @todo Consider renaming to ModuleBridge.
 *
 * @author darrenkelly
 */
interface IModuleMap extends IModuleHelper {

}
