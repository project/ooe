<?php

/**
 * @file
 * MenuCommon.
 */

namespace Drupal\ooe\Menu;

/**
 * Some constants common to many menu aspects.
 *
 * This promotes
 * @link http://drupal7demo.webel.com.au/node/10 DRY @endlink coding.
 *
 * However, constant names for frequently used named menus are not
 * easily extensible at run time or adaptable to complex themes.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2216 MenuCommon @endlink.
 *
 * @author darrenkelly
 */
class MenuCommon {

  /**
   * Navigation menu machine name.
   *
   * @var
   */
  const MENU_NAVIGATION = 'navigation';

  /**
   * User menu machine name.
   *
   * @var
   */
  const MENU_USER = 'user-menu';

  /**
   * Management menu machine name.
   *
   * @var
   */
  const MENU_MANAGEMENT = 'management';

  /**
   * Main menu machine name.
   *
   * @var
   */
  const MENU_MAIN = 'main-menu';

  /**
   * Development menu machine name.
   *
   * @var
   */
  const MENU_DEVEL = 'devel-menu';

}
