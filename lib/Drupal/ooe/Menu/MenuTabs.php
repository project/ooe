<?php

/**
 * @file
 * MenuTabs.
 */

namespace Drupal\ooe\Menu;

use Drupal\ooe\Module\DefaultModuleHelper;
use Drupal\ooe\Page\IPageController;
use Drupal\ooe\Page\IPageMenuItem;
use Drupal\ooe\Factory\IFactory;

/**
 * Implements management of a group of menu tabs.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/1016 MenuTabs @endlink.
 *
 * @author darrenkelly
 */
class MenuTabs extends DefaultModuleHelper implements IMenuTabs {

  /**
   * The path portion of the URL for the menu set.
   *
   * @var string
   */
  private $path;

  /**
   * The base path page controller (will be shared with the 1st tab menu item).
   *
   * @var \Drupal\ooe\Page\IPageController
   */
  private $pageControllerBase;

  /**
   * The controlled page menu item for the base path of the tabs.
   *
   * It will have the same controller as 1st (default) tab menu item.
   *
   * @var \Drupal\ooe\Page\IPageMenuItem
   */
  private $menuItemBase;

  /**
   * The controlled page menu item for the default path of the tabs (1st tab).
   *
   * It will have the same controller as the base menu item.
   *
   * @var \Drupal\ooe\Page\IPageMenuItem
   */
  private $menuItemDefault;

  /**
   * Constructor.
   *
   * @param string $module
   *   Module machine name string.
   * @param string $path
   *   Path to the base (primary) menu item.
   * @param IPageController $pageControllerBase
   *   Controller for the main page (and 1st tab).
   * @param string $title
   *   Title of the main page menu (and first tab).
   * @param IFactory $factory
   *   Optional factory.
   */
  public function __construct(
  $module, $path, IPageController $pageControllerBase, $title, IFactory $factory = NULL
  ) {
    parent::__construct($module, $factory);
    $this->path = $path;
    $this->pageControllerBase = $pageControllerBase;

    $this->menuItemBase = $this->factory()->newPageMenuItem(
        $pageControllerBase, $title, $path);
    $this->menuItemBase->forceAccessCallbackTrue();
    // Will also be inherited into default tab item.

    $this->addMenuItem($this->menuItemBase);

    $this->menuItemDefault = $this->makeTabMenuItem(
        $this->pageControllerBase,
        NULL, // $title: Rely on default tab title assignment by item count.
        // Coder: ERROR | Comments may not appear after statements.
        // Webel: https://www.drupal.org/node/2304897
        // 'Please relax rule for inline Comments: ..'.
        TRUE // $isDefault.
    );
    // The page controller is shared between the
    // "main/base" page and the 1st tab !
    $this->addMenuItem($this->menuItemDefault);
  }

  /**
   * The menu items handled by this.
   *
   * @var \Drupal\ooe\Menu\IMenuItem[]
   */
  private $menuItems = array();

  /**
   * Adds a page menu item to the set of menu items handled by this.
   *
   * @param \Drupal\ooe\Page\IPageMenuItem $menuItem
   *   The menu item to add to this.
   */
  protected function addMenuItem(IPageMenuItem $menuItem) {
    $this->menuItems[] = $menuItem;
  }

  /**
   * The menu items handled by this.
   *
   * @return \Drupal\ooe\Menu\IMenuItem[]
   *   The array of menu items handled by this.
   */
  public function getMenuItems() {
    return $this->menuItems;
  }

  /**
   * Makes a new tab menuitem with the given page controller and title.
   *
   * @param \Drupal\ooe\Page\IPageController $pageController
   *   The page controller for the tab menu item.
   * @param string $title
   *   The title of the menu item.
   * @param bool $isDefault
   *   Whether this is a default.
   *
   * @return \Drupal\ooe\Page\IPageMenuItem
   *   The new tab menu item.
   *
   * @throws \Exception
   *   If $isDefault is not a boolean.
   */
  protected function makeTabMenuItem(IPageController $pageController, $title, $isDefault = FALSE) {
    if (!is_bool($isDefault)) {
      throw new \Exception('$isDefault must be a boolean !');
    }

    $n = count($this->menuItems);
    // !
    // Webel: re Coder: https://www.drupal.org/node/2304897#comment-8978433.

    $path = $this->path . "/tab" . $n;

    if (empty($title)) {
      $title = "Tab$n";
    }

    $m = $this->factory()->newPageMenuItem($pageController, $title, $path);

    if ($isDefault) {
      // $m->forceAccessCallbackTRUE(); // NO, inherited.
      $m->setTypeDefaultLocalTask(TRUE);
    }
    else {
      $m->forceAccessCallbackTrue();
      $m->setTypeLocalTask(TRUE);
    }
    return $m;
  }

  /**
   * Creates a new tab menu item configured with a page controller.
   *
   * @param IPageController $pageController
   *   Page controller for the page triggered by the tab.
   * @param string $title
   *   Title of the tab.
   *
   * @return \Drupal\ooe\Page\IPageMenuItem
   *   The new page menu item for a tab.
   */
  public function newTabMenuItem(IPageController $pageController, $title) {
    $m = $this->makeTabMenuItem($pageController, $title, FALSE);
    $this->addMenuItem($m);
    return $m;
  }

  /**
   * Sets the weight of the main (parent) menu item.
   *
   * @param int $weight
   *   The weight of the main (parent) menu item of this.
   *
   * @return IMenuTabs
   *   This.
   */
  public function setWeight($weight) {
    $this->menuItemBase->setWeight($weight);
    return $this;
  }

}
