<?php

/**
 * @file
 * IMenuItem.
 */

namespace Drupal\ooe\Menu;

use Drupal\ooe\Module\IModuleHelper;

/**
 * An object-oriented encapsulation of (much of) hook_menu().
 *
 * @todo There are many other options described under hook_menu()
 * not yet represented here, only the essentials for basic examples
 * are present so far.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/1011 IMenuItem @endlink
 *
 * @see hook_menu()
 *
 * @author darrenkelly
 */
interface IMenuItem extends IModuleHelper {

  /**
   * Sets the title of the menu.
   *
   * From hook_menu(): 'Required. The untranslated title of the menu item'.
   *
   * Visit also:
   * @link https://www.drupal.org/node/323101 Strings at well-known places: built-in menus, .. @endlink
   *
   * @param string $title
   *   'The untranslated title of the menu item'.
   *
   * @return IMenuItem
   *   This.
   */
  public function setTitle($title);

  /**
   * Sets the path portion of the URL.
   *
   * @param string $path
   *   The path portion of the URL.
   *
   * @return IMenuItem
   *   This.
   */
  public function setPath($path);

  /**
   * Sets (from hook_menu()): 'The untranslated description of the menu item'.
   *
   * Visit also:
   * @link https://www.drupal.org/node/323101 Strings at well-known places: built-in menus, .. @endlink.
   *
   * @param string $description
   *   'The untranslated description of the menu item'.
   *
   * @return IMenuItem
   *   This.
   */
  public function setDescription($description);

  /**
   * The path portion of the URL.
   *
   * @return string
   *   The path portion of the URL.
   */
  public function getPath();

  /**
   * Sets the page callback.
   *
   * From hook_menu():
   * '"page callback": The function to call to display a web page when
   * the user visits the path. If omitted, the parent menu item's callback
   * will be used instead.'
   *
   * @param string $pageCallback
   *   A non-empty string representing the page callback.
   *
   * @return IMenuItem
   *   This.
   */
  public function setPageCallback($pageCallback);

  /**
   * Sets the menu type to be a MENU_NORMAL_ITEM menu item (if TRUE).
   *
   * If TRUE, sets the menu type to be a MENU_NORMAL_ITEM menu item;
   * Ignored if FALSE.
   *
   * From hook_menu():
   * 'MENU_NORMAL_ITEM: Normal menu items show up in the menu tree
   * and can be moved/hidden by the administrator.'
   *
   * @param bool $isTypeNormal
   *   Whether the type should be set to MENU_NORMAL_ITEM.
   *
   * @return IMenuItem
   *   This.
   */
  public function setTypeNormal($isTypeNormal);

  /**
   * Sets the menu type to be a MENU_DEFAULT_LOCAL_TASK (if TRUE).
   *
   * If TRUE, sets the menu type to be a MENU_DEFAULT_LOCAL_TASK
   * the first (default) of a set of menu tabs,
   * or reverts to a MENU_NORMAL_ITEM if FALSE.
   *
   * From hook_menu(): 'MENU_DEFAULT_LOCAL_TASK: Every set of local tasks
   * should provide one "default" task, which should display the same
   * page as the parent item.'
   *
   * @param bool $isTypeDefaultLocalTask
   *   Whether the type should be set to MENU_DEFAULT_LOCAL_TASK.
   *
   * @return IMenuItem
   *   This.
   */
  public function setTypeDefaultLocalTask($isTypeDefaultLocalTask);

  /**
   * Sets the menu type to be a MENU_LOCAL_TASK (if TRUE).
   *
   * If TRUE, sets the menu type to be a MENU_LOCAL_TASK,
   * usually one of a set of menu tabs,
   * or reverts to a MENU_NORMAL_ITEM if FALSE.
   *
   * From hook_menu():
   * 'MENU_LOCAL_TASK: Local tasks are menu items that describe
   * different displays of data, and are generally rendered as tabs.'
   *
   * @param bool $isTypeLocalTask
   *   Whether the type should be set to MENU_LOCAL_TASK.
   *
   * @return IMenuItem
   *   This.
   */
  public function setTypeLocalTask($isTypeLocalTask);

  /**
   * Sets the menu type to be a MENU_LOCAL_ACTION (if TRUE).
   *
   * If TRUE, sets the menu type to be a MENU_LOCAL_ACTION,
   * one of a set of menu tabs,
   * or reverts to a MENU_NORMAL_ITEM if FALSE.
   *
   * From hook_menu():
   * 'MENU_LOCAL_ACTION: Local actions are menu items that describe
   * actions on the parent item such as adding a new user or block,
   * and are rendered in the action-links list in your theme.'
   *
   * @param bool $isTypeLocalAction
   *   Whether the type should be set to MENU_LOCAL_ACTION.
   *
   * @return IMenuItem
   *   This.
   */
  public function setTypeLocalAction($isTypeLocalAction);

  /**
   * Sets the menu type to be a MENU_SUGGESTED_ITEM (if TRUE).
   *
   * If TRUE, sets the menu type to be a MENU_SUGGESTED_ITEM,
   * or reverts to a MENU_NORMAL_ITEM if FALSE.
   *
   * From hook_menu(): 'MENU_SUGGESTED_ITEM: Modules may "suggest"
   * menu items that the administrator may enable.'
   *
   * From MENU_SUGGESTED_ITEM:
   * 'They act just as callbacks do until enabled,
   * at which time they act like normal items.'
   *
   * @param bool $isTypeSuggestedItem
   *   Whether the type should be set to MENU_SUGGESTED_ITEM.
   *
   * @return IMenuItem
   *   This.
   */
  public function setTypeSuggestedItem($isTypeSuggestedItem);

  /**
   * Sets the menu type to be a MENU_CALLBACK menu item (if TRUE).
   *
   * If TRUE, sets the menu type to be a MENU_CALLBACK menu item;
   * Ignored if FALSE.
   *
   * From hook_menu(): 'MENU_CALLBACK: Callbacks simply
   * register a path so that the
   * correct information is generated when the path is accessed.'
   *
   * @param bool $isTypeCallback
   *   Whether the type should be set to MENU_CALLBACK.
   *
   * @return IMenuItem
   *   This.
   */
  public function setTypeCallback($isTypeCallback);

  /**
   * Sets the bitmask of flags describing properties of the menu item.
   *
   * Specific setters are also provided here for all of these.
   *
   * From hook_menu():
   * '"type": A bitmask of flags describing properties of the menu item.'
   *
   * 'Many shortcut bitmasks are provided as constants in menu.inc:
   *
   * - MENU_NORMAL_ITEM: Normal menu items show up in the menu tree
   *   and can be moved/hidden by the administrator.
   * - MENU_CALLBACK: Callbacks simply register a path so that the
   *   correct information is generated when the path is accessed.
   * - MENU_SUGGESTED_ITEM: Modules may "suggest" menu items
   *   that the administrator may enable.
   * - MENU_LOCAL_ACTION: Local actions are menu items that describe
   *   actions on the parent item such as adding a new user or block,
   *   and are rendered in the action-links list in your theme.
   * - MENU_LOCAL_TASK: Local tasks are menu items that describe
   *   different displays of data, and are generally rendered as tabs.
   * - MENU_DEFAULT_LOCAL_TASK: Every set of local tasks should
   *   provide one "default" task, which should display the same page
   *   as the parent item.
   *
   * If the "type" element is omitted, MENU_NORMAL_ITEM is assumed.'
   *
   * Visit also:
   * @link https://api.drupal.org/api/drupal/modules!system!system.api.php/function/hook_menu/7 Menu item types @endlink
   *
   * @var int $typeBitmask
   *   A non empty type bitmask that is a valid
   *   combination of Drupal menu bitmask types.
   *
   * @return IMenuItem
   *   This.
   */
  public function setType($typeBitmask);

  /**
   * Sets the access arguments to pass to the access callback function.
   *
   * From hook_menu():
   * 'An array of arguments to pass to the access callback function ...
   *  If the access callback is inherited ... ,
   * the access arguments will be inherited with it,
   * unless overridden in the child menu item.'
   *
   * @param string[] $accessArguments
   *   An array of string arguments to pass
   *   to the access callback function.
   *
   * @return IMenuItem
   *   This.
   */
  public function setAccessArguments(array $accessArguments);

  /**
   * Sets the page arguments to pass to the page callback function.
   *
   * From hook_menu():
   * '"page arguments": An array of arguments
   * to pass to the page callback function,
   * with path component substitution ...'
   *
   * @param string[] $pageArguments
   *   An array of page argument strings
   *   to pass to the page callback function.
   *
   * @return IMenuItem
   *   This.
   */
  public function setPageArguments(array $pageArguments);

  /**
   * A Drupal array compatible with the array for an item for hook_menu().
   *
   * @return array
   *   A Drupal array compatible with
   *   the array for an item for hook_menu().
   */
  public function get();

  /**
   * Sets the access callback to a string representing a callback function.
   *
   * For forcing it to a boolean TRUE or FALSE please
   * use @link IMenuItem::forceAccessCallbackTRUE @endlink()
   * OR @link IMenuItem::forceAccessCallbackFALSE @endlink().
   *
   * Integer values are not supported (yet) and mostly not needed.
   *
   * @param string $accessCallback
   *   An access callback string representing an access callback function.
   *
   * @return IMenuItem
   *   This.
   *
   * @see hook_menu
   */
  public function setAccessCallback($accessCallback);

  /**
   * Forces the access callback to be TRUE.
   *
   * @see IMenuItem::forceAccessCallbackTrue()
   *
   * @see IMenuItem::setAccessCallback(string)
   *
   * @return IMenuItem
   *   This.
   */
  public function forceAccessCallbackTrue();

  /**
   * Forces the access callback to be FALSE.
   *
   * @see IMenuItem::forceAccessCallbackFalse()
   *
   * @see IMenuItem::setAccessCallback(string)
   *
   * @return IMenuItem
   *   This.
   */
  public function forceAccessCallbackFalse();

  /**
   * Sets the menu item weight.
   *
   * From hook_menu();
   * '"weight": An integer that determines the relative position of items
   * in the menu; higher-weighted items sink. Defaults to 0.
   * Menu items with the same weight are ordered alphabetically.'
   *
   * @param int $weight
   *   An integer that determines the relative position of items in the menu.
   *
   * @return IMenuItem
   *   This.
   */
  public function setWeight($weight);

  /**
   * Sets the name of a custom menu this should appear in.
   *
   * The custom menu will usually be other than Navigation.
   *
   * From hook_menu():
   * 'Optional. Set this to a custom menu if you
   * don't want your item to be placed in Navigation.'
   *
   * CAUTION: this is only safe to use if the
   * name of an already created menu is known !
   *
   * @param string $menuName
   *   The name of a custom menu.
   *
   * @return IMenuItem
   *   This.
   */
  public function setMenuName($menuName);

  /**
   * Sets whether the menu item is expanded.
   *
   * From hook_menu():
   * 'Optional. If set to TRUE, and if a menu link is provided for this
   * menu item
   * (as a result of other properties), then the menu link is always expanded,
   * equivalent to its 'always expanded' checkbox being set in the UI.'
   *
   * @param bool $isExpanded
   *   Whether the menu item is expanded.
   *
   * @return IMenuItem
   *   This.
   */
  public function setExpanded($isExpanded);

}

// @codingStandardsIgnoreStart
//public function setPageCallback(callable $callback);
// http://www.php.net/manual/en/language.types.callable.php
// Difficult to combine with Drupal callback string approach.
// @codingStandardsIgnoreEnd
