<?php

/**
 * @file
 * IMenuItemSet.
 */

namespace Drupal\ooe\Menu;

/**
 * A set/group of menu items.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/1053 IMenuItemSet @endlink.
 *
 * @author darrenkelly
 */
interface IMenuItemSet {

  /**
   * Adds a child menu item to this.
   *
   * @param IMenuItem $menuItem
   *   A child menu item to add.
   *
   * @return IMenuItemSet
   *   This.
   */
  public function addMenuItem(IMenuItem $menuItem);

  /**
   * Adds an array of child menu item to this.
   *
   * @param IMenuItem[] $menuItems
   *   The child menu items to add.
   *
   * @return IMenuItemSet
   *   This.
   */
  public function addMenuItems(array $menuItems);

  /**
   * The child menu items of this.
   *
   * @return IMenuItem[]
   *   The child menu items of this.
   */
  public function getMenuItems();

  /**
   * A Drupal menu array with all menu items included.
   *
   * @return array
   *   A Drupal menu array with all menu items included,
   *   with the path of each menu item as array key.
   */
  public function get();

}
