<?php

/**
 * @file
 * MenuItemSet.
 */

namespace Drupal\ooe\Menu;

/**
 * Implements a set of menu items.
 *
 * Knows how to generate a Drupal menu array suitable for hook_menu().
 *
 * UML: @link http://drupal7demo.webel.com.au/node/1058 MenuItemSet @endlink
 *
 * @author darrenkelly
 */
class MenuItemSet implements IMenuItemSet {

  /**
   * The menu items.
   *
   * @var IMenuItem[]
   */
  private $menuItems = array();

  /**
   * Adds a child menu item.
   *
   * @param IMenuItem $menuItem
   *   The child menu item to add.
   *
   * @return IMenuItemSet
   *   This.
   */
  public function addMenuItem(IMenuItem $menuItem) {
    $this->menuItems[] = $menuItem;
    return $this;
  }

  /**
   * Adds an array of child menu items.
   *
   * @param IMenuItem[] $menuItems
   *   May be null or empty (will be ignored).
   *
   * @return IMenuItemSet
   *   This.
   */
  public function addMenuItems(array $menuItems) {
    if (!empty($menuItems)) {
      foreach ($menuItems as $mi) {
        if ($mi instanceof IMenuItem) {
          $this->addMenuItem($mi);
        }
        // @codingStandardsIgnoreStart
        //else // @todo warn.
        // @codingStandardsIgnoreEnd
      }
    }
    return $this;
  }

  /**
   * Gets this as a Drupal menu array.
   *
   * @return array
   *   Drupal menu array with all menu items included,
   *   with the path of each menu item as array key.
   */
  public function get() {
    $items = array();
    foreach ($this->menuItems as $mi) {
      $items[$mi->getPath()] = $mi->get();
    }
    return $items;
  }

  /**
   * The child menu items of this.
   *
   * @return IMenuItem[]
   *   The child menu items of this.
   */
  public function getMenuItems() {
    return $this->menuItems;
  }

}
