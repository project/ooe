<?php

/**
 * @file
 * DefaultMenuItem.
 */

namespace Drupal\ooe\Menu;

use Drupal\ooe\Module\DefaultModuleHelper;

// @codingStandardsIgnoreStart
// <img src="http://drupal7demo.webel.com.au/system/files/DefaultMenuItem.imp_.jpg"/>
// Webel: Feature Request: Would like to be able
// to include links to images in Class/Interface/Namespace docs
// https://drupal.org/node/2201079
// @codingStandardsIgnoreEnd

/**
 * Default implementation of a menu item (without a page or form controller).
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/1017 DefaultMenuItem @endlink.
 *
 * @author darrenkelly
 */
class DefaultMenuItem extends DefaultModuleHelper implements IMenuItem {

  /**
   * The title of the menu item.
   *
   * @var string
   */
  private $title;

  /**
   * Sets (or resets) the title of the menu item.
   *
   * @param string $title
   *   The title of the menu item.
   *
   * @return IMenuItem
   *   This.
   */
  final public function setTitle($title) {
    // @todo check is_string.
    $this->title = $title;
    return $this;
  }

  /*
  public function __construct($title) {
  $this->setTitle($title);
  } */

  /**
   * Constructor.
   *
   * @param string $module
   *   The module machine name. Required.
   * @param string $title
   *   The menu title. Required.
   * @param string $path
   *   The path portion of the menu item's URL. Required.
   */
  public function __construct($module, $title, $path) {
    parent::__construct($module);
    $this->setTitle($title);
    $this->setPath($path);
  }

  /**
   * The path portion of the menu item's URL.
   *
   * @var string
   */
  private $path;

  /**
   * The path portion of the menu item's URL.
   *
   * @return string
   *   The path portion of the menu item's URL.
   */
  final public function getPath() {
    return $this->path;
  }

  /**
   * Sets the menu path portion of the menu item's URL.
   *
   * Usually not needed, prefer inject via the constructor.
   *
   * @param string $path
   *   (Required).
   *
   * @return IMenuItem
   *   This.
   */
  final public function setPath($path) {
    // @todo check is present and is_string.
    $this->path = $path;
    return $this;
  }

  /**
   * From hook_menu(): 'The untranslated description of the menu item'.
   *
   * @var string
   */
  private $description;

  /**
   * Sets 'The untranslated description of the menu item'.
   *
   * @param string $description
   *   'The untranslated description of the menu item'.
   *
   * @return IMenuItem
   *   This.
   *
   * @see hook_menu()
   */
  final public function setDescription($description) {
    // @todo check is_string.
    $this->description = $description;
    return $this;
  }

  /**
   * An array of arguments to pass to the access callback function.
   *
   * From hook_menu():
   * 'An array of arguments to pass to the access callback function ...
   *  If the access callback is inherited ... ,
   * the access arguments will be inherited with it,
   * unless overridden in the child menu item.'
   *
   * @var string[]
   */
  protected $accessArguments;

  /**
   * Sets the access arguments.
   *
   * From hook_menu():
   * 'An array of arguments to pass to the access callback function ...
   *  If the access callback is inherited ... ,
   * the access arguments will be inherited with it,
   * unless overridden in the child menu item.'
   *
   * @param string[] $accessArguments
   *   An array of arguments to pass to the access callback function.
   *
   * @return IMenuItem
   *   This.
   */
  public function setAccessArguments(array $accessArguments) {
    // @todo checks.
    $this->accessArguments = $accessArguments;
    return $this;
  }

  /**
   * The access callback.
   *
   * The access callback can carry either a string representing
   * a callback function returning TRUE or FALSE, or a boolean TRUE/FALSE;
   * there are dedicated setters provided here for handling each case.
   *
   * The numeric option is not (yet) supported here, and mostly not needed
   * since will be cast to boolean anyway.
   *
   * From hook_menu():
   *
   * '"access callback": A function returning TRUE if the user has access
   * rights to this menu item, and FALSE if not.
   * It can also be a boolean constant instead of a function,
   * and you can also use numeric values (will be cast to boolean).
   * Defaults to user_access() unless a value is inherited from the
   * parent menu item; only MENU_DEFAULT_LOCAL_TASK items can inherit
   * access callbacks. To use the user_access() default callback,
   * you must specify the permission to check as 'access arguments''
   *
   * @var string|bool
   */
  protected $accessCallback;

  /**
   * Sets the access callback to a string representing a callback function.
   *
   * For forcing it to a boolean TRUE or FALSE please
   * use @link IMenuItem::forceAccessCallbackTRUE @endlink()
   * OR @link IMenuItem::forceAccessCallbackFALSE @endlink().
   *
   * Integer values are not supported (yet) and mostly not needed.
   *
   * @todo Check whether callback function or method exists.
   *
   * @param string $accessCallback
   *   A non-empty access callback string.
   *
   * @return IMenuItem
   *   This.
   */
  public function setAccessCallback($accessCallback) {
    if (empty($accessCallback) || !is_string($accessCallback)) {
      throw new \Exception('$accessCallback' . "($accessCallback)" . ' must be a non-null string representing a callback function !');
    }
    $this->accessCallback = $accessCallback;
    return $this;
  }

  /**
   * Forces the access callback to be TRUE.
   *
   * @see IMenuItem::forceAccessCallbackFALSE()
   *
   * @see IMenuItem::setAccessCallback(string)
   *
   * @return IMenuItem
   *   This.
   */
  final public function forceAccessCallbackTrue() {
    $this->accessCallback = TRUE;
    return $this;
  }

  /**
   * Forces the access callback to be FALSE.
   *
   * @see IMenuItem::forceAccessCallbackTRUE()
   *
   * @see IMenuItem::setAccessCallback(string)
   *
   * @return IMenuItem
   *   This.
   */
  final public function forceAccessCallbackFalse() {
    $this->accessCallback = FALSE;
    return $this;
  }

  /**
   * An array of arguments to pass to the page callback.
   *
   * From hook_menu():
   * '"page arguments": An array of arguments to pass to the page callback
   * function, with path component substitution ...'
   *
   * @var string[]
   */
  protected $pageArguments;
  // @codingStandardsIgnoreStart
  // NB: @var string[] not caught by PEAR PHP_UML !
  // @codingStandardsIgnoreEnd

  /**
   * Sets the page arguments.
   *
   * From hook_menu():
   * '"page arguments": An array of arguments to pass to the page callback
   * function, with path component substitution ...'
   *
   * @param string[] $pageArguments
   *   An array of page argument strings.
   *
   * @return IMenuItem
   *   This.
   */
  public function setPageArguments(array $pageArguments) {
    // @todo check that every array element is a string !
    $this->pageArguments = $pageArguments;
    return $this;
  }

  /**
   * The page callback function.
   *
   * From hook_menu():
   * '"page callback": The function to call to display a web page when
   * the user visits the path. If omitted, the parent menu item's callback
   * will be used instead'.
   *
   * @var string
   */
  protected $pageCallback;
  // @codingStandardsIgnoreStart
  // NOT private//permit force set in subclasses
  // public function setPageCallback(callable $pageCallback) {
  // http://www.php.net/manual/en/language.types.callable.php
  // Difficult to combine with Drupal callback string approach.
  // @codingStandardsIgnoreEnd

  /**
   * Sets the page callback.
   *
   * From hook_menu():
   * '"page callback": The function to call to display a web page when
   * the user visits the path. If omitted, the parent menu item's callback
   * will be used instead.'
   *
   * @param string $pageCallback
   *   A non-empty string representing the page callback.
   *
   * @throws \Exception
   *   If the $pageCallback is not a non-empty string.
   *
   * @return IMenuItem
   *   This.
   */
  public function setPageCallback($pageCallback) {
    if (empty($pageCallback) || !is_string($pageCallback)) {
      throw new \Exception('$pageCallback must be a non-null string representing a callback function !');
    }
    $this->pageCallback = $pageCallback;
    return $this;
  }

  /**
   * The bitmask of flags describing properties of the menu item.
   *
   * Specific setters are provided here for these.
   *
   * From hook_menu(): '"type":
   *  A bitmask of flags describing properties of the menu item.
   *
   * 'Many shortcut bitmasks are provided as constants in menu.inc:
   *
   * - MENU_NORMAL_ITEM: Normal menu items show up in the menu tree
   *   and can be moved/hidden by the administrator.
   * - MENU_CALLBACK: Callbacks simply register a path so that the
   *   correct information is generated when the path is accessed.
   * - MENU_SUGGESTED_ITEM: Modules may "suggest" menu items
   *   that the administrator may enable.
   * - MENU_LOCAL_ACTION: Local actions are menu items that describe
   *   actions on the parent item such as adding a new user or block,
   *   and are rendered in the action-links list in your theme.
   * - MENU_LOCAL_TASK: Local tasks are menu items that describe
   *   different displays of data, and are generally rendered as tabs.
   * - MENU_DEFAULT_LOCAL_TASK: Every set of local tasks should
   *   provide one "default" task, which should display the same page
   *   as the parent item.
   *
   * If the "type" element is omitted, MENU_NORMAL_ITEM is assumed.'
   *
   * Visit also
   * @link https://api.drupal.org/api/drupal/modules!system!system.api.php/function/hook_menu/7 Menu item types @endlink
   *
   * @var int
   */
  private $type = MENU_NORMAL_ITEM;

  /**
   * Sets the bitmask of flags describing properties of the menu item.
   *
   * Specific setters are provided here for all of these.
   *
   * From hook_menu():
   * '"type": A bitmask of flags describing properties of the menu item.'
   *
   * 'Many shortcut bitmasks are provided as constants in menu.inc:
   *
   * - MENU_NORMAL_ITEM: Normal menu items show up in the menu tree
   *   and can be moved/hidden by the administrator.
   *
   * - MENU_CALLBACK: Callbacks simply register a path so that the
   *   correct information is generated when the path is accessed.
   *
   * - MENU_SUGGESTED_ITEM: Modules may "suggest" menu items
   *   that the administrator may enable.
   *
   * - MENU_LOCAL_ACTION: Local actions are menu items that describe
   *   actions on the parent item such as adding a new user or block,
   *   and are rendered in the action-links list in your theme.
   *
   * - MENU_LOCAL_TASK: Local tasks are menu items that describe
   *   different displays of data, and are generally rendered as tabs.
   *
   * - MENU_DEFAULT_LOCAL_TASK: Every set of local tasks should
   *   provide one "default" task, which should display the same page
   *   as the parent item.
   *
   * If the "type" element is omitted, MENU_NORMAL_ITEM is assumed.'
   *
   * Visit also
   * @link https://api.drupal.org/api/drupal/modules!system!system.api.php/function/hook_menu/7 Menu item types @endlink.
   *
   * @todo Check against valid Drupal menu item bitmask combinations.
   *
   * @param int $typeBitmask
   *   A non empty type bitmask that is a valid
   *   combination of Drupal menu bitmask types.
   *
   * @return IMenuItem
   *   This.
   */
  final public function setType($typeBitmask = MENU_NORMAL_ITEM) {
    if (empty($typeBitmask) || !is_int($typeBitmask)) {
      throw new \Exception('The $typeBitmask' . "($typeBitmask)" . ' is not a valid bitmask combination');
    }
    $this->type = $typeBitmask;
    return $this;
  }

  /**
   * Sets the menu type to be a MENU_DEFAULT_LOCAL_TASK (if TRUE).
   *
   * If TRUE, sets the menu type to be a MENU_DEFAULT_LOCAL_TASK
   * the first (default) of a set of menu tabs,
   * or reverts to a MENU_NORMAL_ITEM if FALSE.
   *
   * From hook_menu(): 'MENU_DEFAULT_LOCAL_TASK: Every set of local tasks
   * should provide one "default" task, which should display the same
   * page as the parent item'.
   *
   * @param bool $isTypeDefaultLocalTask
   *   Whether the type should be set to MENU_DEFAULT_LOCAL_TASK.
   *
   * @return IMenuItem
   *   This.
   */
  final public function setTypeDefaultLocalTask($isTypeDefaultLocalTask = TRUE) {
    if (!is_bool($isTypeDefaultLocalTask)) {
      throw new \Exception('$isTypeDefaultLocalTask must be a bool !');
    }
    if ($isTypeDefaultLocalTask) {
      $this->type = MENU_DEFAULT_LOCAL_TASK;
    }
    else {
      $this->type = MENU_NORMAL_ITEM;
    }
    return $this;
  }

  /**
   * Sets the menu type to be a MENU_LOCAL_TASK (if TRUE).
   *
   * If TRUE, sets the menu type to be a MENU_LOCAL_TASK,
   * one of a set of menu tabs,
   * or reverts to a MENU_NORMAL_ITEM if FALSE.
   *
   * From hook_menu():
   * 'MENU_LOCAL_TASK: Local tasks are menu items that describe
   * different displays of data, and are generally rendered as tabs.'
   *
   * @param bool $isTypeLocalTask
   *   Whether the type should be set to MENU_LOCAL_TASK.
   *
   * @return IMenuItem
   *   This.
   */
  final public function setTypeLocalTask($isTypeLocalTask = TRUE) {
    if (!is_bool($isTypeLocalTask)) {
      throw new \Exception('$isTypeLocalTask must be a bool !');
    }
    if ($isTypeLocalTask) {
      $this->type = MENU_LOCAL_TASK;
    }
    else {
      $this->type = MENU_NORMAL_ITEM;
    }
    return $this;
  }

  /**
   * Sets the menu type to be a MENU_LOCAL_ACTION (if TRUE).
   *
   * If TRUE, sets the menu type to be a MENU_LOCAL_ACTION,
   * one of a set of menu tabs,
   * or reverts to a MENU_NORMAL_ITEM if FALSE.
   *
   * From hook_menu():
   * 'MENU_LOCAL_ACTION: Local actions are menu items that describe
   * actions on the parent item such as adding a new user or block,
   * and are rendered in the action-links list in your theme.'
   *
   * @param bool $isTypeLocalAction
   *   Whether the type should be set to MENU_LOCAL_ACTION.
   *
   * @return IMenuItem
   *   This.
   */
  final public function setTypeLocalAction($isTypeLocalAction = TRUE) {
    if (!is_bool($isTypeLocalAction)) {
      throw new \Exception('$isTypeLocalAction must be a bool !');
    }
    if ($isTypeLocalAction) {
      $this->type = MENU_LOCAL_ACTION;
    }
    else {
      $this->type = MENU_NORMAL_ITEM;
    }
    return $this;
  }

  /**
   * Sets the menu type to be a MENU_SUGGESTED_ITEM (if TRUE).
   *
   * If TRUE, sets the menu type to be a MENU_SUGGESTED_ITEM,
   * or reverts to a MENU_NORMAL_ITEM if FALSE.
   *
   * From hook_menu(): 'MENU_SUGGESTED_ITEM: Modules may "suggest"
   * menu items that the administrator may enable.'
   *
   * From MENU_SUGGESTED_ITEM:
   * 'They act just as callbacks do until enabled,
   * at which time they act like normal items.'
   *
   * @param bool $isTypeSuggestedItem
   *   Whether the type should be set to MENU_SUGGESTED_ITEM.
   *
   * @return IMenuItem
   *   This.
   */
  final public function setTypeSuggestedItem($isTypeSuggestedItem = TRUE) {
    if (!is_bool($isTypeSuggestedItem)) {
      throw new \Exception('$isTypeSuggestedItem must be a bool !');
    }
    if ($isTypeSuggestedItem) {
      $this->type = MENU_SUGGESTED_ITEM;
    }
    else {
      $this->type = MENU_NORMAL_ITEM;
    }
    return $this;
  }

  /**
   * Sets the menu type to be a MENU_NORMAL_ITEM menu item (if TRUE).
   *
   * If TRUE, sets the menu type to be a MENU_NORMAL_ITEM menu item;
   * Ignored if FALSE.
   *
   * From hook_menu():
   * 'MENU_NORMAL_ITEM: Normal menu items show up in the menu tree
   * and can be moved/hidden by the administrator.'
   *
   * @param bool $isTypeNormal
   *   Whether the type should be set to MENU_NORMAL_ITEM.
   *
   * @return IMenuItem
   *   This.
   */
  final public function setTypeNormal($isTypeNormal = TRUE) {
    if (!is_bool($isTypeNormal)) {
      throw new \Exception('$isTypeNormal must be a bool !');
    }
    if ($isTypeNormal) {
      $this->type = MENU_NORMAL_ITEM;
    }
    // @codingStandardsIgnoreStart
    //else $this->type = MENU_NORMAL_ITEM; //IGNORE
    // @codingStandardsIgnoreEnd
    return $this;
  }

  /**
   * Sets the menu type to be a MENU_CALLBACK menu item (if TRUE).
   *
   * If TRUE, sets the menu type to be a MENU_CALLBACK menu item;
   * Ignored if FALSE.
   *
   * From hook_menu(): 'MENU_CALLBACK: Callbacks simply
   * register a path so that the
   * correct information is generated when the path is accessed.'
   *
   * @param bool $isTypeCallback
   *   Whether the type should be set to MENU_CALLBACK.
   *
   * @return IMenuItem
   *   This.
   */
  final public function setTypeCallback($isTypeCallback = TRUE) {
    if (!is_bool($isTypeCallback)) {
      throw new \Exception('$isTypeCallback must be a bool !');
    }
    if ($isTypeCallback) {
      $this->type = MENU_CALLBACK;
    }
    // @codingStandardsIgnoreStart
    //else $this->type = MENU_CALLBACK; //IGNORE
    // @codingStandardsIgnoreEnd
    return $this;
  }

  /**
   * Builds and returns a valid Drupal menu item array for hook_menu().
   *
   * @return array
   *   A valid Drupal menu item array for hook_menu().
   */
  public function get() {
    $get = array(
      'title' => $this->title,
      'page callback' => $this->pageCallback,
      'type' => $this->type,
    );
    if (!empty($this->accessArguments)) {
      $get['access arguments'] = $this->accessArguments;
    }
    if (!empty($this->accessCallback)) {
      $get['access callback'] = $this->accessCallback;
    }
    if (!empty($this->weight)) {
      $get['weight'] = $this->weight;
    }
    if (!empty($this->isExpanded)) {
      $get['expanded'] = $this->isExpanded;
    }
    if (!empty($this->menuName)) {
      $get['menu_name'] = $this->menuName;
    }
    if (!empty($this->description)) {
      $get['description'] = $this->description;
    }

    $pageArgsTmp = $this->getPageArgumentsDecorated();
    if (!empty($pageArgsTmp)) {
      $get['page arguments'] = $pageArgsTmp;
    }
    return $get;
  }

  /**
   * By default just the pageArguments as set, if any.
   *
   * Can be overridden for special cases to insert additional args
   * (such as needed for page controller menu items).
   *
   * @return array
   *   The page arguments (possibly decorated).
   */
  protected function getPageArgumentsDecorated() {
    return $this->pageArguments;
  }

  /**
   * An integer that determines the relative position of items in the menu.
   *
   * From hook_menu():
   * '"weight": An integer that determines the relative position of items
   *  in the menu; higher-weighted items sink. Defaults to 0. Menu items
   * with the same weight are ordered alphabetically.'
   *
   * @var int
   */
  private $weight;

  /**
   * Sets the menu item weight.
   *
   * From hook_menu();
   * '"weight": An integer that determines the relative position of items
   * in the menu; higher-weighted items sink. Defaults to 0.
   * Menu items with the same weight are ordered alphabetically.'
   *
   * @param int $weight
   *   An integer that determines the relative position of items in the menu.
   *
   * @return IMenuItem
   *   This.
   */
  final public function setWeight($weight) {
    if (!is_int($weight)) {
      throw new \Exception('$weight must be an integer !');
    }
    $this->weight = $weight;
    return $this;
  }

  /**
   * The name of a custom menu this should appear in.
   *
   * Only required to be set if other than the Navigation menu.
   *
   * From hook_menu():
   * 'Optional. Set this to a custom menu if you
   *  don't want your item to be placed in Navigation.'   *
   *
   * @var string
   */
  private $menuName;

  /**
   * The machine name of the menu for this.
   *
   * From hook_menu(): 'Optional. Set this to a custom menu
   * if you don't want your item to be placed in Navigation.'
   *
   * CAUTION: this is only safe to use if the
   * name of an already created menu is known !
   *
   * @todo Query database to check whether this is a valid, existing menu name.
   *
   * @param string $menuName
   *   The name of a custom menu (other than Navigation) this should appear in.
   *
   * @return IMenuItem
   *   This.
   */
  final public function setMenuName($menuName) {
    if (empty($menuName) || !is_string($menuName)) {
      throw new \Exception('$menuName must be a non empty string !');
    }
    $this->menuName = $menuName;
    return $this;
  }

  /**
   * Whether the menu item is expanded.
   *
   * From hook_menu():
   * 'Optional. If set to TRUE, and if a menu link is provided for this menu
   * item
   * (as a result of other properties), then the menu link is always expanded,
   * equivalent to its 'always expanded' checkbox being set in the UI.'
   *
   * @var bool
   */
  private $isExpanded;

  /**
   * Sets whether the menu item is expanded.
   *
   * From hook_menu():
   * 'Optional. If set to TRUE, and if a menu link
   * is provided for this menu item
   * (as a result of other properties), then the menu link is always expanded,
   * equivalent to its 'always expanded' checkbox being set in the UI.'
   *
   * @param bool $isExpanded
   *   Whether the menu item is expanded.
   *
   * @return IMenuItem
   *   This.
   */
  final public function setExpanded($isExpanded) {
    if (!is_bool($isExpanded)) {
      throw new \Exception('$isExpanded must be a bool !');
    }
    $this->isExpanded = $isExpanded;
    return $this;
  }

}
