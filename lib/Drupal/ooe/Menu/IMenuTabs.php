<?php

/**
 * @file
 * IMenuTabs.
 */

namespace Drupal\ooe\Menu;

use Drupal\ooe\Page\IPageController;

use Drupal\ooe\Module\IModuleHelper;

/**
 * Manages a set of menu tabs.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/1015 IMenuTabs @endlink.
 *
 * @author darrenkelly
 */
interface IMenuTabs extends IModuleHelper {

  /**
   * The menu items handled by this.
   *
   * @return IMenuItem[]
   *   The array of menu items handled by this.
   */
  public function getMenuItems();

  /**
   * Creates a new IPageMenuItem set to act as a tab.
   *
   * It will be configured with the given page controller.
   *
   * @param IPageController $pageController
   *   Page controller for the page triggered by the tab.
   * @param string $title
   *   Title of the tab.
   *
   * @return \Drupal\ooe\Page\IPageMenuItem
   *   The new tab menu item.
   */
  public function newTabMenuItem(IPageController $pageController, $title);

  /**
   * Sets the weight of the main (parent) menu item.
   *
   * @param int $weight
   *   The weight of the main (parent) menu item of this.
   *
   * @return IMenuTabs
   *   This.
   */
  public function setWeight($weight);

}
