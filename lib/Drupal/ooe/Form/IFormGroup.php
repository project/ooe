<?php

/**
 * @file
 * IFormGroup.
 */

namespace Drupal\ooe\Form;

/**
 * A group of form fields.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2311 IFormGroup @endlink.
 *
 * @author darrenkelly
 */
interface IFormGroup extends IFormField {

  /**
   * Adds a child field to this group.
   *
   * @param string $key
   *   The array key for the child field.
   * @param IFormField $field
   *   The child field.
   *
   * @return IFormGroup
   *   This.
   */
  public function add($key, IFormField $field);

}
