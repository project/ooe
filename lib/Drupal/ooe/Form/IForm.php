<?php

/**
 * @file
 * IForm.
 */

namespace Drupal\ooe\Form;

/**
 * Encapsulation of a Drupal form.
 *
 * Note that a form is also a form group.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2313 IForm @endlink
 *
 * @author darrenkelly
 */
interface IForm extends IFormGroup {

  /**
   * Sets the submit handler for this.
   *
   * @param \Drupal\ooe\Form\IFormSubmit $submit
   *   The submit item.
   * @param string $key
   *   An array key for the submit item.
   *
   * @return IForm
   *   This.
   */
  function setSubmit(IFormSubmit $submit, $key);

}
