<?php

/**
 * @file
 * FormFieldset.
 */

namespace Drupal\ooe\Form;

/**
 * Implementation of a form fieldset.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2315 FormFieldset @endlink.
 *
 * @author darrenkelly
 */
class FormFieldset extends AbstractFormGroup implements IFormFieldset {

  /**
   * Constructor.
   *
   * @param string $title
   *   The (translated) title of the form fieldset.
   */
  public function __construct($title) {
    parent::__construct('fieldset', $title);
  }

  /**
   * Whether the fieldset should start collapsed.
   *
   * @var bool
   */
  private $collapsed;

  /**
   * Set whether the fieldset should start collapsed.
   *
   * @param bool $collapsed
   *   Whether the fieldset should start collapsed.
   *
   * @return IFormFieldset
   *   This.
   */
  public function setCollapsed($collapsed) {
    $this->collapsed = $collapsed;
    return $this;
  }

  /**
   * Whether the fieldset should be collapsible.
   *
   * @var bool
   */
  private $collapsible;

  /**
   * Set whether the fieldset should be collapsible.
   *
   * @param bool $collapsible
   *   Whether the fieldset should be collapsible.
   *
   * @return IFormFieldset
   *   This.
   */
  public function setCollapsible($collapsible) {
    $this->collapsible = $collapsible;
    return $this;
  }

  /**
   * Gets a Drupal form array portion for a form fieldset.
   *
   * @return array
   *   A Drupal form array portion for a form fieldset.
   */
  public function get() {

    $out = parent::get();

    if (!empty($this->collapsed)) {
      $out['#collapsed'] = $this->collapsed;
    }

    if (!empty($this->collapsible)) {
      $out['#collapsible'] = $this->collapsible;
    }

    return $out;
  }

}
