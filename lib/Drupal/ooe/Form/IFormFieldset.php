<?php

/**
 * @file
 * IFormFieldset.
 */

namespace Drupal\ooe\Form;

/**
 * A form fieldset.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2316 IFormFieldset @endlink.
 *
 * @author darrenkelly
 */
interface IFormFieldset extends IFormGroup {

  /**
   * Sets whether the fieldset should start collapsed.
   *
   * @param bool $collapsed
   *   Whether the fieldset should start collapsed.
   *
   * @return IFormFieldset
   *   This.
   */
  public function setCollapsed($collapsed);

  /**
   * Sets whether the fieldset should be collapsible.
   *
   * @param bool $collapsible
   *   Whether the fieldset should be collapsible.
   *
   * @return IFormFieldset
   *   This.
   */
  public function setCollapsible($collapsible);

}
