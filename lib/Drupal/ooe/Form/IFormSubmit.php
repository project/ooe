<?php

/**
 * @file
 * IFormSubmit.
 */

namespace Drupal\ooe\Form;

/**
 * A submit button form element.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2321 IFormSubmit @endlink.
 *
 * @author darrenkelly
 */
interface IFormSubmit extends IFormValueField {

}
