<?php

/**
 * @file
 * AbstractFormController.
 */

namespace Drupal\ooe\Form;

use Drupal\ooe\Module\DefaultModuleHelper;
use Drupal\ooe\Common;

/**
 * Adapted from Chris Skene's page_controller (@link PageController @endlink).
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/2328 AbstractFormController @endlink
 *
 * @author darrenkelly
 */
abstract class AbstractFormController extends DefaultModuleHelper implements IFormController {

  /**
   * Name of the builder callback method.
   *
   * @var string
   */
  const BUILDER = 'build';

  /**
   * The name of the controller class.
   *
   * @var string
   */
  // @codingStandardsIgnoreStart
  // static private $controller_class_name;
  // @codingStandardsIgnoreEnd

  /**
   * Form builder delegator.
   *
   * @param array $form
   *   A Drupal form array.
   * @param array $form_state
   *   A Drupal form state array as a reference.
   * @param string $arg_controller_class_name
   *   The controller class name.
   *
   * @return array
   *   A Drupal form array.
   */
  static public function createForm(array $form, array &$form_state, $arg_controller_class_name) {

    // $controller_class_name = is_array($vargs) ? array_shift($vargs):$vargs;
    $controller_class_name = $arg_controller_class_name;
    // @todo Handle when called with $vargs array,
    // not just single controller class name string.

    $method = self::BUILDER;

    // $args = is_array($vargs) ? $vargs : array($vargs);
    // $args = array($form, &$form_state,$vargs);
    $args = array($form, &$form_state);
    // @todo extend ?

    if (class_exists($controller_class_name)) {
      $controller = new $controller_class_name(Common::MODULE);
      return call_user_func_array(array($controller, $method), $args);

      // ISSUE: Notice: Undefined index:
      // \Drupal\ooe\Form\AbstractFormController::createForm
      // in drupal_retrieve_form()
      // (line 764 of /home/webel02/public_html/drupal7/includes/form.inc).
      //
      // Cause is clear:
      //
      // drupal_retrieve_form(): 764:  if (!function_exists($form_id)) {
      //
      //  http://au1.php.net/function_exists
      //  http://au1.php.net/manual/en/function.method-exists.php
      //
      // php > class Test { static public function foo($arg) {echo $arg;} }
      // php > echo function_exists('Test::foo');
      // php > if (function_exists('Test::foo')) echo "exists";
      // php > if (method_exists('Test','foo')) echo "exists";
      // exists
    }
    throw new \Exception('Invalid FormController');
  }

  /**
   * Constructor.
   *
   * @param string $module
   *   The module machine name.
   */
  public function __construct($module) {
    // IMPORTANT: Access level to
    // Drupal\ooe\Form\AbstractFormController::__construct()
    // must be public (as in class Drupal\ooe\Module\DefaultModuleHelper)
    parent::__construct($module);
  }

  /**
   * Utility method for accessing a named value from the form state.
   *
   * @param array $form_state
   *   The Drupal form state array.
   * @param string $name
   *   The named value to fetch.
   *
   * @return string
   *   The matching value.
   */
  static protected function getFormValue(array $form_state, $name) {
    return $form_state['values'][$name];
    // @return string|array// @todo?
  }

  /**
   * Add a validation handler to a form.
   *
   * Should be called in the form builder implementation of a subclass.
   *
   * @param array $form
   *   A Drupal form array.
   * @param string $validatorMethodName
   *   The name of a _static_ validator method (only) with signature
   *   ($form, &$form_state); DO NOT include the class name.
   *
   * @throws \Exception
   *   If the validator method name is not a non-empty string.
   */
  protected function addValidateHandler(array &$form, $validatorMethodName) {
    // dpm($validatorMethodName,
    // 'AbstractFormController::addValidateHandler:$validatorMethodName');
    // DEBUG
    if (empty($validatorMethodName) || !is_string($validatorMethodName)) {
      throw new \Exception('$validatorMethodName must be a non empty string !');
    }
    $class = get_class($this);
    if (!method_exists($class, $validatorMethodName)) {
      throw new \Exception('$validatorMethodName(' . $validatorMethodName . ') is not a valid method of class(' . $class . ') !');
    }
    $form['#validate'][] = "\\$class::$validatorMethodName";
    // Above safe: if $form['#validate'] does not exist yet it will be created.
  }

  /**
   * Add a submit handler to a form.
   *
   * Should be called in the form builder implementation of a subclass.
   *
   * @param array $form
   *   A Drupal form array.
   * @param string $submitMethodName
   *   The name of a _static_ validator method (only) with
   *   signature ($form, &$form_state); DO NOT include the class name.
   *
   * @throws \Exception
   *   If the submit method name is not a valid method name.
   */
  protected function addSubmitHandler(array &$form, $submitMethodName) {
    if (empty($submitMethodName) || !is_string($submitMethodName)) {
      throw new \Exception('$submitMethodName must be a non empty string !');
    }
    $class = get_class($this);
    if (!method_exists($class, $submitMethodName)) {
      throw new \Exception('$submitMethodName(' . $submitMethodName . ') is not a valid method of class(' . $class . ') !');
    }
    $form['#submit'][] = "\\$class::$submitMethodName";
    // Above is safe: if $form['#submit'] does not exist yet it will be created.
  }

}

// @codingStandardsIgnoreStart
/*
  static protected function newThis() {
    //http://stackoverflow.com/questions/5197300/new-self-vs-new-static
    //What is the difference between new self and new static?
    //self refers to the same class whose method
    // the new operation takes place in.
    //static in PHP 5.3's late static bindings refers to
    // whatever class in the hierarchy which you call the method on.
    return new static();//PHP5.3
    //FAILS: Fatal error: Cannot instantiate abstract class
    // Drupal\ooe\Form\AbstractFormController
    //Because by definition of the fixed 'page arguments'
    //it is calling on an AbstractFormController !
  }
 */
// @codingStandardsIgnoreEnd