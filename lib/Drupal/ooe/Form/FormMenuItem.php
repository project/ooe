<?php

/**
 * @file
 * FormMenuItem.
 */

namespace Drupal\ooe\Form;

use Drupal\ooe\Menu\DefaultMenuItem;

/**
 * A menu item for a form.
 *
 * Assumes the page callback for the menu item
 * will leverage drupal_get_form($form id):
 *
 * This will process the 1st argument of the
 * page arguments as the form builder function.
 *
 * No assumptions are made about the access arguments,
 * access callback, or the name of the form builder function.
 *
 * This is therefore NOT necessarily object-oriented controlled,
 * and it should only ever (if at all) be used directly to demonstrate
 * old-style Drupal forms. In other words, PLEASE DO NOT USE THIS CLASS
 * except for extension by a controlled menu item implementation.
 *
 * In the Webel OOE world, forms always use an @link IFormController @endlink
 * and
 * should always be used together with a @link IFormControllerMenuItem @endlink.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2025 FormMenuItem @endlink
 *
 * @author darrenkelly
 */
class FormMenuItem extends DefaultMenuItem implements IFormMenuItem {

  /**
   * Fixed at drupal_get_form().
   */
  const PAGE_CALLBACK = 'drupal_get_form';

  /**
   * Constructor: forces the page callback to be drupal_get_form().
   *
   * @param string $module
   *   The module machine name.
   * @param string $title
   *   The title.
   * @param string $path
   *   The path.
   */
  public function __construct($module, $title, $path) {
    parent::__construct($module, $title, $path);
    $this->pageCallback = self::PAGE_CALLBACK;
  }

  /**
   * Overridden to always throw an exception.
   *
   * Prevents override of the fixed page callback.
   *
   * Nothing returned.
   *
   * @param string $pageCallback
   *   An attempted page callback function name to set (will be ignored).
   *
   * @throws \Exception
   *   Always thrown.
   */
  final public function setPageCallback($pageCallback) {
    throw new \Exception("IGNORED: page callback is forced to '" . self::PAGE_CALLBACK . "' !");
  }

}
