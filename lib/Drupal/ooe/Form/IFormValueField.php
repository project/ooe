<?php

/**
 * @file
 * IFormValueField.
 */

namespace Drupal\ooe\Form;

/**
 * A form field that carries a form value (represented by a string).
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/2317 IFormValueField @endlink.
 *
 * @author darrenkelly
 */
interface IFormValueField extends IFormField {

  /**
   * Sets the default value.
   *
   * @param string $defaultValue
   *   The default value.
   *
   * @return IFormValueField
   *   This.
   */
  public function setDefaultValue($defaultValue);

  /**
   * Sets whether the value is required (is not optional).
   *
   * @param bool $required
   *   Whether the value is required (is not optional).
   *
   * @return IFormValueField
   *   This.
   */
  public function setRequired($required);

  /**
   * Set the string value representation of this.
   *
   * @param string $value
   *   The string value representation of this.
   *
   * @return IFormValueField
   *   this.
   */
  public function setValue($value);

}
