<?php

/**
 * @file
 * FormTextField.
 */

namespace Drupal\ooe\Form;

/**
 * Implements a form field that handles a text value.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2320 FormTextField @endlink.
 *
 * @author darrenkelly
 */
class FormTextField extends FormValueField implements IFormTextField {

  /**
   * The maximum length of the text field.
   *
   * @var int
   */
  private $maxlength;

  /**
   * Sets the maximum length of the text field.
   *
   * @param int $maxlength
   *   The maximum length of the text field.
   *
   * @return IFormTextField
   *   This.
   */
  public function setMaxlength($maxlength) {
    $this->maxlength = $maxlength;
    return $this;
  }

  /**
   * The size of the text field.
   *
   * @var int
   */
  private $size;

  /**
   * Sets the size of the text field.
   *
   * @param int $size
   *   The size of the text field.
   *
   * @return IFormTextField
   *   This.
   */
  public function setSize($size) {
    $this->size = $size;
    return $this;
  }

  /**
   * Constructor.
   *
   * @param string $title
   *   The (translated) title of the text field.
   * @param bool $required
   *   Whether the value is required.
   */
  public function __construct($title, $required) {
    parent::__construct('textfield', $title, $required);
    $this->required = $required;
  }

  /**
   * A Drupal form array portion.
   *
   * @return array
   *   A Drupal form array portion.
   */
  public function get() {

    $out = parent::get();

    if (!empty($this->size)) {
      $out['#size'] = $this->size;
    }

    if (!empty($this->maxlength)) {
      $out['#maxlength'] = $this->maxlength;
    }

    return $out;
  }

}
