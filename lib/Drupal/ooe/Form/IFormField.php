<?php

/**
 * @file
 * IFormField.
 */

namespace Drupal\ooe\Form;

/**
 * A form field.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2307 IFormField @endlink.
 *
 * @author darrenkelly
 */
interface IFormField {
  // Could have chosen IField, but don't want to clash with Field API.

  /**
   * A Drupal form array portion.
   *
   * @return array
   *   A Drupal form array portion.
   */
  public function get();

  /**
   * Sets the type of this.
   *
   * @param string $type
   *   The type of this.
   *
   * @return IFormField
   *   This.
   */
  public function setType($type);

  /**
   * Sets the title of this.
   *
   * @param string $title
   *   The (translated) title of this.
   *
   * @return IFormField
   *   This.
   */
  public function setTitle($title);

  /**
   * Sets the description of this.
   *
   * @param string $description
   *   The description of this.
   *
   * @return IFormField
   *   This.
   */
  public function setDescription($description);

}
