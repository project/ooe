<?php

/**
 * @file
 * FormValueField.
 */

namespace Drupal\ooe\Form;

/**
 * Implements a form field that carries a form value (represented by a string).
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2318 FormValueField @endlink.
 *
 * @todo Consider renaming to AbstractFormValueField.
 *
 * @author darrenkelly
 */
abstract class FormValueField extends AbstractFormField implements IFormValueField {

  /**
   * The value (as represented by a string).
   *
   * @var string
   */
  private $value;

  /**
   * Set the string value representation of this.
   *
   * @param string $value
   *   The string value representation of this.
   *
   * @return IFormValueField
   *   This.
   */
  public function setValue($value) {
    $this->value = $value;
    return $this;
  }

  /**
   * The default value (as represented by a string).
   *
   * @var string
   */
  private $defaultValue;

  /**
   * Sets the default value (as represented by a string).
   *
   * @param string $defaultValue
   *   The default value (as represented by a string).
   *
   * @return IFormValueField
   *   This.
   */
  public function setDefaultValue($defaultValue) {
    // @todo here or in intermediate class ?
    $this->defaultValue = $defaultValue;
    return $this;
  }

  /**
   * Whether the value is required (is not optional).
   *
   * @var bool
   */
  private $required;

  /**
   * Sets whether the value is required (is not optional).
   *
   * @param bool $required
   *   Whether the value is required (is not optional).
   *
   * @return IFormValueField
   *   This.
   */
  public function setRequired($required) {
    $this->required = $required;
    return $this;
  }

  /**
   * Constructor.
   *
   * @param string $type
   *   The type of the form field.
   * @param string $title
   *   The (translated) title of the form field.
   * @param bool $required
   *   Whether the value is required (is not optional). Default is FALSE.
   */
  protected function __construct($type, $title, $required = FALSE) {
    parent::__construct($type, $title);
    $this->required = $required;
  }

  /**
   * A Drupal form array portion.
   *
   * @return array
   *   A Drupal form array portion.
   */
  public function get() {

    $out = parent::get();
    // Decorator: NOT a CallSuper code smell !

    if (!empty($this->value)) {
      $out['#value'] = $this->value;
    }

    if (!empty($this->required)) {
      $out['#required'] = $this->required;
    }

    if (!empty($this->defaultValue)) {
      $out['#default_value'] = $this->defaultValue;
    }

    return $out;
  }

}
