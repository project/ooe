<?php

/**
 * @file
 * Form.
 */

namespace Drupal\ooe\Form;

/**
 * Implements a Drupal form.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2314 Form @endlink.
 *
 * @author darrenkelly
 */
class Form extends AbstractFormGroup implements IForm {

  /**
   * A Drupal form array.
   *
   * @var array
   */
  private $form;

  /**
   * Constructor.
   *
   * @param array $form
   *   A Drupal form array.
   * @param string $title
   *   The (translated) title of the form.
   * @param string $description
   *   The (translated) description of the form.
   *
   * @see http://www.php.net/manual/en/language.references.php
   */
  public function __construct(array &$form, $title, $description = NULL) {
    // @todo checks.
    parent::__construct(
        NULL, // @todo type?
        NULL // DO NOT pass $description here, display via description item.
    );

    // Modifications to the Form performed externally on references to it
    // will be merged into the parent::get() in get(), so it is crucial
    // that the private variable uses dereferencing.
    // http://www.php.net/manual/en/language.references.php
    $this->form = &$form;

    $formDescription = new FormItem($title);
    $formDescription->setDescription($description);

    $this->add(
        'description',
        // NB: arbitrary child, not a #description key !
        $formDescription);
  }

  /**
   * Keep an associative reference to the form submit.
   *
   * @var IFormSubmit
   */
  private $submit;

  /**
   * Set the submit handler for this.
   *
   * @todo Check only one handler ?
   *
   * @param \Drupal\ooe\Form\IFormSubmit $submit
   *   A submit field.
   * @param string $key
   *   An optional array key for the submit field.
   *
   * @return IForm
   *   This.
   */
  public function setSubmit(IFormSubmit $submit, $key = 'submit') {
    if (empty($submit)) {
      throw new \Exception('Need a non empty IFormSubmit $submit !');
    }
    $this->submit = $submit;
    if (empty($key)) {
      throw new \Exception('Need a non empty $key for the IFormSubmit !');
    }
    $this->add($key, $submit);
    return $this;
  }

  /**
   * Get this as a Drupal form array.
   *
   * @todo Check whether an IFormSubmit has been set.
   *
   * @return array
   *   A Drupal form array.
   */
  public function get() {
    // dpm($this->form,'Form:get():$form');//DEBUG
    return array_merge($this->form, parent::get());
  }

  /* @todo
  public function setDescription($description) {
  throw new \Exception(
  'Do not use a description string, reserved for a description IFormItem !'
  );
  }
   */

}
