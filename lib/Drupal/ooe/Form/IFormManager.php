<?php

/**
 * @file
 * IFormManager.
 */

namespace Drupal\ooe\Form;

/**
 * Manages validation and submission of a form.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/1658 IFormManager @endlink.
 *
 * @author darrenkelly
 */
interface IFormManager {

  /**
   * Gets a Drupal form.
   *
   * @param array $form
   *   A Drupal form array.
   * @param array $form_submit
   *   A Drupal form submit array.
   *
   * @return array
   *   A Drupal form array.
   */
  public function build(array $form, array &$form_submit);

  /**
   * Validates a form.
   *
   * @param array $form
   *   A Drupal form array.
   * @param array $form_state
   *   A Drupal form state array.
   */
  public function validate(array $form, array &$form_state);

  /**
   * Submits a form.
   *
   * @param array $form
   *   A Drupal form array.
   * @param array $form_state
   *   A Drupal form state array.
   */
  public function submit(array $form, array &$form_state);

}
