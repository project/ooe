<?php

/**
 * @file
 * FormItem.
 */

namespace Drupal\ooe\Form;

/**
 * An item of a form.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2310 FormItem @endlink.
 *
 * @author darrenkelly
 */
class FormItem extends AbstractFormField implements IFormItem {

  /**
   * Constructor.
   *
   * @param string $title
   *   The (translated) form item title.
   */
  public function __construct($title) {
    parent::__construct('item', $title);
  }

}
