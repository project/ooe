<?php

/**
 * @file
 * IFormItem.
 */

namespace Drupal\ooe\Form;

/**
 * An item of a form.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2309 IFormItem @endlink.
 *
 * @author darrenkelly
 */
interface IFormItem extends IFormField {

}
