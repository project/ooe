<?php

/**
 * @file
 * IFormController.
 */

namespace Drupal\ooe\Form;

/**
 * A controller for a form.
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/1663 IFormController @endlink.
 *
 * @author darrenkelly
 */
interface IFormController {

  /**
   * Build a Drupal form array.
   *
   * Visit also: drupal_build_form($form_id, &$form_state).
   *
   * @param array $form
   *   A Drupal form array.
   * @param array $form_submit
   *   A Drupal form submit array by reference.
   *
   * @return array
   *   A Drupal form array.
   */
  public function build(array $form, array &$form_submit);

}
