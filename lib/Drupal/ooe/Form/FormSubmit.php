<?php

/**
 * @file
 * FormSubmit.
 */

namespace Drupal\ooe\Form;

/**
 * Implements a form submit field.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2322 FormSubmit @endlink.
 *
 * @author darrenkelly
 */
class FormSubmit extends FormValueField implements IFormSubmit {

  /**
   * Constructor.
   *
   * @param string $value
   *   The string representation of the value.
   */
  public function __construct($value) {
    // @todo checks.
    $title = NULL;
    $required = TRUE;
    parent::__construct(
        'submit',
        $title,
        $required
    );
    $this->setValue($value);
  }

  /**
   * A Drupal form array portion.
   *
   * @return array
   *   A Drupal form array portion.
   */
  public function get() {

    $out = parent::get();

    /*
    if (!empty($this->value)) {
    $out['#value'] = $this->value;
    }
     */

    return $out;
  }

}
