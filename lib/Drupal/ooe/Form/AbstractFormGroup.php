<?php

/**
 * @file
 * AbstractFormGroup.
 */

namespace Drupal\ooe\Form;

/**
 * Collects aspects common to a form field group.
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/2312 AbstractFormGroup @endlink.
 *
 * @author darrenkelly
 */
abstract class AbstractFormGroup extends AbstractFormField implements IFormGroup {

  /**
   * Constructor.
   *
   * @param string $type
   *   The (translated) field group title.
   * @param string $title
   *   The field group type.
   */
  protected function __construct($type, $title) {
    parent::__construct($type, $title);
  }

  /**
   * An array of form field children.
   *
   * @var IFormField[]
   */
  private $fields = array();

  /**
   * Adds a form field child with the given array key.
   *
   * @param string $key
   *   The array key for the form field.
   * @param IFormField $field
   *   The child form field.
   *
   * @return IFormGroup
   *   This.
   */
  public function add($key, IFormField $field) {
    // @todo Checks.
    $this->fields[$key] = $field;
    return $this;
  }

  /**
   * Gets the Drupal form array portion for this group, including its children.
   *
   * @return array
   *   The Drupal form array portion for this group.
   */
  public function get() {

    $out = parent::get();

    $kids = array();
    foreach ($this->fields as $key => $f) {
      $kids[$key] = $f->get();
    }
    return array_merge($out, $kids);
  }

}
