<?php

/**
 * @file
 * AbstractFormField.
 */

namespace Drupal\ooe\Form;

/**
 * Collects aspects common to all form fields.
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/2308 AbstractFormField @endlink.
 *
 * @author darrenkelly
 */
abstract class AbstractFormField implements IFormField {

  /**
   * The type of the field.
   *
   * @var string
   */
  private $type;

  /**
   * Sets the type of the field.
   *
   * @param string $type
   *   The type of the field.
   *
   * @return IFormField
   *   This.
   */
  public function setType($type) {
    // @todo checks.
    $this->type = $type;
    return $this;
  }

  /**
   * The title of the field.
   *
   * @var string
   */
  private $title;

  /**
   * Sets the title of the field.
   *
   * @param string $title
   *   The (translated) title of the field.
   *
   * @return IFormField
   *   This.
   */
  public function setTitle($title) {
    // @todo checks.
    $this->title = $title;
    return $this;
  }

  /**
   * The field type.
   * @return string
   *   The field type.
   */
  protected function getType() {
    return $this->type;
  }

  /**
   * Constructor: The title will ALWAYS be translated automatically.
   *
   * @param string $type
   *   The field type.
   * @param string $title
   *   The (translated) field title.
   */
  protected function __construct($type, $title) {
    $this->setType($type);
    $this->setTitle($title);
  }

  /**
   * The field description.
   * @var string
   */
  private $description;

  /**
   * Sets the field description.
   *
   * @param string $description
   *   The (translated) field description.
   *
   * @return IFormField
   *   This.
   */
  public function setDescription($description) {
    // @todo checks.
    $this->description = $description;
    return $this;
  }

  /**
   * A Drupal form array portion.
   *
   * @return array
   *   A Drupal form array portion.
   */
  public function get() {

    $out = array();

    if (!empty($this->type)) {
      $out['#type'] = $this->type;
    }

    if (!empty($this->title)) {
      $out['#title'] = $this->title;
      // Assumes already translated !
    }

    if (!empty($this->description)) {
      $out['#description'] = $this->description;
      // Assumes already translated !
    }

    return $out;
  }

}
