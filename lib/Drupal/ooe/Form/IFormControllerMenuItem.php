<?php

/**
 * @file
 * IFormControllerMenuItem.
 */

namespace Drupal\ooe\Form;

/**
 * A menu item for a @link IFormController @endlink.
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/2329 IFormControllerMenuItem @endlink.
 *
 * @author darrenkelly
 */
interface IFormControllerMenuItem extends IFormMenuItem {

  /**
   * The form controller of this.
   *
   * @return IFormController
   *   The form controller of this.
   */
  public function getFormController();

}
