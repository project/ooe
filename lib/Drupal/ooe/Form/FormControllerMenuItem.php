<?php

/**
 * @file
 * FormControllerMenuItem.
 */

namespace Drupal\ooe\Form;

use Drupal\ooe\Form\FormMenuItem;

/**
 * A menu item for a controlled form.
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/2330 FormControllerMenuItem @endlink.
 *
 * For a live demonstration please visit
 * @link http://drupal7demo.webel.com.au/ooe/demo_forms/controlled @endlink.
 *
 * @author darrenkelly
 */
class FormControllerMenuItem extends FormMenuItem implements IFormControllerMenuItem {

  /**
   * A form controller.
   *
   * @var IFormController
   */
  private $formController;

  /**
   * The form controller of this.
   *
   * @return IFormController
   *   The form controller of this.
   */
  public function getFormController() {
    // @todo: SECURITY?
    return $this->formController;
  }

  /**
   * Constructor: requires a form controller.
   *
   * Forces the page arguments to
   * use @link AbstractFormController::createForm @endlink
   * ($form, &$form_state, $vargs).
   *
   * @param IFormController $formController
   *   Required. The object-oriented form controller.
   * @param string $module
   *   The module machine name.
   * @param string $title
   *   The (translated) form title.
   * @param string $path
   *   The form menu item path.
   */
  public function __construct(IFormController $formController, $module, $title, $path) {
    parent::__construct($module, $title, $path);

    $this->formController = $formController;

    $this->pageArguments = array(
      '\Drupal\\ooe\\Form\\AbstractFormController::createForm',
      get_class($formController),
      // Will propagate via drupal_get_form to delegator !
    );
    // Your 'page arguments' should always be the same
    // (it will delegate to the eventual form builder).
  }

  /**
   * Overridden to prevent resetting of locally fixed delegator value.
   *
   * There is no return.
   *
   * @param array $pageArguments
   *   The page arguments array.
   *
   * @throws \Exception
   *   If an attempt is made to (re)set the page arguments.
   */
  final public function setPageArguments(array $pageArguments) {
    throw new \Exception("IGNORED: page arguments are forced to '" . $this->pageArguments . "' !");
  }

}
