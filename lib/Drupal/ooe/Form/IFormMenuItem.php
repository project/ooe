<?php

/**
 * @file
 * IFormMenuItem.
 */

namespace Drupal\ooe\Form;

use Drupal\ooe\Menu\IMenuItem;

/**
 * A menu item for an uncontrolled form.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2028 IFormMenuItem @endlink.
 *
 * @author darrenkelly
 */
interface IFormMenuItem extends IMenuItem {

}
