<?php

/**
 * @file
 * IFormTextField.
 */

namespace Drupal\ooe\Form;

/**
 * A form field that handles a text value.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2319 IFormTextField @endlink.
 *
 * @author darrenkelly
 */
interface IFormTextField extends IFormValueField {

  /**
   * Sets the size of the text field.
   *
   * @param int $size
   *   The size of the text field.
   *
   * @return IFormTextField
   *   This.
   */
  public function setSize($size);

  /**
   * Sets the maximum length of the text field.
   *
   * @param int $maxlength
   *   The maximum length of the text field.
   *
   * @return IFormTextField
   *   This.
   */
  public function setMaxlength($maxlength);

}
