<?php

/**
 * @file
 * BartikRegions.
 */

namespace Drupal\ooe\Theme;

/**
 * Encapsulates names of regions of the Bartik theme as constants.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2303 BartikRegions @endlink.
 *
 * @todo Consider repackage.
 *
 * @author darrenkelly
 */
class BartikRegions {

  const SIDEBAR_FIRST = 'sidebar_first';

}
