<?php

/**
 * @file
 * ThemeHelper.
 */

namespace Drupal\ooe\Theme;

/**
 * Helper class that encapsulates some Drupal theming conventions.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2302 ThemeHelper @endlink.
 *
 * @author darrenkelly
 */
class ThemeHelper {

  /**
   * Encapsulates the theme hook suggestion '__' double underscore convention.
   *
   * @param string $theme_function
   *   The theme function.
   * @param string $module
   *   The module machine name.
   *
   * @return string
   *   A theme hook suggestion "theme_function__module".
   */
  static public function makeSuggestion($theme_function, $module) {
    return $theme_function . '__' . $module;
  }

}
