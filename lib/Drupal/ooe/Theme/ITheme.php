<?php

/**
 * @file
 * ITheme.
 */

namespace Drupal\ooe\Theme;

/**
 * A Drupal theme.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2301 ITheme @endink.
 *
 * @todo So far placeholder only.
 *
 * @author darrenkelly
 */
interface ITheme {

  /**
   * Drupal item list constant.
   *
   * @var
   */
  const ITEM_LIST = "item_list";

}
