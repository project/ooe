<?php

/**
 * @file
 * AbstractPageController.
 */

namespace Drupal\ooe\Page;

use Drupal\page_controller\Controller\PageController;
use Drupal\ooe\Module\DefaultModuleHelper;
use Drupal\ooe\Factory\IFactory;
use Drupal\ooe\Render\RenderFactory;

/**
 * Implements shared aspects of all OOE page controllers,
 * which extend the strategy of
 * the @link https://drupal.org/project/page_controller Page Controller @endlink
 * project.
 *
 * Because PHP does not support multiple inheritance of implementations,
 * we have to mixin the implementation of @link IModuleHelper @endlink
 * available via @link DefaultModuleHelper @endlink.
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/1021 AbstractPageController @endlink.
 *
 * @author darrenkelly
 */
abstract class AbstractPageController extends PageController implements IPageController {

  /**
   * Delegate for mixin.
   *
   * Because of single inheritance in PHP need
   * wrap a delegate for mixin implementations.
   *
   * @var \Drupal\ooe\Module\IModuleHelper
   */
  private $moduleHelper;

  /**
   * Mixin. Sets the module machine name.
   *
   * @param string $module
   *   The module machine name.
   *
   * @return \Drupal\ooe\Module\IModuleHelper
   *   The module helper mixin of this.
   */
  public function setModule($module) {
    $this->moduleHelper($module);
    return $this->moduleHelper;
    // So signature the same !
  }

  /**
   * Mixin. The module machine name.
   *
   * @return string
   *   The module machine name.
   */
  public function getModule() {
    return $this->moduleHelper->getModule();
  }

  /**
   * Mixin: A human readable and displayable name of the module.
   *
   * @return string
   *   The module display name (if set)
   *   or the module machine name as upper case.
   */
  public function getModuleDisplayName() {
    return $this->moduleHelper->getModuleDisplayName();
  }

  /**
   * Mixin. Sets the module display name.
   *
   * @param string $displayName
   *   A human readable and displayable name of the module.
   *
   * @return IModuleHelper
   *   The module helper mixin of this.
   */
  public function setModuleDisplayName($displayName) {
    return $this->moduleHelper->setModuleDisplayName($displayName);
  }

  /**
   * Constructor.
   *
   * @param string $module
   *   The module machine name.
   */
  protected function __construct($module) {
    $this->moduleHelper = new DefaultModuleHelper($module);
    // Delegate for mixins.
  }

  /**
   * The callback name.
   *
   * @return string
   *   The callback name.
   */
  abstract public function getCallbackName();

  /**
   * Mixin. Sets the factory of this.
   *
   * @param \Drupal\ooe\Factory\IFactory $factory
   *   A factory.
   *
   * @return \Drupal\ooe\Module\IModuleHelper
   *   The module helper mixin of this.
   */
  public function setFactory(IFactory $factory) {
    return $this->moduleHelper->setFactory($factory);
  }

  /**
   * Mixin. The factory of this.
   *
   * @return \Drupal\ooe\Factory\IFactory
   *   The module helper mixin of this.
   */
  public function getFactory() {
    return $this->moduleHelper->getFactory();
  }

  /**
   * Shorthand version.
   *
   * @return \Drupal\ooe\Factory\IFactory
   *   The factory.
   */
  protected function factory() {
    return $this->getFactory();
  }

  /**
   * A render factory for building pages with Drupal render arrays.
   *
   * @var \Drupal\ooe\Render\IRenderFactory
   */
  private $renderFactory;

  /**
   * Lazily creates a render factory.
   *
   * @return IRenderFactory
   *   A render factory.
   */
  protected function renderFactory() {
    if (empty($this->renderFactory)) {
      $this->renderFactory = new RenderFactory();
    }
    return $this->renderFactory;
  }

  /**
   * Shorthand version of the lazily created render factory.
   *
   * Provided for convenience since this is used so often.
   *
   * @return IRenderFactory
   *   A render factory.
   */
  protected function rf() {
    return $this->renderFactory();
  }

  /**
   * Shorthand version of the tag factory of the current render factory.
   *
   * Provided for convenience since this may be used often.
   *
   * @return ITagFactory
   *   A tag factory.
   */
  protected function tf() {
    return $this->renderFactory()->getTagFactory();
  }

}
