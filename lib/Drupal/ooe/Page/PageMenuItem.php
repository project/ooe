<?php

/**
 * @file
 * PageMenuItem.
 */

namespace Drupal\ooe\Page;

use Drupal\ooe\Menu\DefaultMenuItem;

/**
 * Implements a menu item with an object-oriented page controller.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2295 PageMenuItem @endlink.
 *
 * @author darrenkelly
 */
class PageMenuItem extends DefaultMenuItem implements IPageMenuItem {

  /**
   * The page controller.
   *
   * @var IPageController
   */
  private $pageController;

  /**
   * The page controller.
   *
   * @return IPageController
   *   The page controller.
   */
  final public function getPageController() {
    return $this->pageController;
  }

  /**
   * Constructor: requires a page controller.
   *
   * Fixes the page callback.
   *
   * @param IPageController $pageController
   *   Required: the page controller.
   * @param string $module
   *   The module machine name.
   * @param string $title
   *   The menu item title.
   * @param string $path
   *   The menu item path.
   */
  public function __construct(IPageController $pageController, $module, $title, $path) {
    // @todo checks.
    parent::__construct($module, $title, $path);
    $this->pageController = $pageController;
    $this->pageCallback = '\Drupal\\page_controller\\Controller\\PageController::createPage';
    // 'Your page callback should always be the same.'
    // 'page callback' => ,
  }

  /**
   * Overridden to prevent attempts to change the fixed page callback.
   *
   * Nothing is returned.
   *
   * @param string $pageCallback
   *   A non-empty string representing the page callback.
   *
   * @throws \Exception
   *   If an attempt is made to override the fixed page callback.
   */
  final public function setPageCallback($pageCallback) {
    throw new \Exception(
        'IGNORED: page callback fixed for controlled page menu item'
        );
  }

  /**
   * Overridden: special case to insert additional args at the beginning.
   *
   * Overridden: special case to insert additional args at the beginning
   * representing the callback class and callback method for a controller call.
   *
   * @return array
   *   The page arguments decorated to include the page controller callback,
   *   which will be popped on processing before forwarding remaining args.
   */
  protected function getPageArgumentsDecorated() {
    // DEBUG dpm($this->pageArguments,'pageArguments');//DEBUG
    $caller = array(
      '\\' . get_class($this->pageController),
      $this->pageController->getCallbackName(),
    );
    if (empty($this->pageArguments)) {
      return $caller;
      // Because merge_array(array,NULL/empty) throws error.
    }
    elseif (is_array($this->pageArguments)) {
      return array_merge($caller, $this->pageArguments);
    }
    else {
      return $this->pageArguments;
      // Should be null.
    }
  }

}
