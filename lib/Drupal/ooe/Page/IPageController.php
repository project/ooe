<?php

/**
 * @file
 * IPageController.
 */

namespace Drupal\ooe\Page;

use Drupal\ooe\Module\IModuleHelper;

/**
 * Controls a page callback method.
 *
 * Usually for an @link IPageMenuItem @endlink.
 *
 * Since Drupal callbacks are string-based only (do not explicitly use the
 * @link http://au1.php.net/manual/en/language.types.callable.php PHP callable type @endlink
 * this identifies the callback by name only.
 * It is thus entirely the responsibility
 * of the implementation to provide the expected,
 * correctly named callback method.
 *
 * It can't be tracked using a method name and specific
 * signature here since that
 * would imply making assumptions about the number of parameters,
 * and PHP does not support method overloading !
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/1024 IPageController @endlink
 *
 * @author darrenkelly
 */
interface IPageController extends IModuleHelper {

  /**
   * The callback method name.
   *
   * It is up to the implementation to provide an ultimate callback method
   * with this name for building the page (usually with render arrays).
   * No assumptions are made about the number of arguments.
   *
   * A @link PageController @endlink will create an instance of this
   * controller and will delegate the initial callback to the method
   * named here, passing on to it any Drupal page path arguments.
   *
   * @return string
   *   The name of the page builder callback method.
   */
  public function getCallbackName();

}
