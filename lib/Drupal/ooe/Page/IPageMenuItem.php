<?php

/**
 * @file
 * IPageMenuItem.
 */

namespace Drupal\ooe\Page;

use Drupal\ooe\Menu\IMenuItem;

/**
 * A menu item with an object-oriented page controller.
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/1024 IPageMenuItem @endlink.
 *
 * @author darrenkelly
 */
interface IPageMenuItem extends IMenuItem {

  /**
   * The page controller associated with this.
   *
   * @return IPageController
   *   The page controller associated with this.
   */
  public function getPageController();
  // @todo Security?

}
