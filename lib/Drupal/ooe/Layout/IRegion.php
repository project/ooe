<?php

/**
 * @file
 * IRegion.
 */

namespace Drupal\ooe\Layout;

/**
 * A region of a theme.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2298 IRegion @endlink.
 *
 * @author darrenkelly
 */
interface IRegion {

  /**
   * A non-null name identifying the region within the theme layout.
   *
   * @return string
   *   A non-null name identifying the region within the theme layout.
   */
  public function getName();

}
