<?php

/**
 * @file
 * DefaultRegion.
 */

namespace Drupal\ooe\Layout;

/**
 * Default implementation of a region of a theme.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2299 DefaultRegion @endlink.
 *
 * @author darrenkelly
 */
class DefaultRegion implements IRegion {

  /**
   * A non-null name identifying the region.
   *
   * @var string
   */
  private $name;

  /**
   * A non-null name identifying the region.
   *
   * @return string
   *   A non-null name identifying the region.
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Constructor.
   *
   * It is recommended that the name be obtained via
   * a class that encapsulates constants representing
   * the names of regions for a given theme.
   *
   * @param string $name
   *   A non-null name identifying the region.
   *
   * @throws Exception
   *   If the $name is not a non empty string.
   *
   * @see BartikRegions
   */
  public function __construct($name) {
    if (empty($name) || !is_string($name)) {
      throw new \Exception("$name must be a non empty string !");
    }
    $this->name = $name;
  }

}
