<?php

/**
 * @file
 * RenderB.
 */

namespace Drupal\ooe\Render\Tag;

use Drupal\ooe\Render\Render;
use Drupal\ooe\Xhtml\XHTML;
use Drupal\ooe\Xhtml\Tag;

/**
 * Renders a B (bold) with simple markup text, optionally translating the text.
 *
 * @deprecated Inflexible, leads to class explosion.
 * Prefer @link IRenderFactory @endlink methods that
 * inject an @link ITag @endlink.
 *
 * This is not really suitable for markup text with
 * complex HTML markup, although you can pass anything you like to it.
 *
 * @author darrenkelly
 */
class RenderB extends Render {

  /**
   * Constructor.
   *
   * @param string $text
   *   Required. The (relatively simple, translated) text markup.
   * @param array $attributes
   *   Optional. A Drupal-style attributes array for the wrapping B tag.
   */
  public function __construct(
      $text,
      array $attributes = NULL) {
    if (empty($text) || !is_string($text)) {
      throw new \Exception('$text must be a non empty string');
    }
    parent::__construct($text, new Tag(XHTML::B, $attributes));
  }

}
