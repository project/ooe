<?php

/**
 * @file
 * RenderH.
 */

namespace Drupal\ooe\Render\Tag;

use Drupal\ooe\Render\Render;

use Drupal\ooe\Xhtml\Tag;


/**
 * Renders an HTML header.
 *
 * @deprecated Inflexible, leads to class explosion.
 * Prefer @link IRenderFactory @endlink methods that
 * inject an @link ITag @endlink.
 *
 * The "size" of the HTML header can be chosen.
 *
 * @author darrenkelly
 */
class RenderH extends Render {

  /**
   * Constructs a header with simple text.
   *
   * @param string $header
   *   The (translated) header text.
   * @param int $size
   *   An integer from 1 to 6 for the HTML H1 .. H6 header choice.
   * @param array $attributes
   *   Optional. A Drupal-style attributes array for the wrapping P tag.
   */
  public function __construct(
      $header, $size = 1,
      array $attributes = NULL) {
    if (!empty($size) && (!is_int($size) || $size < 1 || $size > 6)) {
      throw new \Exception('$size must be an integer from 1 .. 6');
    }
    if (empty($header) || !is_string($header)) {
      throw new \Exception('$header must be a non empty string');
    }
    parent::__construct($header, new Tag("h$size", $attributes));
  }

}
