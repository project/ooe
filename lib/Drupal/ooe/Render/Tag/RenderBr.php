<?php

/**
 * @file
 * RenderBr.
 */

namespace Drupal\ooe\Render\Tag;

use Drupal\ooe\Xhtml\XHTML;

use Drupal\ooe\Xhtml\Tag;

use Drupal\ooe\Render\Render;

/**
 * Renders a BR line break.
 *
 * @deprecated Inflexible, leads to class explosion.
 * Prefer @link IRenderFactory @endlink methods that
 * inject an @link ITag @endlink.
 *
 * @author darrenkelly
 */
class RenderBr extends Render {

  /**
   * Constructor.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array for the wrapping BR tag.
   */
  public function __construct(array $attributes = NULL) {
    parent::__construct(NULL, new Tag(XHTML::BR, $attributes));
  }

}
