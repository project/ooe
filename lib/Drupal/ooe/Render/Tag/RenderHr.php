<?php

/**
 * @file
 * RenderHr.
 */

namespace Drupal\ooe\Render\Tag;

use Drupal\ooe\Xhtml\XHTML;

use Drupal\ooe\Render\Render;

use Drupal\ooe\Xhtml\Tag;

/**
 * Renders an HR horizontal line.
 *
 * @deprecated Inflexible, leads to class explosion.
 * Prefer @link IRenderFactory @endlink methods
 * that inject an @link ITag @endlink.
 *
 * @author darrenkelly
 */
class RenderHr extends Render {

  /**
   * Constructor.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array for the wrapping HR tag.
   */
  public function __construct(array $attributes = NULL) {
    parent::__construct(NULL, new Tag(XHTML::HR, $attributes));
  }

}
