<?php

/**
 * @file
 * RenderEm.
 */

namespace Drupal\ooe\Render\Tag;

use Drupal\ooe\Xhtml\XHTML;

use Drupal\ooe\Xhtml\Tag;

use Drupal\ooe\Render\Render;

/**
 * Renders an EM (emphasis) with simple markup text.
 *
 * @deprecated Inflexible, leads to class explosion.
 * Prefer @link IRenderFactory @endlink methods
 * that inject an @link ITag @endlink.
 *
 * This is not really suitable for markup text
 * with complex HTML markup, although you can pass anything you like to it.
 *
 * @author darrenkelly
 */
class RenderEm extends Render {

  /**
   * Constructor.
   *
   * @param string $text
   *   Required. The (relatively simple, translated) text markup.
   * @param array $attributes
   *   Optional. A Drupal-style attributes array for the wrapping EM tag.
   */
  public function __construct(
      $text,
      array $attributes = NULL) {
    if (empty($text) || !is_string($text)) {
      throw new \Exception('$text must be a non empty string');
    }
    parent::__construct($text, new Tag(XHTML::EM, $attributes));
  }

}
