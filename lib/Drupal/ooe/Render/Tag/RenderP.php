<?php

/**
 * @file
 * RenderP.
 */

namespace Drupal\ooe\Render\Tag;

use Drupal\ooe\Xhtml\XHTML;

use Drupal\ooe\Render\Render;

use Drupal\ooe\Xhtml\Tag;

/**
 * Renders a simple paragraph.
 *
 * @deprecated Inflexible, leads to class explosion.
 * Prefer @link IRenderFactory @endlink methods that
 * inject an @link ITag @endlink.
 *
 * This is NOT really suitable for paragraph text
 * with complex HTML markup, although you can pass anything you like to it.
 *
 * This is not intended for use with complex render structures;
 * for that see instead @link RenderSet @endlink
 * and @link IRenderFactory::newSetP @endlink, which has
 * a facility for accepting render object children.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2066 RenderP @endlink.
 *
 * @author darrenkelly
 */
class RenderP extends Render {

  /**
   * Constructor.
   *
   * @param string $text
   *   Required. The (simple, translated) text markup for the paragraph.
   * @param array $attributes
   *   Optional. A Drupal-style attributes array for the wrapping P tag.
   */
  public function __construct(
      $text,
      array $attributes = NULL) {
    if (empty($text) || !is_string($text)) {
      throw new \Exception('$text must be a non empty string');
    }
    parent::__construct($text, new Tag(XHTML::P, $attributes));
  }

}
