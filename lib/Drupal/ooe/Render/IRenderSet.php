<?php

/**
 * @file
 * IRenderSet.
 */

namespace Drupal\ooe\Render;

/**
 * Groups render objects and constructs a composite Drupal render array.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2188 IRenderSet @endlink.
 *
 * @todo Consider renaming to IRenderGroup or IRenderMap
 * since 'set' has a specific meaning.
 *
 * @author darrenkelly
 */
interface IRenderSet extends IRender {

  /**
   * Add a render object item with the given key.
   *
   * @param string $key
   *   The array key.
   * @param IRender $item
   *   The render item to add.
   *
   * @return IRenderSet
   *   This.
   */
  public function addItem($key, IRender $item);

}
