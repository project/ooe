<?php

/**
 * @file
 * IRenderList.
 */

namespace Drupal\ooe\Render;

/**
 * Interface for themed list render arrays.
 *
 * @see theme_item_list()
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2193 IRenderList @endlink
 *
 * @author darrenkelly
 */
interface IRenderList extends IRender {

  /**
   * Sets 'The type of list to return (e.g. "ul", "ol")'.
   *
   * @param string $type
   *   'The type of list to return (e.g. "ul", "ol")'.
   *
   * @return \Drupal\ooe\Render\IRenderList
   *   This.
   */
  public function setType($type);

  /**
   * Sets this to use an ordered list.
   *
   * @return \Drupal\ooe\Render\IRenderList
   *   This.
   */
  public function setTypeOrdered();

  /**
   * Sets this to use an unordered list.
   *
   * @return \Drupal\ooe\Render\IRenderList
   *   This.
   */
  public function setTypeUnordered();

  /**
   * Sets this to use a description list.
   *
   * @return \Drupal\ooe\Render\IRenderList
   *   This.
   */
  public function setTypeDescription();

  /**
   * Sets 'The attributes applied to the list element'.
   *
   * From drupal_attributes:
   * 'Converts an associative array to an XML/HTML tag attribute string'.
   *
   * @param array $attributes
   *   'The attributes applied to the list element.'
   *    Suitable for drupal_attributes(array).
   *
   * @return \Drupal\ooe\Render\IRenderList
   *   This.
   */
  public function setAttributes(array $attributes);
  // @todo OOE type.

  /**
   * Forces construction of a theme hook suggestion.
   *
   * For the given module machine name.
   *
   * @param string $module
   *   The module machine name.
   *
   * @return \Drupal\ooe\Render\IRenderList
   *   This.
   */
  public function setThemeHookSuggestionModule($module);

}
