<?php

/**
 * @file
 * RenderList.
 */

namespace Drupal\ooe\Render;

use Drupal\ooe\Theme\ITheme;
use Drupal\ooe\Theme\ThemeHelper;
use Drupal\ooe\Xhtml\XHTML;

/**
 * Render array for a themed list.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2193 RenderList @endlink.
 *
 * @see theme_item_list()
 *
 * @author darrenkelly
 */
class RenderList extends Render implements IRenderList {

  /**
   * The title of the list.
   *
   * @var string
   */
  private $title;

  /**
   * The managed list of items.
   *
   * @var IListItems
   */
  private $listItems;

  /**
   * Constructor.
   *
   * @param IListItems $listItems
   *   The managed list of items.
   * @param string $title
   *   (Optional.) 'The title of the list.' Translated. Defaults to null.
   */
  public function __construct(IListItems $listItems, $title = NULL) {
    $this->listItems = $listItems;
    if (!empty($title) && !is_string($title)) {
      throw new \Exception('$title must a string (or null) !');
    }
    if (!empty($title)) {
      $this->title = $title;
      // Parent ignored !
    }
  }

  /**
   * The HTML list type.
   *
   * @var string
   */
  private $type;

  /**
   * Gets 'The type of list to return (e.g. "ul", "ol")'.
   *
   * @return string
   *   'The type of list to return (e.g. "ul", "ol")'.
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Sets 'The type of list to return (e.g. "ul", "ol")'.
   *
   * @param string $type
   *   'The type of list to return (e.g. "ul", "ol")'.
   */
  public function setType($type) {
    if (empty($type) || !is_string($type)) {
      throw new \Exception('$type must be a non empty string');
    }
    $t = strtolower($type);
    if (!($t == XHTML::OL || $t == XHTML::UL || $t = XHTML::DL)) {
      // @todo leverage XHTML definitions.
      throw new \Exception(
      '$type must be one of (' . XHTML::OL . ',' . XHTML::UL . '', '' . XHTML::DL . ') !'
      );
    }
    $this->type = $t;
    return $this;
  }

  /**
   * Sets to use an ordered list.
   *
   * @return \Drupal\ooe\Render\IRenderList
   *   This.
   */
  public function setTypeOrdered() {
    $this->setType(XHTML::OL);
    return $this;
  }

  /**
   * Sets to use an unordered list.
   *
   * @return \Drupal\ooe\Render\IRenderList
   *   This.
   */
  public function setTypeUnordered() {
    $this->setType(XHTML::UL);
    return $this;
  }

  /**
   * Sets to use a description list.
   *
   * @return \Drupal\ooe\Render\IRenderList
   *   This.
   */
  public function setTypeDescription() {
    $this->setType(XHTML::DL);
    return $this;
  }

  /**
   * The Drupal attributes array.
   *
   * @var string
   */
  private $attributes;

  /**
   * Sets 'The attributes applied to the list element'.
   *
   * From drupal_attributes:
   * 'Converts an associative array to an XML/HTML tag attribute string'.
   *
   * @param array $attributes
   *   'The attributes applied to the list element.'
   *    Suitable for drupal_attributes(array).
   *
   * @return \Drupal\ooe\Render\IRenderList
   *   This.
   */
  public function setAttributes(array $attributes) {
    // @todo type checking within array.
    $this->attributes = $attributes;
    return $this;
  }

  /**
   * Machine name of the module for theme hook suggestion.
   *
   * @var string
   */
  private $module;

  /**
   * Forces construction of a theme hook suggestion for the given module name.
   *
   * @param string $module
   *   The module machine name.
   *
   * @throws \Exception
   *   If the given module machine name is not a non empty string.
   */
  public function setThemeHookSuggestionModule($module) {
    if (empty($module) || !is_string($module)) {
      throw new \Exception('$module must be a non empty string');
    }
    $this->module = $module;
    return $this;
  }

  /**
   * Builds and gets the list render array.
   *
   * @return array
   *   The Drupal list render array.
   */
  public function get() {

    $out = array();
    $out['#items'] = $this->listItems->get();

    if (!empty($this->module)) {
      // Use theme hook suggestion.
      $out['#theme'] = ThemeHelper::makeSuggestion(ITheme::ITEM_LIST, $this->module);
    }
    else {
      $out['#theme'] = ITheme::ITEM_LIST;
    }

    if (!empty($this->title)) {
      $out['#title'] = $this->title;
      // Assumes already translated.
    }
    if (!empty($this->type)) {
      $out['#type'] = $this->type;
    }
    if (!empty($this->attributes)) {
      $out['#attributes'] = $this->attributes;
    }
    return $out;
  }

}
