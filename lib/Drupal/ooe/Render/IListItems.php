<?php

/**
 * @file
 * IListItems.
 */

namespace Drupal\ooe\Render;

/**
 * Interface for possibly nested list items.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2187 IListItems @endlink.
 *
 * @see theme_item_list()
 *
 * @author darrenkelly
 */
interface IListItems {

  /**
   * A Drupal list items array suitable for theme_item_list().
   *
   * 'An array of items to be displayed in the list.
   * If an item is a string, then it is used as is.
   * If an item is an array, then the "data" element of
   * the array is used as the contents of the list item.
   * If an item is an array with a "children" element,
   * those children are displayed in a nested list.
   * All other elements are treated as attributes
   * of the list item element.'
   *
   * The OOE implementation only uses the 'data' element approach,
   * it does not support an item that is only a string.
   *
   * @return array
   *   A Drupal list items array suitable for theme_item_list().
   */
  public function get();

  /**
   * Add the given list item as a child of this.
   *
   * @param IListItem $item
   *   The non-empty list item to add as a child to this.
   *
   * @return IListItems
   *   This.
   */
  public function addItem(IListItem $item);

  /**
   * Whether this has any children.
   *
   * @return bool
   *   Whether this has any children.
   */
  public function hasItems();

}
