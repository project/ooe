<?php

/**
 * @file
 * RenderHelper.
 */

namespace Drupal\ooe\Render;

/**
 * Manages an @link IRenderFactory @endlink.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2019 RenderHelper @endlink.
 *
 * @author darrenkelly
 */
class RenderHelper {

  /**
   * Set or reset the render factory.
   *
   * @param IRenderFactory $renderFactory
   *   If given will be set as the render factor, otherwise
   *   a default @link RenderFactory @endlink will be used.
   */
  static public function setRenderFactory(IRenderFactory $renderFactory = NULL) {
    if (!empty(self::$renderFactory)) {
      self::$renderFactory = $renderFactory;
    }
  }

  /**
   * A render factory for building pages with Drupal render arrays.
   *
   * @var \Drupal\ooe\Render\IRenderFactory
   */
  static private $renderFactory;

  /**
   * Lazily creates a render factory.
   *
   * @return \Drupal\ooe\Render\IRenderFactory
   *   A render factory.
   */
  static public function getRenderFactory() {
    if (empty(self::$renderFactory)) {
      self::$renderFactory = new RenderFactory();
    }
    return self::$renderFactory;
  }

  /**
   * Shorthand version of the lazily created render factory.
   *
   * Provided for convenience since this is used so often.
   *
   * @return \Drupal\ooe\Render\IRenderFactory
   *   A render factory.
   */
  static protected function rf() {
    return self::getRenderFactory();
  }

  /**
   * Shorthand version of the tag factory of the current render factory.
   *
   * Provided for convenience since this may be used often.
   *
   * @return \Drupal\ooe\Xhtml\ITagFactory
   *   A tag factory.
   */
  static protected function tf() {
    return self::rf()->getTagFactory();
  }

}
