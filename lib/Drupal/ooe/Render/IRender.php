<?php

/**
 * @file
 * IRender.
 */

namespace Drupal\ooe\Render;

use Drupal\ooe\Xhtml\ITag;

/**
 * Interface for all render array managers.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2030 IRender @endlink.
 *
 * Visit also:
 * - @link https://drupal.org/node/930760 Render Arrays in Drupal 7 @endlink
 * - drupal_render(&$elements)
 *
 * @author darrenkelly
 */
interface IRender {

  /**
   * Builds and returns a Drupal render array.
   *
   * @return array
   *   A Drupal render array.
   */
  public function get();

  /**
   * Sets the render prefix.
   *
   * Prefer @link IRender::setWrapperTag @endlink(ITag)
   * over use of freestyle, error prone XHTML strings !
   *
   * @param string $prefix
   *   The render prefix; may contain HTML.
   *
   * @return IRender
   *   This.
   *
   * @see drupal_render(&$elements)
   */
  public function setPrefix($prefix);

  /**
   * Sets the render suffix.
   *
   * Prefer @link IRender::setWrapperTag @endlink(ITag)
   * over use of freestyle, error prone XHTML strings !
   *
   * @param string $suffix
   *   The render prefix; may contain HTML. May be null.
   *
   * @return IRender
   *   This.
   *
   * @see drupal_render(&$elements);
   */
  public function setSuffix($suffix);

  /**
   * If this has a wrapper tag, it will be used to build a prefix.
   *
   * Otherwise the prefix as explicitly set will be used.
   *
   * @return string
   *   The prefix.
   */
  public function getPrefix();

  /**
   * If this has a wrapper tag, it will be used to build a suffix.
   *
   * Otherwise the suffix as explicitly set will be used.
   *
   * @return string
   *   The suffix.
   */
  public function getSuffix();

  /**
   * Sets a wrapper tag.
   *
   * Will be used
   * to generate the prefix and suffix of this.
   *
   * This is an extremely important operation
   * in the OOE render object strategy, because
   * it enables one to essentially associate
   * a managed XHTML tag, including its attributes,
   * with a portion of a Drupal render array.
   *
   * This approach is far less error prone than
   * using freestyle XHTML strings
   * in @link IRender::setPrefix @endlink(string)
   * and @link IRender::setSuffix @endlink(string),
   * and should be preferred over those operations
   * whenever possible.
   *
   * It is particularly powerful when coupled
   * with the convenient creation methods
   * of @link IRenderFactory @endlink.
   *
   * @param ITag $tag
   *   The XHTML tag holder.
   *
   * @return IRender
   *   This.
   */
  public function setWrapperTag(ITag $tag);

}
