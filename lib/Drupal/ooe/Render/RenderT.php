<?php

/**
 * @file
 * RenderT.
 */

namespace Drupal\ooe\Render;

use Drupal\ooe\Xhtml\ITag;

/**
 * A simple markup render array that always translates the given markup.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2194 RenderT @endlink.
 *
 * Visit also
 * @link https://api.drupal.org/api/drupal/includes%21bootstrap.inc/function/t/7 The Drupal t() function @endlink.
 *
 * @deprecated Because the Drupal localization registry does not catch
 * strings passed to t() via class variables this class that applies
 * translation to markup as internal policy should no longer be used.
 *
 * @author darrenkelly
 */
class RenderT extends Render {

  /**
   * Constructor.
   *
   * @param string $markup
   *   The markup string to be translated.
   * @param ITag $wrapperTag
   *   If present, will be used to "wrap" the markup
   *   with a prefix and suffix from the given XHTML tag
   *   (including any attributes in the prefix).
   * @param array $attributes
   *   Optional. A Drupal-style attributes array
   *   for the wrapping tag (if present). Will
   *   overwrite any existing attributes in the tag !.
   */
  public function __construct($markup, ITag $wrapperTag = NULL, array $attributes = NULL) {
    parent::__construct($markup, $wrapperTag, $attributes, TRUE);
  }

}
