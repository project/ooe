<?php

/**
 * @file
 * RenderFactory.
 */

namespace Drupal\ooe\Render;

use Drupal\ooe\Common;

use Drupal\ooe\Xhtml\XHTML;

use Drupal\ooe\Xhtml\ITag;

use Drupal\ooe\Xhtml\TagFactory;

/**
 * A factory for @link IRender @endlink implementations.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2067 RenderFactory @endlink.
 *
 * @todo Cover more render cases.
 *
 * @author darrenkelly
 */
class RenderFactory implements IRenderFactory {

  /**
   * A tag factory.
   *
   * @var \Drupal\ooe\Xhtml\ITagFactory
   */
  private $tagFactory;

  /**
   * A lazily created and configured tag factory.
   *
   * @return ITagFactory
   *   A lazily created and configured tag factory.
   */
  public function getTagFactory() {
    if (empty($this->tagFactory)) {
      $this->tagFactory = new TagFactory();
    }
    return $this->tagFactory;
  }

  /**
   * A lazily created and configured tag factory; short form.
   *
   * @return \Drupal\ooe\Xhtml\ITagFactory
   *   A lazily created and configured tag factory.
   */
  protected function tf() {
    return $this->getTagFactory();
  }

  /**
   * Supplies a most basic markup render object as product.
   *
   * @param string $markup
   *   Required. Translated ! The (relatively simple) markup, may contain HTML.
   * @param ITag $wrapperTag
   *   If present, will be used to "wrap" the markup
   *   with a prefix and suffix from the given XHTML tag holder.
   * @param array $attributes
   *   Optional. A Drupal-style attributes array
   *   for the wrapping tag (if present). Will
   *   overwrite any existing attributes in the tag !.
   *
   * @return \Drupal\ooe\Render\IRender
   *   A new render object.
   */
  public function newR(
  $markup, ITag $wrapperTag = NULL, array $attributes = NULL) {
    return new Render($markup, $wrapperTag, $attributes);
  }

  /**
   * Supplies a basic markup render object as product.
   *
   * Applies automatic translation of the given markup text.
   *
   * @param string $markup
   *   Required. The (relatively simple) markup, may contain HTML.
   * @param ITag $wrapperTag
   *   If present, will be used to "wrap" the markup
   *   with a prefix and suffix from the given XHTML tag holder.
   * @param array $attributes
   *   Optional. A Drupal-style attributes array
   *   for the wrapping tag (if present). Will
   *   overwrite any existing attributes in the tag !.
   *
   * @return \Drupal\ooe\Render\IRender
   *   A new render object with internally translated markup.
   *
   * @deprecated Because the Drupal localization registry
   * does not catch markup passed to t() as class variables.
   */
  public function newT($markup, ITag $wrapperTag = NULL, array $attributes = NULL) {
    return new RenderT($markup, $wrapperTag, $attributes);
  }

  /**
   * A new paragraph with simple markup text.
   *
   * This is not intended for use with complex render structures;
   * for that see instead @link IRenderFactory::newSetP @endlink.
   *
   * @param string $text
   *   The (simple, translated) text markup for the paragraph.
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return \Drupal\ooe\Render\IRender
   *   A new P paragraph render object.
   */
  public function newP($text, array $attributes = NULL) {
    return $this->newR($text, $this->tf()->newP($attributes));
  }

  /**
   * A new span with simple markup text.
   *
   * This is not really suitable
   * for use with complex content, consider instead using
   * a set/group via @link IRenderFactory::newSetSpan @endlink()
   * and adding smaller translatable pieces to the span.
   *
   * @param string $text
   *   The (simple, translated) text markup for the span.
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return \Drupal\ooe\Render\IRender
   *   A new SPAN render object.
   */
  public function newSpan($text, array $attributes = NULL) {
    return $this->newR($text, $this->tf()->newSpan($attributes));
  }

  /**
   * A new B (bold) with simple markup text.
   *
   * @param string $text
   *   The (simple, translated) text markup.
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return \Drupal\ooe\Render\IRender
   *   A new B bold render object.
   */
  public function newB($text, array $attributes = NULL) {
    return $this->newR($text, $this->tf()->newB($attributes));
  }

  /**
   * A new I (italic) with simple markup text.
   *
   * @param string $text
   *   The (simple, translated) text markup.
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return \Drupal\ooe\Render\IRender
   *   A new I italic render object.
   */
  public function newI($text, array $attributes = NULL) {
    return $this->newR($text, $this->tf()->newI($attributes));
  }

  /**
   * A new EM (emphasis) with simple markup text.
   *
   * @param string $text
   *   The (simple, translated) text markup.
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return \Drupal\ooe\Render\IRender
   *   A new EM render object.
   */
  public function newEm($text, array $attributes = NULL) {
    return $this->newR($text, $this->tf()->newEm($attributes));
  }

  /**
   * A new STRONG with simple markup text.
   *
   * @param string $text
   *   The (simple, translated) text markup.
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return \Drupal\ooe\Render\IRender
   *   A new STRONG render object.
   */
  public function newStrong($text, array $attributes = NULL) {
    return $this->newR($text, $this->tf()->newStrong($attributes));
  }

  /**
   * A new header with simple text.
   *
   * @param string $header
   *   The (translated) header text.
   * @param int $size
   *   An integer from 1 to 6 for the HTML H1 .. H6 header choice.
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return \Drupal\ooe\Render\IRender
   *   A new H header render object.
   */
  public function newH($header, $size, array $attributes = NULL) {
    if (!empty($size) && (!is_int($size) || $size < 1 || $size > 6)) {
      throw new \Exception('$size must be an integer from 1 .. 6');
    }
    if (empty($header) || !is_string($header)) {
      throw new \Exception('$header must be a non empty string');
    }
    return $this->newR($header, $this->tf()->newTag("h$size", $attributes));
  }

  /**
   * A new HR (horizontal line).
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return \Drupal\ooe\Render\IRender
   *   A new HR render object.
   */
  public function newHr(array $attributes = NULL) {
    return $this->newR(NULL, $this->tf()->newTag(XHTML::HR, $attributes));
  }

  /**
   * A new BR (line break).
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return \Drupal\ooe\Render\IRender
   *   A new BR render object.
   */
  public function newBr(array $attributes = NULL) {
    return $this->newR(NULL, $this->tf()->newTag(XHTML::BR, $attributes));
  }

  /**
   * A new themed list renderer.
   *
   * @param IListItems $items
   *   A list item manager that can build a
   *   Drupal items array for theme_item_list().
   * @param string $title
   *   (Optional.) 'The title of the list.' Translated ! Defaults to null.
   *
   * @return \Drupal\ooe\Render\IRenderList
   *   A new list renderer.
   */
  public function newList(IListItems $items, $title = NULL) {
    return new RenderList($items, $title);
  }

  /**
   * A new @link ListItems @endlink product.
   *
   * @return \Drupal\ooe\Render\IListItems
   *   A list items product.
   */
  public function newListItems() {
    return new ListItems();
  }

  /**
   * A new @link ListItem @endlink product.
   *
   * @todo accept attributes
   *
   * @todo is $data only string ?
   *
   * @param string $data
   *   From theme_item_list(): 'the "data" element ...
   *   is used as the contents of the list item'.
   *
   * @return \Drupal\ooe\Render\IListItem
   *   A list item product.
   */
  public function newListItem($data) {
    return new ListItem($data);
  }

  /**
   * A new menu subtree render object.
   *
   * Identified by the
   * link path of the parent of the subtree
   * within a given existing menu.
   *
   * @param string $link_path
   *   The menu link path to find.
   * @param string $menu
   *   The name of a valid menu to search in.
   *
   * @return \Drupal\ooe\Render\IRender
   *   A new menu subtree render object.
   */
  public function newMenuSubtree($link_path, $menu) {
    return new RenderMenuSubtree($link_path, $menu);
  }

  /**
   * A new render set (render group) for constructing composite render arrays.
   *
   * If a wrapper tag is given, and if an attributes
   * array is given, they will be applied to the tag
   * (before the tag is applied as render array prefix and suffix)
   *
   * @param ITag $wrapperTag
   *   If present, will be used to "wrap" all grouped elements
   *   with a prefix and suffix from the given XHTML tag manager.
   *   Typical examples would be a P or a DIV tag manager.
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return \Drupal\ooe\Render\IRenderSet
   *   A new render set.
   */
  public function newSet(ITag $wrapperTag = NULL, array $attributes = NULL) {
    return new RenderSet($wrapperTag, $attributes);
  }

  /**
   * A new render set/group with a P tag wrapper.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return \Drupal\ooe\Render\IRenderSet
   *   A new P render set.
   */
  public function newSetP(array $attributes = NULL) {
    return new RenderSet($this->tf()->newP($attributes));
  }

  /**
   * A new render set/group with a DIV tag wrapper.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return \Drupal\ooe\Render\IRenderSet
   *   A new DIV render set.
   */
  public function newSetDiv(array $attributes = NULL) {
    return new RenderSet($this->tf()->newDiv($attributes));
  }

  /**
   * A new render set/group with a SPAN tag wrapper.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return \Drupal\ooe\Render\IRenderSet
   *   A new span render set.
   */
  public function newSetSpan(array $attributes = NULL) {
    return new RenderSet($this->tf()->newSpan($attributes));
  }

  /**
   * A new render set/group with a BLOCKQUOTE tag wrapper.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return \Drupal\ooe\Render\IRenderSet
   *   A new blockquote render set.
   */
  public function newSetBlockquote(array $attributes = NULL) {
    return new RenderSet($this->tf()->newBlockquote($attributes));
  }

}
