<?php

/**
 * @file
 * ListItems.
 */

namespace Drupal\ooe\Render;

/**
 * Implements a (possibly nested) group of list items.
 *
 * @see theme_item_list()
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2191 ListItems @endlink.
 *
 * @author darrenkelly
 */
class ListItems implements IListItems {

  /**
   * Constructor.
   */
  public function __construct() {
  }

  /**
   * The list items of this.
   *
   * @var IListItem[]
   */
  private $items = array();

  /**
   * Add the given list item as a child of this.
   *
   * @param IListItem $li
   *   The non-empty list item to add as a child to this.
   *
   * @return IListItems
   *   This.
   */
  public function addItem(IListItem $li) {
    // @todo check unique ? list vs set ?
    // @todo warn or throw if empty ?
    if (!empty($li)) {
      $this->items[] = $li;
    }
    return $this;
  }

  /**
   * Whether this has any children.
   *
   * @return bool
   *   Whether this has any children.
   */
  public function hasItems() {
    return count($this->items) > 0;
  }

  /**
   * Builds a Drupal list items array suitable for theme_item_list().
   *
   * 'An array of items to be displayed in the list.
   * If an item is a string, then it is used as is.
   * If an item is an array, then the "data" element of
   * the array is used as the contents of the list item.
   * If an item is an array with a "children" element,
   * those children are displayed in a nested list.
   * All other elements are treated as attributes
   * of the list item element.'
   *
   * The OOE implementation only uses the 'data' element approach,
   * it does not support an item that is only a string.
   *
   * @return array
   *   A Drupal list items array suitable for theme_item_list().
   */
  public function get() {
    $get = array();
    foreach ($this->items as $li) {
      $get[] = $li->get();
    }
    return $get;
  }

}
