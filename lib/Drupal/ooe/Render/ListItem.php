<?php

/**
 * @file
 * ListItem.
 */

namespace Drupal\ooe\Render;

use Drupal\ooe\Xhtml\TagManager;

/**
 * Implements a list item, possibly with children.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2190 ListItem @endlink.
 *
 * @author darrenkelly
 */
class ListItem extends Render implements IListItem {

  /**
   * Used as the contents of the list item.
   *
   * From theme_item_list(): 'the "data" element ...
   * is used as the contents of the list item'
   *
   * @todo What type restrictions ?
   *
   * @todo Encapsulate possible Drupal data options for list items IListItemData
   *
   * @var string
   */
  private $data;

  /**
   * Constructs with the given list item data.
   *
   * @todo accept attributes
   *
   * @todo is $data only string ?
   *
   * @param string $data
   *   From theme_item_list(): 'the "data" element ...
   *   is used as the contents of the list item'.
   */
  public function __construct($data) {
    // @todo checks.
    $this->data = $data;
  }

  /**
   * The children of this.
   *
   * @var IListItem[]
   */
  private $children = array();

  /**
   * A (copy of) the children of this.
   *
   * Please note that any additional operations
   * on the returned array will have no affect
   * on the underlying children of this.
   *
   * @return IListItem[]
   *   An array of child list items.
   */
  public function getChildren() {
    return $this->children;
    // @todo immutable clone of ?
  }

  /**
   * Add a child item to this.
   *
   * @param IListItem $child
   *   A child item for this.
   *
   * @return IListItem
   *   This (not the added child).
   */
  public function addChild(IListItem $child) {
    // @todo check whether unique ? list vs set ?
    if (!empty($child)) {
      $this->children[] = $child;
    }
    return $this;
  }

  /**
   * Builds and returns a Drupal list item array suitable for theme_item_list().
   *
   * 'If an item is an array, then the "data" element of
   * the array is used as the contents of the list item.
   * If an item is an array with a "children" element,
   * those children are displayed in a nested list.
   * All other elements are treated as attributes
   * of the list item element.'
   *
   * The OOE implementation only uses the 'data' element approach,
   * it does not support an item that is only a string.
   *
   * Please note that usually one does not need to use this directly,
   * as a list item is always handled by an @link IListItems @endlink.
   *
   * @return array
   *   A Drupal list item array.
   */
  public function get() {
    $get = $this->getAttributes();

    $get['data'] = $this->data;

    $kids = $this->children;
    if (!empty($kids)) {
      foreach ($kids as $li) {
        $get['children'][] = $li->get();
      }
    }
    return $get;
  }

  /**
   * A tag manager mixin.
   *
   * @var \Drupal\ooe\Xhtml\ITagManager
   */
  private $tagManager;

  /**
   * A lazily created and configured @link TagManager @endlink mixin.
   *
   * @return \Drupal\ooe\Xhtml\ITagManager
   *   A lazily created and configured tag manager mixin.
   */
  protected function tagManager() {
    if (empty($this->tagManager)) {
      // @codingStandardsIgnoreStart
      //$this->tagManager = $this->factory()->newTagManager();
      // @codingStandardsIgnoreEnd
      $this->tagManager = new TagManager();
    }
    return $this->tagManager;
  }

  /**
   * Add a CSS class to this.
   *
   * @param string $class
   *   A CSS class for this.
   *
   * @return ITagManager
   *   The tag manager of this.
   */
  public function addCssClass($class) {
    $this->tagManager()->addCssClass($class);
    return $this->tagManager();
  }

  /**
   * An array populated with XHTML tag attributes.
   *
   * @return array
   *   An array populated with XHTML tag attributes.
   */
  public function getAttributes() {
    return $this->tagManager()->getAttributes();
  }

  /**
   * Sets an XHTML attribute of this with the given name and value.
   *
   * @param string $name
   *   A non empty name of an HTML attribute.
   * @param string $value
   *   A value of an XHTML attribute (may be null).
   *
   * @return ITagManager
   *   The tag manager of this.
   */
  public function setAttribute($name, $value) {
    $this->tagManager()->setAttribute($name, $value);
    return $this->tagManager();
  }

  /**
   * Sets the entire tag attributes array (overwriting any current attributes).
   *
   * @param array $attributes
   *   A Drupal-style attributes array.
   *
   * @return ITagManager
   *   The tag manager of this.
   */
  public function setAttributes(array $attributes) {
    $this->tagManager()->setAttributes($attributes);
    return $this->tagManager();
  }

  /**
   * Sets the CSS classes of this.
   *
   * @param string[] $classes
   *   An array of CSS classes for this.
   *
   * @return ITagManager
   *   The tag manager of this.
   */
  public function setCssClasses(array $classes) {
    $this->tagManager()->setCssClasses($classes);
    return $this->tagManager();
  }

  /**
   * Sets the CSS style of this.
   *
   * @param string $style
   *   The CSS style of this.
   *
   * @return ITagManager
   *   The tag manager of this.
   */
  public function setCssStyle($style) {
    $this->tagManager()->setCssStyle($style);
    return $this->tagManager();
  }

}
