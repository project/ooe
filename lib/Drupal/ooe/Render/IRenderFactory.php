<?php

/**
 * @file
 * IRenderFactory.
 */

namespace Drupal\ooe\Render;

use Drupal\ooe\Xhtml\ITag;

/**
 * A factory for @link IRender @endlink implementations.
 *
 * @todo Cover more render cases.
 *
 * @author darrenkelly
 */
interface IRenderFactory {

  /**
   * The tag factory of this.
   *
   * The motto is:
   * "Let an @link ITag @endlink handle the information about XHTML tags,
   * let @link IRender @endlink deal with the
   * Drupal render array structure only."
   *
   * @return \Drupal\ooe\Xhtml\ITagFactory
   *   The tag factory of this.
   */
  public function getTagFactory();

  /**
   * Supplies a most basic markup render object product.
   *
   * @param string $markup
   *   The (usually HTML) markup.
   * @param ITag $wrapperTag
   *   If present, will be used to "wrap" the markup
   *   with a prefix and suffix from the given XHTML tag holder.
   * @param array $attributes
   *   Optional. A Drupal-style attributes array
   *   for the wrapping tag (if present). Will
   *   overwrite any existing attributes in the tag !.
   *
   * @return \Drupal\ooe\Render\IRender
   *   A new render object.
   */
  public function newR($markup, ITag $wrapperTag, array $attributes);

  /**
   * Supplies a basic translated markup render object as product.
   *
   * Uses automatic translation of the given markup text.
   *
   * @param string $markup
   *   Required. The (relatively simple) markup, may contain HTML.
   * @param ITag $wrapperTag
   *   If present, will be used to "wrap" the markup
   *   with a prefix and suffix from the given XHTML tag holder.
   * @param array $attributes
   *   Optional. A Drupal-style attributes array
   *   for the wrapping tag (if present). Will
   *   overwrite any existing attributes in the tag !.
   *
   * @return \Drupal\ooe\Render\IRender
   *   A new render object with translated markup.
   *
   * @deprecated Because internal translation of text
   * as class variables passed to t() is not compatible
   * with the Drupal localization registry.
   */
  public function newT($markup, ITag $wrapperTag, array $attributes);

  /**
   * A new paragraph with simple markup text.
   *
   * This is not really intended for use with complex
   * content, consider instead using a set/group
   * via @link IRenderFactory::newSetP @endlink().
   *
   * @param string $text
   *   The (translated) text markup for the paragraph.
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return \Drupal\ooe\Render\IRender
   *   A new paragraph render object.
   */
  public function newP($text, array $attributes);

  /**
   * A new span with simple markup text.
   *
   * This is not really suitable
   * for use with complex content, consider instead using
   * a set/group via @link IRenderFactory::newSetSpan @endlink()
   * and adding smaller translated pieces to the span.
   *
   * @param string $text
   *   The (simple, translated) text markup for the span.
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return \Drupal\ooe\Render\IRender
   *   A new span render object.
   */
  public function newSpan($text, array $attributes);

  /**
   * A new B (bold) with simple markup text.
   *
   * @param string $text
   *   The (simple, translated) text markup.
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return \Drupal\ooe\Render\IRender
   *   A new bold render object.
   */
  public function newB($text, array $attributes);

  /**
   * A new I (italic) with simple markup text.
   *
   * @param string $text
   *   The (simple, translated) text markup.
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return \Drupal\ooe\Render\IRender
   *   A new italic render object.
   */
  public function newI($text, array $attributes);

  /**
   * A new EM (emphasis) with simple markup text.
   *
   * @param string $text
   *   The (simple, translated) text markup.
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return \Drupal\ooe\Render\IRender
   *   A new emphasis render object.
   */
  public function newEm($text, array $attributes);

  /**
   * A new STRONG with simple markup text.
   *
   * @param string $text
   *   The (simple, translated) text markup.
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return \Drupal\ooe\Render\IRender
   *   A new strong render object.
   */
  public function newStrong($text, array $attributes);

  /**
   * A new header with simple text.
   *
   * @param string $header
   *   The (translated) header text.
   * @param int $size
   *   An integer from 1 to 6 for the HTML H1 .. H6 header choice.
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return \Drupal\ooe\Render\IRender
   *   a new header redner object.
   */
  public function newH($header, $size, array $attributes);

  /**
   * A new HR (horizontal line).
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return \Drupal\ooe\Render\IRender
   *   A new horizontal ruler render object.
   */
  public function newHr(array $attributes);

  /**
   * A new BR (line break).
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return \Drupal\ooe\Render\IRender
   *   A new line break render object.
   */
  public function newBr(array $attributes);

  /**
   * A new themed list renderer.
   *
   * @param IListItems $items
   *   A list item manager that can build a
   *   Drupal items array for theme_item_list().
   * @param string $title
   *   (Optional.) 'The title of the list.' Defaults to null.
   *
   * @return \Drupal\ooe\Render\IRenderList
   *   A new render list.
   */
  public function newList(IListItems $items, $title);

  /**
   * Supples a list items product.
   *
   * @return \Drupal\ooe\Render\IListItems
   *   A new list items product.
   */
  public function newListItems();

  /**
   * Supplies a list item product.
   *
   * @todo accept attributes
   *
   * @todo is $data only string ?
   *
   * @param string $data
   *   From theme_item_list(): 'the "data" element ...
   *   is used as the contents of the list item'.
   *
   * @return \Drupal\ooe\Render\IListItem
   *   A list item product.
   */
  public function newListItem($data);

  /**
   * A new menu subtree.
   *
   * Identified by the
   * link path of the parent of the subtree
   * within a given existing menu.
   *
   * @param string $link_path
   *   The menu link path to find.
   * @param string $menu
   *   The name of a valid menu to search in.
   *
   * @return \Drupal\ooe\Render\IRender
   *   A new menu subtree render object.
   */
  public function newMenuSubtree($link_path, $menu);

  /**
   * A new render set (render group) for constructing composite render arrays.
   *
   * If a wrapper tag is given, and if an attributes
   * array is given, they will be applied to the tag
   * (before the tag is applied as render array prefix and suffix).
   *
   * Consider instead using directly one of the
   * convenient wrapped set/group creators like
   * @link IRenderFactory::newSetP @endlink(),
   * @link IRenderFactory::newSetDiv @endlink(),
   * @link IRenderFactory::newSetBlockquote @endlink().
   *
   * @param ITag $wrapperTag
   *   If present, will be used to "wrap" all grouped elements
   *   with a prefix and suffix from the given XHTML tag manager.
   *   Typical examples would be a P or a DIV tag manager.
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return \Drupal\ooe\Render\IRenderSet
   *   A new render set.
   */
  public function newSet(ITag $wrapperTag, array $attributes);

  /**
   * A new render set/group with a P tag wrapper.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return \Drupal\ooe\Render\IRenderSet
   *   A new render set/group with a P tag wrapper.
   */
  public function newSetP(array $attributes);

  /**
   * A new render set/group with a SPAN tag wrapper.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return \Drupal\ooe\Render\IRenderSet
   *   A new render set/group with a SPAN tag wrapper.
   */
  public function newSetSpan(array $attributes);

  /**
   * A new render set/group with a DIV tag wrapper.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return \Drupal\ooe\Render\IRenderSet
   *   A new render set/group with a DIV tag wrapper.
   */
  public function newSetDiv(array $attributes);

  /**
   * A new render set/group with a BLOCKQUOTE tag wrapper.
   *
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   *
   * @return \Drupal\ooe\Render\IRenderSet
   *   A new render set/group with a BLOCKQUOTE tag wrapper.
   */
  public function newSetBlockquote(array $attributes);

}

// @codingStandardsIgnoreStart
/**
 * A new themed list renderer.
 *
 * @param array $items From theme_item_list:
 * 'An array of items to be displayed in the list.
 * If an item is a string, then it is used as is. If an item is an array,
 * then the "data" element of the array is used
 * as the contents of the list item.
 * If an item is an array with a "children" element,
 * those children are displayed in a nested list.
 * All other elements are treated as attributes of the list item element.'
 *
 * @param string $title
 *   (Optional.) 'The title of the list.' Defaults to null.
 * @return \Drupal\ooe\Render\IRenderList
 */
//public function newList(array $items, $title);
// @codingStandardsIgnoreEnd
