<?php

/**
 * @file
 * IListItem.
 */

namespace Drupal\ooe\Render;

use Drupal\ooe\Xhtml\ITagManager;

/**
 * Interface for a list item, possibly with children.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2186 IListItem @endlink.
 *
 * @author darrenkelly
 */
interface IListItem extends ITagManager {

  /**
   * Builds and returns a Drupal list item array suitable for theme_item_list().
   *
   * 'If an item is an array, then the "data" element of
   * the array is used as the contents of the list item.
   * If an item is an array with a "children" element,
   * those children are displayed in a nested list.
   * All other elements are treated as attributes
   * of the list item element.'
   *
   * The OOE implementation only uses the 'data' element approach,
   * it does not support an item that is only a string.
   *
   * Please note that usually one does not need to use this directly,
   * as a list item is always handled by an @link IListItems @endlink.
   *
   * @return array
   *   A Drupal list item array.
   */
  public function get();


  /**
   * A (copy of) the children of this.
   *
   * Please note that any additional operations
   * on the returned array will have no affect
   * on the underlying children of this.
   *
   * @return IListItem[]
   *   An array of child list items.
   */
  public function getChildren();

  /**
   * Adds a child item to this.
   *
   * @param IListItem $child
   *   A child item for this.
   *
   * @return IListItem
   *   This (not the added child).
   */
  public function addChild(IListItem $child);

}
