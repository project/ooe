<?php

/**
 * @file
 * Render.
 */

namespace Drupal\ooe\Render;

use Drupal\ooe\Common;
use Drupal\ooe\Xhtml\ITag;

/**
 * Implements the simplest render array manager,
 * defaulting to generating a markup render array.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2031 Render @endlink.
 *
 * Visit also:
 * - @link https://drupal.org/node/930760 Render Arrays in Drupal 7 @endlink.
 *
 * @author darrenkelly
 */
class Render implements IRender {

  /**
   * The markup contained by this (if any). May be null.
   *
   * @var string
   */
  private $markup = NULL;

  /**
   * Constructor.
   *
   * Construct with (optional) markup,
   * and an optional "wrapper" tag
   * that will be used to set a tag prefix (including any attributes)
   * and a tag suffix (unless the tag is self-closing)
   * around the generated markup render array,
   * (or around any child elements of set/group-like subclasses).
   *
   * Special note on automatic internal translation:
   * Since the Drupal localization registry does not catch
   * strings passed to t() via class variables $translate
   * should usually be FALSE, it is only kept as a legacy switch.
   *
   * Where possible, markup strings should be translated
   * using t() before passing to this constructor
   * (possibly also using placeholders).
   *
   * @param string $markup
   *   The (usually HTML) markup. If null will be ignored.
   * @param ITag $wrapperTag
   *   If present, will be used to "wrap" the markup
   *   with a prefix and suffix from the given XHTML tag
   *   (including any attributes in the prefix).
   * @param array $attributes
   *   Optional. A Drupal-style attributes array
   *   for the wrapping tag (if present). Will
   *   overwrite any existing attributes in the tag !
   * @param bool $translate
   *   Whether to translate the markup text;
   *   by default as set in @link Common::TRANSLATE_DEFAULT @endlink.
   *   DEPRECATED: since internal translation of strings as
   *   class variables passed to t() is not compatible with
   *   the Drupal localization registry.
   */
  public function __construct(
      $markup,
      ITag $wrapperTag = NULL,
      array $attributes = NULL,
      $translate = Common::TRANSLATE_DEFAULT
  ) {
    if (!is_null($markup) && !is_string($markup)) {
      throw new \Exception('$markup must be a string.');
    }
    if (is_null($translate) || !is_bool($translate)) {
      throw new \Exception('$translate must be a non null boolean got(' . $translate . ') !');
    }
    if (!is_null($markup)) {

      $t = $translate ? t($markup) : $markup;
      // @codingStandardsIgnoreStart
      // The above is known to be incorrect for Drupal translation
      // as it currently stands, but works OK for some OOE demos.
      // A correct strategy for being able to apply translation
      // as a policy to encapsulated markup is "work in progress".
      // @codingStandardsIgnoreEnd

      $this->markup = $t;
    }
    if (!empty($wrapperTag)) {
      if (!empty($attributes)) {
        $wrapperTag->setAttributes($attributes);
      }
      $this->setWrapperTag($wrapperTag);
    }
  }

  /**
   * Builds and returns a Drupal render array.
   *
   * If it was constructed with any markup it will
   * be assumed that it is a markup render array.
   *
   * If a prefix and/or suffix are defined they
   * will be included.
   *
   * @return array
   *   A Drupal render array.
   */
  public function get() {
    $out = array();
    if (!empty($this->markup)) {
      $out['#type'] = 'markup';
      $out['#markup'] = $this->markup;
    }
    $prefix = $this->getPrefix();
    if (!empty($prefix)) {
      $out['#prefix'] = $prefix;
    }
    $suffix = $this->getSuffix();
    if (!empty($suffix)) {
      $out['#suffix'] = $suffix;
    }
    return $out;
  }

  /**
   * The prefix.
   *
   * If this has a wrapper tag, it will be used to build a prefix,
   * otherwise the prefix as explicitly set will be used.
   *
   * @return string
   *   The prefix.
   */
  public function getPrefix() {
    if (!empty($this->wrapperTag)) {
      return $this->wrapperTag->getAsPrefix();
    }
    return $this->prefix;
  }

  /**
   * The suffix.
   *
   * If this has a wrapper tag, it will be used to build a suffix,
   * otherwise the suffix as explicitly set will be used.
   *
   * @return string
   *   The suffix.
   */
  public function getSuffix() {
    if (!empty($this->wrapperTag)) {
      return $this->wrapperTag->getAsSuffix();
    }
    return $this->prefix;
  }

  /**
   * The render prefix (if any, may be null).
   *
   * @see drupal_render(&$elements);
   *
   * @var string
   */
  private $prefix = NULL;

  /**
   * Sets the render prefix.
   *
   * @param string $prefix
   *   The render prefix; may contain HTML, must not be empty here.
   *
   * @return IRender
   *   This.
   *
   * @see drupal_render(&$elements)
   */
  public function setPrefix($prefix) {
    if (empty($prefix) || !is_string($prefix)) {
      throw new \Exception('$prefix must be a non empty string !');
    }
    $this->prefix = $prefix;
    return $this;
  }

  /**
   * The render suffix (if any, may be null).
   *
   * @see drupal_render(&$elements)
   *
   * @var string
   */
  private $suffix = NULL;

  /**
   * Sets the render suffix.
   *
   * @param string $suffix
   *   The render prefix; may contain HTML. May be null.
   *
   * @return IRender
   *   This.
   *
   * @throws \Exception
   *   If the provided suffix is not a string and not null.
   *
   * @see drupal_render(&$elements)
   */
  public function setSuffix($suffix) {
    if (!empty($suffix) && !is_string($suffix)) {
      throw new \Exception('$suffix must be a string (or NULL) !');
    }
    $this->suffix = $suffix;
    return $this;
  }

  /**
   * Sets a wrapper tag.
   *
   * Will be used
   * to generate the prefix and suffix of this.
   *
   * Please read also the extended remarks
   * under @link IRender::setWrapperTag @endlink(ITag) !
   *
   * @param ITag $tag
   *   The XHTML tag holder. Must not be null or empty.
   *
   * @return IRender
   *   This.
   */
  public function setWrapperTag(ITag $tag) {
    if (empty($tag)) {
      throw new \Exception('$tag must not be empty !');
    }
    /*
    $this->setPrefix($tag->getAsPrefix());
    $this->setSuffix($tag->getAsSuffix());
     */
    $this->wrapperTag = $tag;
    return $this;
  }

  /**
   * The wrapper tag (if any). May be null.
   *
   * If non-null it will be used for the suffix and prefix !
   *
   * @var ITag
   */
  private $wrapperTag;

}
