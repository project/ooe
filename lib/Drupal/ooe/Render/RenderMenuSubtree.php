<?php

/**
 * @file
 * RenderMenuSubtree.
 */

namespace Drupal\ooe\Render;

use Drupal\ooe\Utility\Utility;

use Drupal\ooe\Menu\MenuCommon;

/**
 * Renders a subtree of a menu identifed by
 * the menu path of the parent of the subtree.
 *
 * UML:
 * @link http://drupal7demo.webel.com.au/node/2203 RenderMenuSubtree @endlink.
 *
 * @todo the extension of Render is not entirely suitable because
 * the get() here ignores for example the suffix and prefix.
 * Consider wrapping generated menu_tree_output in another render array.
 *
 * @author darrenkelly
 */
class RenderMenuSubtree extends Render {

  /**
   * The menu link path to find.
   *
   * @var string
   */
  private $linkPath;

  /**
   * The name of the menu to search in.
   *
   * @var string
   */
  private $menu = MenuCommon::MENU_NAVIGATION;

  /**
   * Construct.
   *
   * @param string $linkPath
   *   The menu link path to find.
   * @param string $menu
   *   The name of a valid menu to search in.
   *
   * @var bool $expandAll
   *   Whether all menu items should be forced
   *   to be expanded on display as links.
   */
  public function __construct($linkPath, $menu = MenuCommon::MENU_NAVIGATION, $expandAll = TRUE) {
    // @todo check args.
    $this->linkPath = $linkPath;
    $this->menu = $menu;
    $this->expandAll = $expandAll;
    // @todo: Parent ignored !
  }

  /**
   * Whether all menu items should be forced to be expanded on display as links.
   *
   * @var bool
   */
  private $expandAll;

  /**
   * Builds and returns a Drupal render array for the menu subtree.
   *
   * Visit also:
   * - @link menu_tree_output @endlink($tree)
   * - @link Utility::submenu @endlink($path, $menuName)
   *
   * @return array
   *   A Drupal render array for the menu subtree.
   */
  public function get() {
    $subtree = Utility::submenu($this->linkPath, $this->menu);
    if ($this->expandAll) {
      $this->expand($subtree);
    }
    return menu_tree_output($subtree);
  }

  /**
   * Recursively expands the given Drupal menu array tree.
   *
   * @param array $tree
   *   Reference to a Drupal array tree.
   */
  private function expand(array &$tree) {
    foreach ($tree as &$branch) {
      // $branch['link']['expanded'] = 1; // NO.
      $link = &$branch['link'];
      $link['expanded'] = 1;
      $this->expand($branch['below']);
    }
  }

}
