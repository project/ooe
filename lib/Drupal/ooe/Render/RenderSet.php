<?php

/**
 * @file
 * RenderSet.
 */

namespace Drupal\ooe\Render;

use Drupal\ooe\Xhtml\ITag;

/**
 * Groups render objects and constructs a composite Drupal render array.
 *
 * UML: @link http://drupal7demo.webel.com.au/node/2189 RenderSet @endlink.
 *
 * @todo Consider renaming to RenderGroup
 * or RenderMap since 'set' has a specific meaning.
 *
 * @author darrenkelly
 */
class RenderSet extends Render implements IRenderSet {

  /**
   * Constructor.
   *
   * If a wrapper tag is given, and if an attributes
   * array is given, they will be applied to the tag
   * (before the tag is applied as render array prefix and suffix).
   *
   * It is always assumed that this has no direct markup text.
   * The onus of translation is delegated to the children.
   *
   * @param ITag $wrapperTag
   *   If present, will be used to "wrap" all grouped elements
   *   with a prefix and suffix from the given XHTML tag.
   *   Typical examples would be a P or a DIV tag.
   * @param array $attributes
   *   Optional. A Drupal-style attributes array.
   */
  public function __construct(ITag $wrapperTag = NULL, array $attributes = NULL) {

    // @codingStandardsIgnoreStart
    //if (!empty($wrapperTag) && !empty($attributes)) {
    // $wrapperTag->setAttributes($attributes);
    //}
    // @codingStandardsIgnoreEnd

    parent::__construct(NULL, $wrapperTag, $attributes);
  }

  /**
   * An array of render objects.
   *
   * @var IRender[]
   */
  private $items;

  /**
   * Add a render object item with the given key.
   *
   * @param string $key
   *   The array key.
   * @param IRender $item
   *   The render item to add.
   *
   * @return IRenderSet
   *   This.
   */
  public function addItem($key, IRender $item) {
    // @todo checks.
    $this->items[$key] = $item;
    return $this;
  }

  /**
   * Builds and returns the composite Drupal render array.
   *
   * @return array
   *   The composite Drupal render array.
   */
  public function get() {
    $out = parent::get();
    foreach ($this->items as $key => $item) {
      $out[$key] = $item->get();
    }
    return $out;
  }

}
